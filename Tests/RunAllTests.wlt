Print[System`Private`$InputFileName];
path = FileNameTake[FindFile["LieAnalysis`"], {1, -4}];
testfolder = FileNameJoin[{path,"Tests"}];
Print[testfolder]

testfiles = FileNames["Test - *.wlt",{testfolder}];

(* Run all tests *)
Import/@testfiles;