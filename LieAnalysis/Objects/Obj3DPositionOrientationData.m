(*************************************************************************************************************************
** Obj3DPositionOrientationData.m
** This .m file contains a class-definition for Obj3DPositionOrientationData
**
** Author: F.C. Martin <f.c.martin@tue.nl>
**************************************************************************************************************************)
BeginPackage["LieAnalysis`Objects`Obj3DPositionOrientationData`", {
	"Classes`",
	"LieAnalysis`Objects`ObjWaveletTransform`"
}];


	(*|****************************|*)
  	(*| Declare Class and Defaults |*)
  	(*|****************************|*)
  	
  	
	DeclareClass[ObjWaveletTransform, Obj3DPositionOrientationData];
	DeclareDefaults[Obj3DPositionOrientationData,Association[
		"DcData"->Null,
		"Symmetry"->None,
		"OrientationList"->None
	]];


	Begin["`Private`"];


	(*|****************************|*)
  	(*| Declare Invariants         |*)
  	(*|****************************|*)
  	
  	
  	DeclareInvariant[Obj3DPositionOrientationData,
  		(#Data === Automatic || ArrayQ[#Data,4,NumberQ]) /. False :> (Message[ValidQ::elfail, "Data", "Obj3DPostionOrientationData", "should be a 4D array"]; False)&];
  	
	DeclareInvariant[Obj3DPositionOrientationData, 
		(#DcData === Null || ArrayQ[#DcData,3]) /. False :> (Message[ValidQ::elfail, "DcData", "Obj3DPostionOrientationData", "should be a 3D matrix"]; False)&];
		
	DeclareInvariant[Obj3DPositionOrientationData, 
		MatchQ[#Symmetry, None | "AntiPodal" | "AntiPodalAntisymmetric"] /. False :> (Message[ValidQ::elfail, "Symmetry", "Obj3DPostionOrientationData", "be one the following: None, AntiPodal or AntiPodalAntisymmetric"]; False)&];

	DeclareInvariant[Obj3DPositionOrientationData,
		(#OrientationList === None || MatchQ[#OrientationList,{{_,_,_}..}]) /. False :> (Message[ValidQ::elfail, "OrientationList", "Obj3DPostionOrientationData", "either a list of 3d positions or none."]; False)&];


	(* ************************** *)
  	(* Properties 		          *)
  	(* ************************** *)
  	
  	
	(* Number of Orientations *)
	object_Obj3DPositionOrientationData["Orientations"] := Length[object["FullOrientationList"]];
	
	(* Construct Full orientation scores *)
	object_Obj3DPositionOrientationData["FullData"] := Switch[object[["Symmetry"]],
		"AntiPodal" , Join[object["Data"], Conjugate[object["Data"]],4],
		"AntiPodalAntisymmetric" , Join[object["Data"], -Conjugate[object["Data"]],4],
		None , object["Data"]
	];
	
	(* Real Full orientation scores *)
	object_Obj3DPositionOrientationData["FullData","Real"] := Re[object["FullData"]];
	
	(* Imaginary Full orientation scores *)
	object_Obj3DPositionOrientationData["FullData","Imaginary"] := Im[object["FullData"]];
		
	(* Orientation List *)
	object_Obj3DPositionOrientationData["OrientationList"] := object[["OrientationList"]];
	
	(* Full Orientation List *)
	object_Obj3DPositionOrientationData["FullOrientationList"] := Switch[object[["Symmetry"]],
		"AntiPodal" , Join[object[["OrientationList"]],-object[["OrientationList"]]],
		"AntiPodalAntisymmetric" , Join[object[["OrientationList"]],-object[["OrientationList"]]],
		None , object[["OrientationList"]]
	];
		
	(* Polar Orientation List *)		
	object_Obj3DPositionOrientationData["PolarOrientationList"] := Transpose[LieAnalysis`Common`CartesianToSphericalCoordinates[object[["OrientationList"]]]][[2;;3]];
	
	(* Full Polar OrientationList *)
	object_Obj3DPositionOrientationData["FullPolarOrientationList"] := Transpose[LieAnalysis`Common`CartesianToSphericalCoordinates[object["FullOrientationList"]]][[2;;3]];
		
	(* Topology *)
	object_Obj3DPositionOrientationData["Topology"] := If[object[["Symmetry"]]==="AntiPodal", Union[Sort /@Mod[object["FullTopology"],object["Orientations"]/2,1]],object["FullTopology"]];
	
	(* Full Topology *) 
	object_Obj3DPositionOrientationData["FullTopology"] := MeshCells[ConvexHullMesh[object["FullOrientationList"]],2][[All,1]];
	

	(*|****************************|*)
  	(*| Symmary Visualization      |*)
  	(*|****************************|*)
	
	
	Obj3DPositionOrientationData["Format"] = Function[{instance,fmt},Module[{alwaysGrid, sometimesGrid, head = Head[instance]},
	
		alwaysGrid = {
			BoxForm`MakeSummaryItem[{"Input Dimensions: ", instance["InputDimensions"]}, fmt],
			BoxForm`MakeSummaryItem[{"Orientations: ", ToString[instance["Orientations"]]<>" ("<>ToString[instance["NumberOfTransforms"]]<>")"}, fmt],
			BoxForm`MakeSummaryItem[{"Symmetry: ", instance[["Symmetry"]]}, fmt],
			BoxForm`MakeSummaryItem[{"Wavelets: ", instance["WaveletType"]}, fmt],
			BoxForm`MakeSummaryItem[{"Is Complex: ", instance["ComplexQ"]}, fmt],
			BoxForm`MakeSummaryItem[{"Valid: ", ValidQ[instance]}, fmt]
		};
	
		sometimesGrid = {
			BoxForm`MakeSummaryItem[{"Elements: ", ElisionsDump`expandablePane[Style @@@ Normal @ KeySort @ Merge[{Thread[Keys[head["Defaults"]] -> Plain], Thread[(Select[Keys @@ instance, StringQ[#] (*&& StringStartsQ[#, "$"]*) &]) -> Bold]}, Prepend[#, Italic] &]]}, fmt],
			BoxForm`MakeSummaryItem[{"Properties: ", ElisionsDump`expandablePane[Style @@@ Normal @ KeySort @ Merge[{Thread[head["Properties", "Public"] -> Plain], Thread[(Select[Keys @@ instance, StringQ[#] && StringStartsQ[#, "$"] &]) -> Bold]}, Prepend[#, Italic] &]]}, fmt],
			BoxForm`MakeSummaryItem[{"ByteCount: ", ToString[ByteCount[instance]/10.^6]<>" MB"}, fmt]
		};
	
		BoxForm`ArrangeSummaryBox[
			head,
			ToString[head] <> "[<>]",
			ImageAdjust[Image3D[instance["Data","Real"][[All,All,All,1]]]],
			alwaysGrid,
			sometimesGrid,
			fmt,
			"Interpretable" -> False
		]
	
	]];


	(*|****************************|*)
  	(*| Bulletproofing		       |*)
  	(*|****************************|*)
  	
  	
	Protect[Obj3DPositionOrientationData];


	End[];


EndPackage[];