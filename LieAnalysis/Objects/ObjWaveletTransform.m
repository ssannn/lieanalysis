(*************************************************************************************************************************
** ObjWaveletTransform.m
** This .m file contains common function used for wavelet transformations of 2D images.
**
** Author: F.C. Martin <f.c.martin@tue.nl>
**************************************************************************************************************************)
BeginPackage["LieAnalysis`Objects`ObjWaveletTransform`", {
	"Classes`",
  	"LieAnalysis`Objects`Object`"
}];


	(*|****************************|*)
  	(*| Declare Class and Defaults |*)
  	(*|****************************|*)
  	
  	
	DeclareClass[Object, ObjWaveletTransform];
	DeclareDefaults[ObjWaveletTransform, Association[
    	"Wavelets"->None,
      	"InputData"->None
  	]];


  	Begin["`Private`"];


	(*|****************************|*)
  	(*| Declare Invariants         |*)
  	(*|****************************|*)
  	
  	
    DeclareInvariant[ObjWaveletTransform, 
    	(ArrayQ[#Data] || #Data === Automatic) /. False :> (Message[ValidQ::elfail, "Data", "ObjWaveletTransform","be a 2D or 3D array"]; False)&];
    	
    DeclareInvariant[ObjWaveletTransform, 
    	(ValidQ[#Wavelets, LieAnalysis`Objects`ObjWavelet`ObjWavelet] || #Wavelets === None) /. False :> (Message[ValidQ::elfail, "Wavelets", "ObjWaveletTransform", "be a WaveletObj"]; False)&];
    	
    DeclareInvariant[ObjWaveletTransform, 
    	(ValidQ[#InputData, ObjWaveletTransform] || MatrixQ[#InputData] || MatchQ[#InputData,{{{__}...}...}] || ImageQ[#InputData]  || #InputData === None) /. False :> (Message[ValidQ::elfail, "InputData", "ObjWaveletTransform", "be a matrix, image or None"]; False)&];
	

	(* ************************** *)
  	(* Properties 		          *)
  	(* ************************** *)
  	
  	(* Returns boolean if the data is complex *)
  	object_ObjWaveletTransform["ComplexQ"] := object["Data"] =!= Re[object["Data"]];
  	
    (* Return Number of Layers *)
    object_ObjWaveletTransform["NumberOfLayers"] := If[ArrayQ[object[["Data"]]], Last[Dimensions[object[["Data"]]]], 0];
    
    (* Return Number of the Transforms *)
    object_ObjWaveletTransform["NumberOfTransforms"] := object["NumberOfLayers"];
    
    (* Return wavelet transform volume *)
    object_ObjWaveletTransform["Data"] := object[["Data"]];
    	
    (* Real wavelet transform volume*)
    object_ObjWaveletTransform["Data","Real"] := Re[object[["Data"]]];
    
    (* Imaginairy wavelet transform volume*)
    object_ObjWaveletTransform["Data","Imaginary"] := Im[object[["Data"]]];
    
    (* Return the Input Dimensions *)
    object_ObjWaveletTransform["InputDimensions"] := If[object[["InputData"]] =!= None,
    	Switch[Head[object[["InputData"]]],
    		Image | Image3D, ImageDimensions[object[["InputData"]]],
    		List, Dimensions[object[["InputData"]]],
    		_ , None 
    	],
    	None
    ];
    
    (* Return the Wavelet Family *)
    object_ObjWaveletTransform["WaveletType"] := If[object[["Wavelets"]] =!= None, Head[object[["Wavelets"]]], None];


	(*|****************************|*)
  	(*| Symmary Visualization      |*)
  	(*|****************************|*)
	
	
	ObjWaveletTransform["Format"] = Function[{instance, fmt}, Module[{alwaysGrid, sometimesGrid, head = Head[instance]},

      	alwaysGrid = {
	        BoxForm`MakeSummaryItem[{"Input Dimensions: ", instance["InputDimensions"]}, fmt],
	        BoxForm`MakeSummaryItem[{"Wavelets: ", instance["WaveletType"]}, fmt],
	        BoxForm`MakeSummaryItem[{"Valid: ", ValidQ[instance]}, fmt]
      	};

      	sometimesGrid = {
	        BoxForm`MakeSummaryItem[{"Elements: ", ElisionsDump`expandablePane[Style @@@ Normal @ KeySort @ Merge[{Thread[Keys[head["Defaults"]] -> Plain], Thread[(Select[Keys @@ instance, StringQ[#] (*&& StringStartsQ[#, "$"]*) &]) -> Bold]}, Prepend[#, Italic] &]]}, fmt],
	        BoxForm`MakeSummaryItem[{"Properties: ", ElisionsDump`expandablePane[Style @@@ Normal @ KeySort @ Merge[{Thread[head["Properties", "Public"] -> Plain], Thread[(Select[Keys @@ instance, StringQ[#] && StringStartsQ[#, "$"] &]) -> Bold]}, Prepend[#, Italic] &]]}, fmt],
	        BoxForm`MakeSummaryItem[{"ByteCount: ", ToString[ByteCount[instance]/10.^6]<>" MB"}, fmt]
      	};

		BoxForm`ArrangeSummaryBox[
			head,
	        SymbolName[head] <> "[<>]",
	        Image[instance["Data","Real"]],
	        alwaysGrid,
	        sometimesGrid,
	        fmt,
	        "Interpretable" -> False
      	]

    ]];


	(*|****************************|*)
  	(*| Bulletproofing		       |*)
  	(*|****************************|*)
  	
  	
    Protect[ObjWaveletTransform];


  	End[];


EndPackage[];