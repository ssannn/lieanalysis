(*************************************************************************************************************************
** ObjPositionOrientationData .m
** This .m file contains class-defination for the ObjPositionOrientationData 
**
** Author: F.C. Martin <f.c.martin@tue.nl>
**************************************************************************************************************************)
BeginPackage["LieAnalysis`Objects`ObjPositionOrientationData`", {
	"Classes`",
	"LieAnalysis`Objects`ObjWaveletTransform`"
}];


	(*|****************************|*)
  	(*| Declare Class and Defaults |*)
  	(*|****************************|*)
  	
  	
	DeclareClass[ObjWaveletTransform, ObjPositionOrientationData];
	DeclareDefaults[ObjPositionOrientationData, Association[
		"DcFilteredImage"-> 0,
		"Symmetry"-> Pi
	]];


	Begin["`Private`"];


	(*|****************************|*)
  	(*| Declare Invariants         |*)
  	(*|****************************|*)
  	
  	
  	DeclareInvariant[ObjPositionOrientationData,
  		(ArrayQ[#Data,3,NumberQ] || #Data == Automatic) /. False :> (Message[ValidQ::elfail, "Data", "ObjPositionOrientationData", "be a numeric 3D array"]; False)&];
  	
	DeclareInvariant[ObjPositionOrientationData,
		(MatrixQ[#DcFilteredImage] || #DcFilteredImage == 0) /. False :> (Message[ValidQ::elfail, "DcFilteredImage", "ObjPositionOrientationData", "be a matrix or zero"]; False)&];
		
	DeclareInvariant[ObjPositionOrientationData, 
		(MatchQ[#Symmetry, Pi | -Pi | 2Pi]) /. False :> (Message[ValidQ::elfail, "Symmetry", "ObjPositionOrientationData", "Should be Pi, -Pi or 2Pi"]; False)&];
	

	(*|**************************|*)
  	(*| Properties 		         |*)
  	(*|**************************|*)
  	
  	
	(* Construct 0-2pi orientation scores *)
	object_ObjPositionOrientationData["FullData"] := Switch[object[["Symmetry"]],
		Pi	, Join[object["Data"], Conjugate[object["Data"]],3],
		-Pi , Join[object["Data"], -Conjugate[object["Data"]],3],
		2Pi , object["Data"]
	];
	
	(* Real Full orientation scores *)
	object_ObjPositionOrientationData["FullData","Real"] := Re[object["FullData"]];
	
	(* Imaginairy full orientation scores *)
	object_ObjPositionOrientationData["FullData","Imaginairy"] := Im[object["FullData"]];

	(* Angular Resolution *)
	object_ObjPositionOrientationData["AngularResolution"] := Abs[object[["Symmetry"]] / object["NumberOfLayers"]];
	
	(* Orientation List *)
	object_ObjPositionOrientationData["OrientationList"] := Range[0,object[["Symmetry"]]-object["AngularResolution"],object["AngularResolution"]];
	
	(* Full Orientation List *)
	object_ObjPositionOrientationData["FullOrientationList"] := Range[0,2Pi-object["AngularResolution"],object["AngularResolution"]];
	
	(* Number of Orientations *)
	object_ObjPositionOrientationData["Orientations"] := Length[object["FullOrientationList"]];
	

	(*|****************************|*)
  	(*| Symmary Visualization      |*)
  	(*|****************************|*)
	
	
	ObjPositionOrientationData["Format"] = Function[{instance, fmt},Module[{alwaysGrid, sometimesGrid, head = Head[instance]},

		alwaysGrid = {
			BoxForm`MakeSummaryItem[{"Input Dimensions: ", instance["InputDimensions"]}, fmt],
			BoxForm`MakeSummaryItem[{"Orientations: ", ToString[instance["Orientations"]]<>" ("<>ToString[instance["NumberOfLayers"]]<>")"}, fmt],
			BoxForm`MakeSummaryItem[{"Symmetry: ", instance[["Symmetry"]]}, fmt],
			BoxForm`MakeSummaryItem[{"Wavelets: ", instance["WaveletType"]}, fmt],
			BoxForm`MakeSummaryItem[{"Is Complex: ", instance["ComplexQ"]}, fmt],
			BoxForm`MakeSummaryItem[{"Valid: ", ValidQ[instance]}, fmt]
		};

		sometimesGrid = {
			BoxForm`MakeSummaryItem[{"Elements: ", ElisionsDump`expandablePane[Style @@@ Normal @ KeySort @ Merge[{Thread[Keys[head["Defaults"]] -> Plain], Thread[(Select[Keys @@ instance, StringQ[#] (*&& StringStartsQ[#, "$"]*) &]) -> Bold]}, Prepend[#, Italic] &]]}, fmt],
			BoxForm`MakeSummaryItem[{"Properties: ", ElisionsDump`expandablePane[Style @@@ Normal @ KeySort @ Merge[{Thread[head["Properties", "Public"] -> Plain], Thread[(Select[Keys @@ instance, StringQ[#] && StringStartsQ[#, "$"] &]) -> Bold]}, Prepend[#, Italic] &]]}, fmt],
			BoxForm`MakeSummaryItem[{"ByteCount: ", ToString[ByteCount[instance]/10.^6]<>" MB"}, fmt]
		};

		BoxForm`ArrangeSummaryBox[
			head,
			SymbolName[head] <> "[<>]",
			ImageAdjust[Image[instance["Data","Real"]]],
			alwaysGrid,
			sometimesGrid,
			fmt,
			"Interpretable" -> False
		]

	]];


	(*|****************************|*)
  	(*| Bulletproofing		       |*)
  	(*|****************************|*)
  	
  	
	Protect[ObjPositionOrientationData];


	End[];


EndPackage[];