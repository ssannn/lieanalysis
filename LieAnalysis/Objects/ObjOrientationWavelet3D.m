(*************************************************************************************************************************
** ObjOrientationWavelet3D.m
** This .m file contains a class-definition for the ObjOrientationWavelet3D
**
** Author: F.C. Martin <f.c.martin@tue.nl>
**************************************************************************************************************************)
BeginPackage["LieAnalysis`Objects`ObjOrientationWavelet3D`", {
  	"Classes`",
  	"LieAnalysis`Objects`ObjWavelet`"
}];


	(*|****************************|*)
  	(*| Declare Class and Defaults |*)
  	(*|****************************|*)
  	
  	
	DeclareClass[ObjWavelet, ObjOrientationWavelet3D];
	DeclareDefaults[ObjOrientationWavelet3D, Association[
		"OrientationCoordinates"->Null,
		"Symmetry"->None,
		"OrientationList"->{}
	]];


  	Begin["`Private`"];


	(*|****************************|*)
  	(*| Declare Invariants         |*)
  	(*|****************************|*)
  	
  	
    DeclareInvariant[ObjOrientationWavelet3D, 
    	MatchQ[#OrientationCoordinates, {{_Real, _Real, _Real} ...} | Null] /. False :> (Message[ValidQ::elfail, "OrientationCoordinates", "be a list of 3D coordinates {p1,p2,...}"]; False)&];

    DeclareInvariant[ObjOrientationWavelet3D, 
    	MatchQ[#Symmetry, None | "AntiPodal" | "AntiPodalAntisymmetric"] /. False :> (Message[ValidQ::elfail, "Symmetry", "be one the following: None, AntiPodal or AntiPodalAntisymmetric"]; False)&];


	(*|**************************|*)
  	(*| Properties 		         |*)
  	(*|**************************|*)
  	
  	
    (* Returns Number of Orientations from 0-2pi *)
    object_ObjOrientationWavelet3D["Orientations"] := Switch[object[["Symmetry"]],
		"AntiPodal" , 2 object["NumberOfWavelets"],
      	None, object["NumberOfWavelets"],
      	_, "Unknown"
    ];

    (* Returns Full Orientation Wavelet Stack *)
    object_ObjOrientationWavelet3D["FullData"] := Switch[object[["Symmetry"]],
      	"AntiPodal" , Join[object[["Data"]], Conjugate[object[["Data"]]]],
      	"AntiPodalAntisymmetric" , Join[object[["Data"]], -Conjugate[object[["Data"]]]],
      	None , object[["Data"]]
    ];
	
    (* Returns Orientation List Symmetry Dependant *)
    object_ObjOrientationWavelet3D["OrientationList"] := object[["OrientationList"]];
    
    (* Returns Orientation List with Considering the Symmetry *)
    object_ObjOrientationWavelet3D["FullOrientationList"] := Switch[object[["Symmetry"]],
      	"AntiPodal" , Join[object[["OrientationList"]],-object[["OrientationList"]]],
      	"AntiPodalAntisymmetric" , Join[object[["OrientationList"]],-object[["OrientationList"]]],
      	None , object[["OrientationList"]]
    ];
    
    (* Returns the Polar Orientation List *)
    object_ObjOrientationWavelet3D["PolarOrientationList"] := Transpose[LieAnalysis`Common`CartesianToSphericalCoordinates[object[["OrientationList"]]]][[2;;3]];
    
    (* Returns the Polar Orientation List Considering the Symmetry *)
    object_ObjOrientationWavelet3D["FullPolarOrientationList"] := Transpose[LieAnalysis`Common`CartesianToSphericalCoordinates[object["FullOrientationList"]]][[2;;3]]; 
    
    (* Returns Topology *)
    object_ObjOrientationWavelet3D["Topology"] := If[object[["Symmetry"]]==="AntiPodal", Union[Sort /@Mod[object["FullTopology"],object["Orientations"]/2,1]],object["FullTopology"]];
    
    (* Returns Topolofy considering the Symmetry *)
    object_ObjOrientationWavelet3D["FullTopology"]:= MeshCells[ConvexHullMesh[object["FullOrientationList"]],2][[All,1]];

    (* Returns the Image *)
    object_ObjOrientationWavelet3D["Image"] := LieAnalysis`Visualization`QuickVisualize[object];


	(*|****************************|*)
  	(*| Symmary Visualization      |*)
  	(*|****************************|*)
	
	
    ObjOrientationWavelet3D["Format"] = Function[{instance, fmt},Module[{alwaysGrid, sometimesGrid, head = Head[instance]},

		alwaysGrid = {
			BoxForm`MakeSummaryItem[{"Orientations: ", instance["NumberOfWavelets"]}, fmt],
			BoxForm`MakeSummaryItem[{"WaveletSize: ", instance["WaveletSize"]}, fmt],
			BoxForm`MakeSummaryItem[{"Valid: ", ValidQ[instance]}, fmt]
		};

      	sometimesGrid = {
	        BoxForm`MakeSummaryItem[{"Elements: ", ElisionsDump`expandablePane[Style @@@ Normal @ KeySort @ Merge[{Thread[Keys[head["Defaults"]] -> Plain], Thread[(Select[Keys @@ instance, StringQ[#] (*&& StringStartsQ[#, "$"]*) &]) -> Bold]}, Prepend[#, Italic] &]]}, fmt],
	        BoxForm`MakeSummaryItem[{"Properties: ", ElisionsDump`expandablePane[Style @@@ Normal @ KeySort @ Merge[{Thread[head["Properties", "Public"] -> Plain], Thread[(Select[Keys @@ instance, StringQ[#] && StringStartsQ[#, "$"] &]) -> Bold]}, Prepend[#, Italic] &]]}, fmt],
	        BoxForm`MakeSummaryItem[{"ByteCount: ", ToString[ByteCount[instance]/10.^6]<>" MB"}, fmt]
      	};

		BoxForm`ArrangeSummaryBox[
			head,
	        SymbolName[head] <> "[<>]",
	        instance["Image"],
	        alwaysGrid,
	        sometimesGrid,
	        fmt,
	        "Interpretable" -> False
      	]

    ]];


	(*|****************************|*)
  	(*| Bulletproofing		       |*)
  	(*|****************************|*)
  	
  	
    Protect[ObjOrientationWavelet3D];


  End[];


EndPackage[];