(*************************************************************************************************************************
** ObjWavelet.m
** This .m file contains a class-definition for ObjWavelet
**
** Author: F.C. Martin <f.c.martin@tue.nl>
**************************************************************************************************************************)
BeginPackage["LieAnalysis`Objects`ObjWavelet`", {
	"Classes`",
	"LieAnalysis`Objects`Object`"
}];


	(*|****************************|*)
  	(*| Declare Class and Defaults |*)
  	(*|****************************|*)
  	
  	
	DeclareClass[Object, ObjWavelet];
	DeclareDefaults[ObjWavelet, Association[
			"PreSpatialFilters"	 -> Automatic
	]];


	Begin["`Private`"];


	(*|****************************|*)
  	(*| Declare Invariants         |*)
  	(*|****************************|*)
  	

	DeclareInvariant[ObjWavelet,
		(#PreSpatialFilters === Automatic || ArrayQ[#PreSpatialFilters]) /. False :> (Message[ValidQ::elfail, "PreSpatialFilters", "ObjWavelet", "automatic or an Array"];False;)&];  	
  	

	(*|**************************|*)
  	(*| Properties 		         |*)
  	(*|**************************|*)
  	
  	(* Returns the number of stored wavelets *)
	object_ObjWavelet["NumberOfWavelets"] := If[object[["Data"]] =!= Automatic, Length[object[["Data"]]],0];
	
	(* Returns the Spatial Dimensions of the Wavelets *)
	object_ObjWavelet["WaveletSize"] := If[object[["Data"]] =!= Automatic, Dimensions[ object[["Data"]]][[2;;-1]],0];
	
	(* Returns an Image from the Wavelet Stack *)
	object_ObjWavelet["Image"] := If[object[["Data"]] =!= Automatic, LieAnalysis`Visualization`QuickVisualize[object, ImageSize->100],Classes`Private`$ElisionsIcon];
	

	(*|****************************|*)
  	(*| Symmary Visualization      |*)
  	(*|****************************|*)
	
	
	ObjWavelet["Format"] = Function[{instance,fmt},Module[{alwaysGrid, sometimesGrid, head = Head[instance]},

		alwaysGrid = {
			BoxForm`MakeSummaryItem[{"Number of Wavelets: ", instance["NumberOfWavelets"]}, fmt],
			BoxForm`MakeSummaryItem[{"WaveletSize: ", instance["WaveletSize"]}, fmt],
			BoxForm`MakeSummaryItem[{"Valid: ", ValidQ[instance]}, fmt]
		};

		sometimesGrid = {
			BoxForm`MakeSummaryItem[{"Elements: ", ElisionsDump`expandablePane[Style @@@ Normal @ KeySort @ Merge[{Thread[Keys[head["Defaults"]] -> Plain], Thread[(Select[Keys @@ instance, StringQ[#] (*&& StringStartsQ[#, "$"]*) &]) -> Bold]}, Prepend[#, Italic] &]]}, fmt],
			BoxForm`MakeSummaryItem[{"Properties: ", ElisionsDump`expandablePane[Style @@@ Normal @ KeySort @ Merge[{Thread[head["Properties", "Public"] -> Plain], Thread[(Select[Keys @@ instance, StringQ[#] && StringStartsQ[#, "$"] &]) -> Bold]}, Prepend[#, Italic] &]]}, fmt],
			BoxForm`MakeSummaryItem[{"ByteCount: ", ToString[ByteCount[instance]/10.^6]<>" MB"}, fmt]
		};

		BoxForm`ArrangeSummaryBox[
			head,
			SymbolName[head] <> "[<>]",
			instance["Image"],
			alwaysGrid,
			sometimesGrid,
			fmt,
			"Interpretable" -> False
		]

	]];


	(*|****************************|*)
  	(*| Bulletproofing		       |*)
  	(*|****************************|*)
  	
  	
	Protect[ObjWavelet];


	End[];


EndPackage[];