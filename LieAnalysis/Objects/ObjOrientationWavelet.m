(*************************************************************************************************************************
** ObjOrientationWavelet.m
** This .m file describes a class-definition for ObjOrientationWavelet
**
** Author: T.C.J. Dela Haije <T.C.J.Dela.Haije@tue.nl>
** Author: F.C. Martin <f.c.martin@tue.nl>
**************************************************************************************************************************)
BeginPackage["LieAnalysis`Objects`ObjOrientationWavelet`", {
  "Classes`",
  "LieAnalysis`Objects`ObjWavelet`"
}];


  (* Declare Class and Defaults *)
  DeclareClass[ObjWavelet, ObjOrientationWavelet];
  DeclareDefaults[ObjOrientationWavelet, Association[
      "Directional"     -> False,
      "Symmetry"        -> Pi
    ]
  ];


	Begin["`Private`"];



    (* Declare Invariants *)
    DeclareInvariant[ObjOrientationWavelet, BooleanQ[#Directional] /. False :> (Message[ValidQ::elfail, "Directional", "be a boolean"]; False)&];
    DeclareInvariant[ObjOrientationWavelet, MatchQ[#Symmetry, Pi | -Pi | 2Pi ] /. False :> (Message[ValidQ::elfail, "Symmetry", "be either \[Pi], -\[Pi] or 2\[Pi]"]; False)&];

    (* Gives the number of orientations between 0-2pi *)
    object_ObjOrientationWavelet["Orientations"] := Switch[object[["Symmetry"]],
      Pi | -Pi , 2 object["NumberOfWavelets"],
      2Pi, object["NumberOfWavelets"],
      _, "Unknown"
    ];

    (* Computes the angular resultion *)
    object_ObjOrientationWavelet["AngularResolution"] := Abs[object[["Symmetry"]] / object["NumberOfWavelets"]];

    (* Construct 0-2pi orientation scores *)
    object_ObjOrientationWavelet["FullData"] := Switch[object[["Symmetry"]],
      Pi  , Join[object[["Data"]], Conjugate[object[["Data"]]]],
      -Pi , Join[object[["Data"]], -Conjugate[object[["Data"]]]],
      2Pi , object[["Data"]]
    ];
    
    object_ObjOrientationWavelet["OrientationList"] := Range[0,object[["Symmetry"]]-object["AngularResolution"],object["AngularResolution"]];

    (* Visualization of the object *)
    ObjOrientationWavelet["Format"] = Function[{instance, fmt},Module[{alwaysGrid, sometimesGrid, head = Head[instance]},

      alwaysGrid = {
        BoxForm`MakeSummaryItem[{"Orientations: ", instance["Orientations"]}, fmt],
        BoxForm`MakeSummaryItem[{"WaveletSize: ", instance["WaveletSize"]}, fmt],
        BoxForm`MakeSummaryItem[{"Symmetry: ", instance[["Symmetry"]]}, fmt],
        BoxForm`MakeSummaryItem[{"Directional: ", instance[["Directional"]]}, fmt],
        BoxForm`MakeSummaryItem[{"Valid: ", ValidQ[instance]}, fmt]
      };

      sometimesGrid = {
        BoxForm`MakeSummaryItem[{"Elements: ", ElisionsDump`expandablePane[Style @@@ Normal @ KeySort @ Merge[{Thread[Keys[head["Defaults"]] -> Plain], Thread[(Select[Keys @@ instance, StringQ[#] (*&& StringStartsQ[#, "$"]*) &]) -> Bold]}, Prepend[#, Italic] &]]}, fmt],
        BoxForm`MakeSummaryItem[{"Properties: ", ElisionsDump`expandablePane[Style @@@ Normal @ KeySort @ Merge[{Thread[head["Properties", "Public"] -> Plain], Thread[(Select[Keys @@ instance, StringQ[#] && StringStartsQ[#, "$"] &]) -> Bold]}, Prepend[#, Italic] &]]}, fmt],
        BoxForm`MakeSummaryItem[{"ByteCount: ", ToString[ByteCount[instance]/10.^6]<>" MB"}, fmt]
      };

      BoxForm`ArrangeSummaryBox[
        head,
        SymbolName[head] <> "[<>]",
        instance["Image"],
        alwaysGrid,
        sometimesGrid,
        fmt,
        "Interpretable" -> False
      ]

    ]];


    Protect[ObjOrientationWavelet];


  End[];



EndPackage[];





