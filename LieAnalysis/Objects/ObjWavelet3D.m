(*************************************************************************************************************************
** ObjWavelet.m
** This .m file contains common class definition ObjWavelet3D.
**
** Author: F.C. Martin <f.c.martin@tue.nl>
**************************************************************************************************************************)
BeginPackage["LieAnalysis`Objects`ObjWavelet3D`", {
	"Classes`",
	"LieAnalysis`Objects`Object`"
}];


	(*|****************************|*)
  	(*| Declare Class and Defaults |*)
  	(*|****************************|*)
  	
  	
	DeclareClass[Object, ObjWavelet3D];
	DeclareDefaults[ObjWavelet3D, Association[
		"WaveletSize" -> Automatic
	]];


	Begin["`Private`"];


	(*|****************************|*)
  	(*| Declare Invariants         |*)
  	(*|****************************|*)
  	
  	
	DeclareInvariant[ObjWavelet3D, 
		(Internal`PositiveIntegerQ[#WaveletSize] || #WaveletSize === Automatic) /. False :> (Message[ValidQ::elfail, "WaveletSize", "be a positive integer"])&];


	(*|****************************|*)
  	(*| Bulletproofing		       |*)
  	(*|****************************|*)
  	
  	
	Protect[ObjWavelet3D];


	End[];


EndPackage[];