(*************************************************************************************************************************
** ObjTensor.m
** This .m file contains class-defination for the ObjTensor
**
** Author: F.C. Martin <f.c.martin@tue.nl>
**************************************************************************************************************************)
BeginPackage["LieAnalysis`Objects`ObjTensor`", {
		"Classes`",
		"LieAnalysis`Objects`Object`"
}];


	(*|****************************|*)
  	(*| Declare Class and Defaults |*)
  	(*|****************************|*)
  	
  	
	DeclareClass[Object, ObjTensor];
	DeclareDefaults[ObjTensor,Association[
		"Labels" -> None,
		"OrientationScore" -> None,
		"Metadata" -> <||>,
		"SpatialDimensions"->None
	]];



	Begin["`Private`"];
	

	(*|****************************|*)
  	(*| Declare Invariants         |*)
  	(*|****************************|*)
  	
  	DeclareInvariant[ObjTensor,
		(#Data === Automatic || ArrayQ[#Data]) /. False :> (Message[ValidQ::elfail, "Data", "ObjTensor", "an array or Automatic"];False;)&];
	
	DeclareInvariant[ObjTensor,
		(#OrientationScore === None || ValidQ[#OrientationScore,LieAnalysis`Objects`ObjWaveletTransform`ObjWaveletTransform]) /. False :> (Message[ValidQ::elfail, "OrientationScore", "ObjTensor", "a valid ObjWaveletTransform"];False;)&]
	

	(*|**************************|*)
  	(*| Properties 		         |*)
  	(*|**************************|*)
  	
  	(* Returns the number of orientations in case an Orientation Score is used to construct the tensor *)
	 object_ObjTensor["Orientations"] := If[ValidQ[object[["OrientationScore"]],LieAnalysis`Objects`ObjPositionOrientationData`ObjPositionOrientationData], 
	 	Switch[object[["OrientationScore"]][["Symmetry"]],
			Pi | -Pi , 2 object[["OrientationScore"]][["Wavelets"]]["NumberOfWavelets"],
			2Pi, object[["OrientationScore"]][["Wavelets"]]["NumberOfWavelets"],
			_, "Unknown"
	 	],
	 	"Unknown"
	];
		

	(*|****************************|*)
  	(*| Symmary Visualization      |*)
  	(*|****************************|*)
	
	
	ObjTensor["Format"] = Function[{instance,fmt},Module[{alwaysGrid, sometimesGrid, head = Head[instance]},

		alwaysGrid = {
			BoxForm`MakeSummaryItem[{"Valid: ", ValidQ[instance]}, fmt],
			BoxForm`MakeSummaryItem[{"Labels: ", MatrixForm[instance[["Labels"]]] }, fmt],
			BoxForm`MakeSummaryItem[{"Spatial Dimensions: ", Dimensions[instance[["Data"]],instance[["SpatialDimensions"]]] }, fmt],
			BoxForm`MakeSummaryItem[{"Orientations: ",	ToString@instance["Orientations"]<> " ("<>ToString[Dimensions[instance[["Data"]]][[instance[["SpatialDimensions"]]+1]]]<>")" }, fmt]
		};

		sometimesGrid = {
			BoxForm`MakeSummaryItem[{"Metadata: ", Keys[instance[["Metadata"]]] }, fmt],
			BoxForm`MakeSummaryItem[{"Elements: ", ElisionsDump`expandablePane[Style @@@ Normal @ KeySort @ Merge[{Thread[Keys[head["Defaults"]] -> Plain], Thread[(Select[Keys @@ instance, StringQ[#] (*&& StringStartsQ[#, "$"]*) &]) -> Bold]}, Prepend[#, Italic] &]]}, fmt],
			BoxForm`MakeSummaryItem[{"Properties: ", ElisionsDump`expandablePane[Style @@@ Normal @ KeySort @ Merge[{Thread[head["Properties", "Public"] -> Plain], Thread[(Select[Keys @@ instance, StringQ[#] && StringStartsQ[#, "$"] &]) -> Bold]}, Prepend[#, Italic] &]]}, fmt],
			BoxForm`MakeSummaryItem[{"ByteCount: ", ToString[ByteCount[instance]/10.^6]<>" MB"}, fmt]
		};

		BoxForm`ArrangeSummaryBox[
			head,
			ToString[head] <> "[<>]",
			LieAnalysis`Visualization`QuickVisualize[instance],
			alwaysGrid,
			sometimesGrid,
			fmt,
			"Interpretable" -> False
		]

	]];
		

	(*|****************************|*)
  	(*| Bulletproofing		       |*)
  	(*|****************************|*)
  	
  	
	Protect[ObjTensor];


	End[];


EndPackage[];