(*************************************************************************************************************************
** ObjCakeWavelet3D.m
** This .m file contains a class-definition for the ObjCakeWavelet3D
**
** Author: F.C. Martin <f.c.martin@tue.nl>
**************************************************************************************************************************)
BeginPackage["LieAnalysis`Objects`ObjCakeWavelet3D`", {
  	"Classes`",
  	"LieAnalysis`Objects`ObjOrientationWavelet3D`"
}];


	(*|****************************|*)
  	(*| Declare Class and Defaults |*)
  	(*|****************************|*)
  	
  	
	DeclareClass[ObjOrientationWavelet3D, ObjCakeWavelet3D];
	DeclareDefaults[ObjCakeWavelet3D,Association[
		"DcFilter"                        -> Null,
		"RadialWindow"                    -> Null,
		"WaveletsF"                       -> Null,
		"LMax"                            -> 1,
		"SigmaAngle"                      -> 0.3,
		"DcStandardDeviation"             -> 8,
		"MnStandardDeviation"             -> Automatic,
		"Gamma"                           -> 0.6,
		"Epsilon"                         -> 10^(-5)
	]];



  	Begin["`Private`"];


	(*|****************************|*)
  	(*| Declare Invariants         |*)
  	(*|****************************|*)
  	
  	
    DeclareInvariant[ObjCakeWavelet3D, 
    	(MatchQ[#DcFilter,{{{__},__},__}] || #DcFilter === Null) /. False :> (Message[ValidQ::elfail, "DcFilter", "be a 3D Matrix"]; False)&];
    	
    DeclareInvariant[ObjCakeWavelet3D, 
    	Internal`PositiveIntegerQ[#LMax] /. False :> (Message[ValidQ::elfail, "LMax", "be a positive integer"]; False)&];
    	
    DeclareInvariant[ObjCakeWavelet3D, 
    	Positive[#SigmaAngle] /. False :> (Message[ValidQ::elfail, "SigmaAngle", "be a positive real number"]; False)&];
    	
    DeclareInvariant[ObjCakeWavelet3D, 
    	Positive[#DcStandardDeviation] /. False :> (Message[ValidQ::elfail, "DcStandardDeviation", "be a positive real number"]; False)&];
    	
    DeclareInvariant[ObjCakeWavelet3D, 
    	(Positive[#DcStandardDeviation] || #DcStandardDeviation === Automatic) /. False :> (Message[ValidQ::elfail, "MnStandardDeviation", "be a positive real number"]; False)&];
    	
    DeclareInvariant[ObjCakeWavelet3D, 
    	(#Gamma > 0 && #Gamma < 1) /. False :> (Message[ValidQ::elfail, "Gamma", "be a number between 0 and 1"]; False)&];
    	
    DeclareInvariant[ObjCakeWavelet3D, 
    	Positive[#Epsilon] /. False :> (Message[ValidQ::elfail, "Epsilon", "be a positive real number"]; False)&];


	(* ************************** *)
  	(* Properties 		          *)
  	(* ************************** *)


	(* Return the Image *)
	object_ObjCakeWavelet3D["Image"] := LieAnalysis`Visualization`QuickVisualize[object, ImageSize->100,Background->Black];
	  	

	(*|****************************|*)
  	(*| Symmary Visualization      |*)
  	(*|****************************|*)

    
    ObjCakeWavelet3D["Format"] = Function[{instance, fmt}, Module[{alwaysGrid, sometimesGrid, head = Head[instance]},

      alwaysGrid = {
        BoxForm`MakeSummaryItem[{"Orientations: ", ToString[instance["Orientations"]]<>" ("<>ToString[instance["NumberOfWavelets"]]<>")"}, fmt],
        BoxForm`MakeSummaryItem[{"WaveletSize: ", instance["WaveletSize"]}, fmt],
        BoxForm`MakeSummaryItem[{"Symmetry: ", instance[["Symmetry"]]}, fmt],
        BoxForm`MakeSummaryItem[{"Valid: ", ValidQ[instance]}, fmt]
      };

      sometimesGrid = {
        BoxForm`MakeSummaryItem[{"Elements: ", ElisionsDump`expandablePane[Style @@@ Normal @ KeySort @ Merge[{Thread[Keys[head["Defaults"]] -> Plain], Thread[(Select[Keys @@ instance, StringQ[#] (*&& StringStartsQ[#, "$"]*) &]) -> Bold]}, Prepend[#, Italic] &]]}, fmt],
        BoxForm`MakeSummaryItem[{"Properties: ", ElisionsDump`expandablePane[Style @@@ Normal @ KeySort @ Merge[{Thread[head["Properties", "Public"] -> Plain], Thread[(Select[Keys @@ instance, StringQ[#] && StringStartsQ[#, "$"] &]) -> Bold]}, Prepend[#, Italic] &]]}, fmt],
        BoxForm`MakeSummaryItem[{"ByteCount: ", ToString[ByteCount[instance]/10.^6]<>" MB"}, fmt]
      };

      BoxForm`ArrangeSummaryBox[
        head,
        SymbolName[head] <> "[<>]",
        instance["Image"],
        alwaysGrid,
        sometimesGrid,
        fmt,
        "Interpretable" -> False
      ]

    ]];


	(*|****************************|*)
  	(*| Bulletproofing		       |*)
  	(*|****************************|*)
  	
  	
    Protect[ObjCakeWavelet3D];


  End[];


EndPackage[];