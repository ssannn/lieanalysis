(*************************************************************************************************************************
** ObjScaleOrientationScores2D.m
** This .m file contains class-defination for the ObjOrientationScores2D
**
** Author: T.C.J. Dela Haije <T.C.J.Dela.Haije@tue.nl>
** Author: F.C. Martin <f.c.martin@tue.nl>
**************************************************************************************************************************)
BeginPackage["LieAnalysis`Objects`ObjScaleOrientationScores2D`", {
  "Classes`",
  "LieAnalysis`Objects`ObjPositionOrientationData`"
}];


  (* Declare Class and Defaults *)
  DeclareClass[ObjPositionOrientationData, ObjScaleOrientationScores2D];
  DeclareDefaults[ObjScaleOrientationScores2D, Association[]];



  Begin["`Private`"];

    (* Number of scales *)    
    object_ObjScaleOrientationScores2D["Scales"] := If[object[["Wavelets"]] =!= Null, object[["Wavelets"]][["Scales"]], "Unknown"];
    
    (* Angular Resolution *)
    object_ObjScaleOrientationScores2D["AngularResolution"] := If[IntegerQ[object["NumberOfLayers"]] &&IntegerQ[object["Scales"]],Abs[object[["Symmetry"]] / (object["NumberOfLayers"]/object["Scales"])], "Unknown"];
    
    (* Number of stored orientations *)
    object_ObjScaleOrientationScores2D["StoredOrientations"] := If[IntegerQ[object["Scales"]] && ArrayQ[object["Data"]],(Last@Dimensions[object["Data"]])/object["Scales"],"Unknown"];

	(* Number of Orientations from 0 - 2pi *)    
    object_ObjScaleOrientationScores2D["Orientations"] := If[ArrayQ[object["FullData"]] && IntegerQ[object["Scales"]],Last@Dimensions[object["FullData"]]/object["Scales"],"Unknown"];

	(* Returns the orientations used from 0-symmetry *)
    object_ObjScaleOrientationScores2D["OrientationList"] := Range[0,object[["Symmetry"]]-object["AngularResolution"],object["AngularResolution"]];
    
    (* Visualization of the object *)
    ObjScaleOrientationScores2D["Format"] = Function[{instance, fmt},Module[{alwaysGrid, sometimesGrid, head = Head[instance]},

      alwaysGrid = {
        BoxForm`MakeSummaryItem[{"Input Dimensions: ", instance["InputDimensions"]}, fmt],
        BoxForm`MakeSummaryItem[{"Orientations: ", ToString[instance["Orientations"]]<>" ("<>ToString[instance["StoredOrientations"]]<>")"}, fmt],
        BoxForm`MakeSummaryItem[{"Scales: ", instance["Scales"]}, fmt],
        BoxForm`MakeSummaryItem[{"Symmetry: ", instance[["Symmetry"]]}, fmt],
        BoxForm`MakeSummaryItem[{"Wavelets: ", instance["WaveletType"]}, fmt],
        BoxForm`MakeSummaryItem[{"Valid: ", ValidQ[instance]}, fmt]
      };

      sometimesGrid = {
        BoxForm`MakeSummaryItem[{"Elements: ", ElisionsDump`expandablePane[Style @@@ Normal @ KeySort @ Merge[{Thread[Keys[head["Defaults"]] -> Plain], Thread[(Select[Keys @@ instance, StringQ[#] (*&& StringStartsQ[#, "$"]*) &]) -> Bold]}, Prepend[#, Italic] &]]}, fmt],
        BoxForm`MakeSummaryItem[{"Properties: ", ElisionsDump`expandablePane[Style @@@ Normal @ KeySort @ Merge[{Thread[head["Properties", "Public"] -> Plain], Thread[(Select[Keys @@ instance, StringQ[#] && StringStartsQ[#, "$"] &]) -> Bold]}, Prepend[#, Italic] &]]}, fmt],
        BoxForm`MakeSummaryItem[{"ByteCount: ", ToString[ByteCount[instance]/10.^6]<>" MB"}, fmt]
      };

      BoxForm`ArrangeSummaryBox[
        head,
        SymbolName[head] <> "[<>]",
        ImageAdjust[Image[instance[["Data"]]]],
        alwaysGrid,
        sometimesGrid,
        fmt,
        "Interpretable" -> False
      ]

    ]];


    Protect[ObjScaleOrientationScores2D];


  End[];


EndPackage[];