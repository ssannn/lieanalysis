(*************************************************************************************************************************
** ObjScaleCakeWavelet.m
** This .m file contains a class-definition for the ObjScaleCakeWavelet
**
** Author: F.C. Martin <f.c.martin@tue.nl>
**************************************************************************************************************************)
BeginPackage["LieAnalysis`Objects`ObjScaleCakeWavelet`", {
  	"Classes`",
  	"LieAnalysis`Objects`ObjCakeWavelet`"
}];


	DeclareClass[ObjCakeWavelet, ObjScaleCakeWavelet];
	DeclareDefaults[ObjScaleCakeWavelet, Association[
		"Scales"->1,
		"rhoMin"->.1,
		"rhoMax"->.7
	]];


	Begin["`Private`"];
	

	(*|****************************|*)
  	(*| Declare Invariants         |*)
  	(*|****************************|*)

		
	DeclareInvariant[ObjScaleCakeWavelet,
		(IntegerQ[#Scales]) /. False :> (Message[ValidQ::elfail, "Scales", "ObjScaleCakeWavelet", "a positive integer."])&];
		
	DeclareInvariant[ObjScaleCakeWavelet,
		(NumberQ[#rhoMin] && #rhoMin > 0 && #rhoMin < #rhoMax) /. False :> (Message[ValidQ::elfail, "rhoMin", "ObjScaleCakeWavelet", "a number between 0 and 1 but smaller than rhoMax."];False;)&];
		
	DeclareInvariant[ObjScaleCakeWavelet,
		(NumberQ[#rhoMax] && #rhoMax > #rhoMin && #rhoMax < 1 ) /. False :> (Message[ValidQ::elfail, "rhoMax", "ObjScaleCakeWavelet", "a number between 0 and 1 and bigger than rhoMin."];False;)&]
  		

	(*|**************************|*)
  	(*| Properties 		         |*)
  	(*|**************************|*)
  	
  	
	object_ObjScaleCakeWavelet["Scales"] := object[["Scales"]];(*First@Dimensions[object[["Data"]],1];*)
	object_ObjScaleCakeWavelet["StoredOrientations"] := (First@Dimensions[object[["Data"]],1])/object[["Scales"]];
	object_ObjScaleCakeWavelet["Orientations"] := Switch[ object[["Symmetry"]],
		-Pi | Pi, 2 object["StoredOrientations"],
		2pi, object["StoredOrientations"],
		_, "Unknown"
	];
	object_ObjScaleCakeWavelet["Image"] := If[object[["Data"]] =!= Null, ImageAdjust@Image[Re@(Transpose[object[["Data"]][[1;;-1;;object["StoredOrientations"]]],{3,1,2}]),ImageSize->100]];
	
	object_ObjScaleCakeWavelet["LogScaleStepSize"] := (1/( object[["Scales"]] - 1)) * Log[ object[["rhoMax"]] / object[["rhoMin"]] ];
	
	object_ObjScaleCakeWavelet["SpatialScales"] := Table[(1-object[["rhoMin"]]) * E^(l * object["LogScaleStepSize"] ) ,{l,0,object[["Scales"]]-1}];

  	ObjScaleCakeWavelet["Format"] = Function[{instance, fmt},
	    Module[{alwaysGrid, sometimesGrid, head = Head[instance]},
	
		    alwaysGrid = {
		      BoxForm`MakeSummaryItem[{"Design: ", Subscript["\[CurlyPhi]", instance[["Design"]]]}, fmt],
		      BoxForm`MakeSummaryItem[{"Orientations: ", ToString[instance["Orientations"]]<>" ("<>ToString[instance["StoredOrientations"]]<>")"}, fmt],
		      BoxForm`MakeSummaryItem[{"Scales: ", instance["Scales"]}, fmt],
		      BoxForm`MakeSummaryItem[{"WaveletSize: ", instance["WaveletSize"]}, fmt],
		      BoxForm`MakeSummaryItem[{"Symmetry: ", instance[["Symmetry"]]}, fmt],
		      BoxForm`MakeSummaryItem[{"Directional: ", instance[["Directional"]]}, fmt],
		      BoxForm`MakeSummaryItem[{"Valid: ", ValidQ[instance]}, fmt]
		    };
		
		    sometimesGrid = {
		      BoxForm`MakeSummaryItem[{"Elements: ", ElisionsDump`expandablePane[Style @@@ Normal @ KeySort @ Merge[{Thread[Keys[head["Defaults"]] -> Plain], Thread[(Select[Keys @@ instance, StringQ[#] (*&& StringStartsQ[#, "$"]*) &]) -> Bold]}, Prepend[#, Italic] &]]}, fmt],
		      BoxForm`MakeSummaryItem[{"Properties: ", ElisionsDump`expandablePane[Style @@@ Normal @ KeySort @ Merge[{Thread[head["Properties", "Public"] -> Plain], Thread[(Select[Keys @@ instance, StringQ[#] && StringStartsQ[#, "$"] &]) -> Bold]}, Prepend[#, Italic] &]]}, fmt],
		      BoxForm`MakeSummaryItem[{"ByteCount: ", ToString[ByteCount[instance]/10.^6]<>" MB"}, fmt]
		    };
		
		    BoxForm`ArrangeSummaryBox[
		      head,
		      SymbolName[head] <> "[<>]",
		      instance["Image"],
		      alwaysGrid,
		      sometimesGrid,
		      fmt,
		      "Interpretable" -> False
		    ]
		]
	];

	Protect[ObjScaleCakeWavelet];

End[];


EndPackage[];