(*************************************************************************************************************************
** Object.m
** This .m file contains a class-definition for the Object
**
** Author: F.C. Martin <f.c.martin@tue.nl>
**************************************************************************************************************************)
BeginPackage["LieAnalysis`Objects`Object`", {
  "Classes`"
  }
];



  (* Declare Class and invariants *)
  DeclareClass[Object];
  DeclareDefaults[Object, Association[
      "Data" -> Automatic,
      "SpecifiedOptions" -> {}
  ]];



	Begin["`Private`"];

	    (* Declare Incvariants *)
		(*DeclareInvariant[Object, (VectorQ[Flatten@#SpecifiedOptions, MatchQ[#, _Rule | {} ] &]) /. False :> (Message[ValidQ::elfail, "SpecifiedOptions", "Object", "be a array of rules"]; False)&];*)
		
		object_Object["Data"] := object[["Data"]];

	    (* Visualization of the object *)
	    Object["Format"] = Function[{instance, fmt},

      	Module[{head = Head[instance], alwaysGrid, sometimesGrid},

	        alwaysGrid = {
	          	BoxForm`MakeSummaryItem[{"Base: ", head["Base"]}, fmt],
	          	BoxForm`MakeSummaryItem[{"Valid: ", ValidQ[instance]}, fmt]
	        };

	        sometimesGrid = {
				BoxForm`MakeSummaryItem[{"Parents: ", ElisionsDump`expandablePane[head["Parents"]]}, fmt],
				BoxForm`MakeSummaryItem[{"Elements: ", ElisionsDump`expandablePane[Style @@@ Normal @ KeySort @ Merge[{Thread[Keys[head["Defaults"]] -> Plain], Thread[(Select[Keys @@ instance, StringQ[#] (*&& StringStartsQ[#, "$"]*) &]) -> Bold]}, Prepend[#, Italic] &]]}, fmt],
				BoxForm`MakeSummaryItem[{"Properties: ", ElisionsDump`expandablePane[Style @@@ Normal @ KeySort @ Merge[{Thread[head["Properties", "Public"] -> Plain], Thread[(Select[Keys @@ instance, StringQ[#] && StringStartsQ[#, "$"] &]) -> Bold]}, Prepend[#, Italic] &]]}, fmt],
				BoxForm`MakeSummaryItem[{"ByteCount: ", ToString[ByteCount[instance]/10.^6]<>" MB"}, fmt]
	        };

        	BoxForm`ArrangeSummaryBox[
          		head,
          		SymbolName[head] <> "[<>]", (*Ideally this should be adapted in the output form when BoxForm`UseTextFormattingQ is True.*)
          		Classes`Private`$ElisionsIcon,
          		alwaysGrid,
          		sometimesGrid,
          		fmt,
          		"Interpretable" -> False
        	]
      	]
    ];



    Protect[Object];



  End[];



EndPackage[];