(*************************************************************************************************************************
** ExponentialCurveFitting.m
** This .m file contains functions to fit exponential curves to the SE(2) space
** 
** Author: E.J. Bekkers <e.j.bekkers@tue.nl>
** Author: F.C. Martin <f.c.martin@tue.nl>
**************************************************************************************************************************)
BeginPackage["LieAnalysis`OrientationScores2D`GaugeFrames`ExponentialCurveFitting`",{
	"LieAnalysis`"
}];




	Begin["`Private`"];



		Needs["Classes`"];		
		Get["LieAnalysis`Common`"];
		 (*TODO: Remove *)
		EigenvectorsPosDef=Compile[{{mat3by3,_Real,2}},

With[
{
a=mat3by3[[1,1]],
b=mat3by3[[1,2]],
c=mat3by3[[1,3]],
d=mat3by3[[2,2]],
e=mat3by3[[2,3]],
f=mat3by3[[3,3]]
}

,

Module[
{
c0=a e^2+d c^2+f b^2-a d f-2 b c e,
c1=a d+a f+d f-(b^2+c^2+e^2),
c2=a+d+f,
p,q,phi,v1,v2,v3,l1,l2,l3
}
,
p=c2^2-3 c1;
q=-27/2 c0-c2 (9/2 c1-c2^2);
phi=With[{mask=Unitize[p]},1/3 ArcTan[q+(1-mask),Sqrt[Abs[27 (1/4 c1^2 (p-c1)+c0 (q+27/4 c0))]]]];

(*eigenvalues*)
l1=Sqrt[p]/3 2 Cos[phi]+c2/3;
l2=Sqrt[p]/3 (-Cos[phi]+Sqrt[3] Sin[phi])+c2/3;
l3=Sqrt[p]/3 (-Cos[phi]-Sqrt[3] Sin[phi])+c2/3;

{l3, l2, l1} = {l1, l2, l3}[[Ordering[Abs[{l1, l2, l3}]]]];(*Sort values from large to small*)

(*eigenvectors*)
{v1,v2,v3}={
{b e-c d+c l1,c b-e a+e l1,(a-l1) (d-l1)-b^2},
{b e-c d+c l2,c b-e a+e l2,(a-l2) (d-l2)-b^2},
{b e-c d+c l3,c b-e a+e l3,(a-l3) (d-l3)-b^2}
};

(*Normalize*)
	{
		v1/Sqrt[v1[[1]]^2+v1[[2]]^2+v1[[3]]^2],
		v2/Sqrt[v2[[1]]^2+v2[[2]]^2+v2[[3]]^2],
		v3/Sqrt[v3[[1]]^2+v3[[2]]^2+v3[[3]]^2]	
	}
]
],RuntimeAttributes->{Listable}
];
		
		Options[OrientationScoreGaugeFrames] = {
			"Xi" -> Automatic, (* TODO: this parameter name should change *)
			"Tensor"->"Hessian"
		};
		
		OrientationScoreGaugeFrames::invalidOptVal = "Unknown option value `1` for \"FrameBasis\", \"LeftInvariant\" is used instead. Valid option values are \"LeftInvariant\" (default) and \"Cartesian\".";
		
		OrientationScoreGaugeFrames[objTensor_LieAnalysis`Objects`ObjTensor`ObjTensor /; ValidQ[objTensor, LieAnalysis`Objects`ObjTensor`ObjTensor], sigmas_:{0,0}, opts:OptionsPattern[]] := Module[
		
			(* Local Variable Declaration *)	
			{
				data = objTensor[["Data"]],
				xi = OptionValue["Xi"],
				meta = objTensor[["Metadata"]],
				dim = Dimensions[objTensor[["Data"]]],
				orientations = objTensor[["OrientationScore"]]["FullOrientationList"],
				MxiInv, scaledHessian, symmetricHessian,v1,v2,v3,c1,c2,c3, Chi, Nu, frames, Rc  
			},
			
			(* TODO: Check dimensions of the tensor, should be the hessian *)
			
			(* Set xi parameter *)
			xi = If[xi === Automatic, 
				
					(* Test that the keys exist *)
					If[KeyExistsQ[meta, "SigmaOrientation"] && KeyExistsQ[meta, "SigmaSpatial"],
						(meta["SigmaOrientation"])/(meta["SigmaSpatial"])
						,
						Message[General::argfail, "objTensor","containing SigmaOrientation and SigmaSpatial in the Metadata, you can specify the xi parameter by setting the option \"Xi\""];
						Abort[];					
					]
				
				, (* Else *)
				
					xi 
			];
			
			(* Rescale Hessian *)
			MxiInv = DiagonalMatrix[{xi^-1, xi^-1, 1}];
			scaledHessian = Map[MxiInv.#.MxiInv &, data, {3}];
			
			(* Apply External Regularization *)
			If[sigmas =!= {0,0},
				scaledHessian = LieAnalysis`Common`ExternalRegularization[scaledHessian, orientations, sigmas];
			];
			
			(* Symmetrize Hessian Matrix *)
			symmetricHessian = Map[Transpose[#].# &, scaledHessian, {3}];
			
			(* Compute Eigen Vectors *)
			{v1,v2,v3} = Transpose[EigenvectorsPosDef[Re@symmetricHessian],{3,4,5,1,2}];
			{c1,c2,c3} = v3;(*Vector 3 is the eigenvector with smallest eigenvalue*)

			(* Horizontality angle *)
			Chi = Mod[ArcTan[c1, c2],Pi,-Pi/2]; (*TODO: maybe better to do this with the arg of a complex *)
			
			(* Spherical angle *)
			Nu = ArcTan[Sqrt[c1^2 + c2^2], c3 Sign[c1]];

			
			(* Rotation Matrices (transposed and combined)*)
			Rc =Transpose[Flatten[Transpose[Transpose@{
				{Cos[Nu]*Cos[Chi],  -Sin[Chi], Cos[Chi]*Sin[Nu]}, 
				{Cos[Nu]*Sin[Chi], Cos[Chi], Sin[Nu]*Sin[Chi]}, 
				{-Sin[Nu], ConstantArray[0,Dimensions[Nu]], Cos[Nu]}
			},{4,5,1,2,3}],1],{2,1,3,4}];
			
			(* Assemble Gauge frames *)
			frames = Table[
				(#.MxiInv)&/@Rc[[angle]],
				{angle,1,objTensor[["OrientationScore"]]["NumberOfLayers"]}
			];
			
			frames = ArrayReshape[Transpose[frames,{2,1,3,4}],dim];
			
			(* Return Frames *)
			Return[frames];
			
		]
		
		
		
		OrientationScoreGaugeFrames[objOst_LieAnalysis`Objects`ObjPositionOrientationData`ObjPositionOrientationData /; ValidQ[objOst, LieAnalysis`Objects`ObjPositionOrientationData`ObjPositionOrientationData], {sigmaSpatial_,sigmaOrientation_,sigmaSpatialEx_:0,sigmaOrientationEx_:0}, opts:OptionsPattern[]] := Module[
			
			(* Local Variable Declaration *)
			{
				tensorObj, frames
			},
			
			(* Compute Hessian Tensor from Orientation Score *)
			tensorObj = OrientationScoreTensor[objOst, {sigmaSpatial, sigmaOrientation}, OptionValue["Tensor"]];
			
			(* Call Mainfunction *)	
			frames  = OrientationScoreGaugeFrames[tensorObj,{sigmaSpatialEx, sigmaOrientationEx} ,opts];
			
			(* Return Gauge Frame *)
			Return[frames];
			
		]
		
		


	
	End[];
	


EndPackage[]