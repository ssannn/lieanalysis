(*************************************************************************************************************************
** Construction.m
** This .m file contains functions to construct a wavelet transform using different kind of filters
** 
** Author: E.J. Bekkers <e.j.bekkers@tue.nl>
** Author: F.C. Martin <f.c.martin@tue.nl>
**************************************************************************************************************************)
BeginPackage["LieAnalysis`OrientationScores2D`Construction`",{
	"LieAnalysis`"
}];

	Begin["`Private`"];

		Get["LieAnalysis`Common`"];
		Needs["Classes`"];

		(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
		(* WaveletTransform- - - - - - - - - - - - - - - - - - - - - *)
		(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)


		WaveletTransform[img_, waveletObj_ /; ValidQ[waveletObj,LieAnalysis`Objects`ObjWavelet`ObjWavelet]] := Module[
    		(* Local Varialble Declaration *)
			{wt2d, waveletTransformObj},
			(* Compute the Tranform stack *)
			wt2d = WaveletTransform[img, waveletObj[["Data"]]];
			
			(* Store Result *)
			waveletTransformObj = LieAnalysis`Objects`ObjWaveletTransform`ObjWaveletTransform[{
				"Data"->wt2d,
				"InputData"->img,
				"Wavelets"->waveletObj
			}];
			(* Return the Wavelet transform *)
			Return[waveletTransformObj]
			
		];
		(* If an image is inputted *)
		WaveletTransform[img_/;ArrayQ[img], kernels_List] := 
			WaveletTransform[LieAnalysis`Common`ToImage[img],kernels]		
		(* In case of Complex wavelets *)
		WaveletTransform[img_, kernels_List /; (Re[kernels] =!= kernels)] := 			(#1+ I#2)&@@Map[WaveletTransform[img, #]&,{Re[kernels], -Im[kernels]}];
  		(* Wolfram Laungage Implementation *)
		WaveletTransform[img_ /; (ImageQ[img] && !OptionValue[LieAnalysis,"UseCUDA"]), kernels_List /; (Re[kernels] === kernels)] := Module[
			(* Local Varialble Declaration *)
			{stack},
      		(* Compute list of orientation score images *)
      		stack = Map[ImageConvolve[img, #, Padding->"Reversed"]&,kernels];
      		(* Smash them into a single image with colorchannels *)
      		stack = ImageData[ColorCombine[stack]];
		  	
		  	(* Return the multi-space *)
			Return[stack]
		];
				(* CUDA Implementation *)
		WaveletTransform[img_ /; (OptionValue[LieAnalysis,"UseCUDA"]), kernels_List] := Module[

			{CUDAWaveletTransform, image, no, waveletSize, width, height, osIm, osRe, os, kernelsFlat},

			(* Load CUDA function OrientationScoreTransform *)
			CUDAWaveletTransform = LieAnalysis`Common`LoadCUDAFunction["orientationScoreTransform"];

			(* Process the input image *)
			image = LieAnalysis`Common`InputProcessor[img];
			
			(* Prepare input *)
			{no, waveletSize} = Dimensions[kernels,2];
			{width, height} = Dimensions[image];
      		image = Flatten[image];
      		kernelsFlat = Flatten[kernels];

      		(* CUDA Implementation *)
      		osRe = CUDAWaveletTransform[image, width, height, Re@kernelsFlat, no, (waveletSize - 1) / 2 ];
      		osIm = CUDAWaveletTransform[image, width, height, -Im@kernelsFlat, no, (waveletSize - 1) / 2 ];
			os = osRe + I osIm;

      		(* Reshape Array *)
      		os = ArrayReshape[os, {no, width, height}];

      		(* Reorder to x,y,I *)
      		os = LieAnalysis`Common`Reorder[os, "SpatialFirst"];

      		(* Strip List if only one kernel is provided *)
			If[Length[kernels] == 1, os = os[[All,All,1]]];

		  	(* Return the multi-space *)
			Return[os]

		];
		(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
		(* OrientationScoresTransform- - - - - - - - - - - - - - - - *)
		(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)


    	OrientationScoreTransform::invalidInput = "The supplied value '`1`' for option `2` is invalid. (`3`)";

		Options[OrientationScoreTransform] = DeleteDuplicates@Join[Options[CakeWaveletStack], Options[GaborWaveletStack], Options[MultiScaleCakeWavelets],{
			"WaveletPreProcess"->Re,
			"Type"->"Cake",
			"Orientations"->12, (* The number of orientations to construct the Oriented WaveletStack *)
			"Scale"->1, (* The gabor scale parameter *)
			"rhoMax"->0.9, (* The settings for multiscale wavelets *)
			"rhoMin"->0.1, (* The settings for multiscale wavelets *)
			"Scales"->4, (* The settings for multiscale wavelets *)
			"WaveletSize"->75
		}];

		OrientationScoreTransform[img_, cwsObj_/;(ValidQ[cwsObj, LieAnalysis`Objects`ObjOrientationWavelet`ObjOrientationWavelet]),OptionsPattern[]] := Module[
			(* Local Variables Declaration *)
			{image, cws = cwsObj, wtObj, osObj, dcFilteredImage, f = OptionValue["WaveletPreProcess"]},
			(* Pre-process wavelets *)
			cws = Affix[cws, "Data"->f[cws[["Data"]]]];
			
    		(* Process the input image *)
      		image = LieAnalysis`Common`InputProcessor[img];
      		(* Create the Orientation Score *)
      		wtObj = WaveletTransform[image, cws];
			(* Select appropiate method per filters *)
			Switch[Head[cws],
				LieAnalysis`Objects`ObjCakeWavelet`ObjCakeWavelet,        			wtObj[[0]] = ClassSet[wtObj[[0]],LieAnalysis`Objects`ObjPositionOrientationData`ObjPositionOrientationData]; (* Convert to OS2D child *)
					osObj = wtObj;
            		dcFilteredImage = WaveletTransform[image, {cws[["DcFilter"]]}];
            		osObj = Affix[osObj, "DcFilteredImage"->dcFilteredImage];,
				LieAnalysis`Objects`ObjGaborWavelet`ObjGaborWavelet,
        			wtObj[[0]] = ClassSet[wtObj[[0]],LieAnalysis`Objects`ObjPositionOrientationData`ObjPositionOrientationData];
        			osObj = wtObj;,
        		LieAnalysis`Objects`ObjScaleCakeWavelet`ObjScaleCakeWavelet,
        			wtObj[[0]] = ClassSet[wtObj[[0]],LieAnalysis`Objects`ObjScaleOrientationScores2D`ObjScaleOrientationScores2D];
        			osObj = wtObj;,
				_,
        			wtObj[[0]] = ClassSet[wtObj[[0]],LieAnalysis`Objects`ObjPositionOrientationData`ObjPositionOrientationData];
            		osObj = wtObj;
			];
			(* Re-arrange to x,y,theta and set symmetry property *)
			osObj = Affix[osObj, {
				"Symmetry"->cws[["Symmetry"]]
			}];
			(* Return the Orientation Score *)
      		Return[osObj]
		];
		OrientationScoreTransform[img_, opts:OptionsPattern[]] := Module[
			(* Local Variables Declaration*)
			{
				type = OptionValue["Type"],
				scale = OptionValue["Scale"],
        		orientations = OptionValue["Orientations"],
        		size = OptionValue["WaveletSize"],
        		nScales = OptionValue["Scales"],
        		rhoMax = OptionValue["rhoMax"],
        		rhoMin = OptionValue["rhoMin"],
				wsObj, osObj
			},

			(* Input validation *)
			And[
				MatchQ[type, "Cake" | "Gabor" | "MultiScaleCake"]
					/. False :> (Message[OrientationScoreTransform::invalidInput, ToString[type], "Type", "Should be either \"Cake\", \"Gabor\", or \"MultiScaleCake\" "]; False),
				Internal`PositiveIntegerQ[scale]
					/. False :> (Message[OrientationScoreTransform::invalidInput, ToString[scale], "Scale", "Should be a positive integer"]; False),
				Internal`PositiveIntegerQ[orientations]
					/. False :> (Message[OrientationScoreTransform::invalidInput, ToString[orientations], "Orientations", "Should be a positive integer"]; False),
				Internal`PositiveIntegerQ[size]
					/. False :> (Message[OrientationScoreTransform::invalidInput, ToString[size], "WaveletSize", "Should be a positive integer"]; False)
			] /. False :> Abort[];
			(* Construct the Selected Wavelets *)
			Switch[type,

				"Gabor",
      				(* Construct Gabor Wavelet Stack *)
        			wsObj = GaborWaveletStack[orientations, scale, size, FilterRules[{opts}, Options[GaborWaveletStack]]]
        		,
				"MultiScaleCake",
					wsObj = MultiScaleCakeWaveletStack[orientations, size, {rhoMin, rhoMax, nScales}, FilterRules[{opts}, Options[MultiScaleCakeWaveletStack]]]
				,
				"Cake" | _,
					(* Compute Cake Wavelet Stack*)
					wsObj = CakeWaveletStack[orientations, size, FilterRules[{opts}, Options[CakeWaveletStack]]]
			];
			(* Compute orientation score transform *)
			osObj = OrientationScoreTransform[img, wsObj,opts];
			(* Append the used options to the object *)
			AffixTo[osObj,"SpecifiedOptions"->{opts}];
			(* Return the Orientation Score and metaData *)
			Return[osObj]
		];


	End[];


EndPackage[];