(*************************************************************************************************************************
** Reconstruction.m
** This .m file contains functions to reconstruct an image from an 2D Orientation Score
**
** Author: E.J. Bekkers <e.j.bekkers@tue.nl>
** Author: F.C. Martin <f.c.martin@tue.nl>
** Version: 0.1
**************************************************************************************************************************)
BeginPackage["LieAnalysis`OrientationScores2D`Reconstruction`",{
	"LieAnalysis`"
}];



	Begin["`Private`"];

		Needs["Classes`"];
    	Get["LieAnalysis`Common`"];
		(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
		(* InverseOrientationTransform2D - - - - - - - - - - - - - - *)
		(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)



		Options[InverseOrientationScoreTransform] = {
			"Method" -> "Summation",
      		"UseDcComponent" -> True
		};



		InverseOrientationScoreTransform::invalidInput = "The supplied value '`1`' for option `2` is invalid. (`3`)";



		InverseOrientationScoreTransform[ostObjIn_/;ValidQ[ostObjIn,LieAnalysis`Objects`ObjWaveletTransform`ObjWaveletTransform],OptionsPattern[]] := Module[

			(* Local Variables Declaration *)
			{
				image,
				method = OptionValue["Method"],
        		useDC = OptionValue["UseDcComponent"],
        		ostObj = ostObjIn
			},

			(* Check Method in combination with the construction method *)
			And[

				MatchQ[method, "Summation" | "L2" | "Exact" | "RknAdjoint" | "L2adjoint" ]
					/. False :> (Message[InverseOrientationScoreTransform::invalidInput, ToString[method], "Method", "Should be either \"RknAdjoint\", \"L2adjoint\" or \"Summation\""]; False),

        		BooleanQ[useDC]
            		/. False :> (Message[InverseOrientationScoreTransform::invalidInput, ToString[useDC],"UseDcComponent", "Should be a boolean value"]; False)

			] /. False :> Abort[];

			(* Remove DC-component *)
			If[!useDC,
				ostObj = Affix[ostObj, "DcFilteredImage"->ConstantArray[0, Dimensions[ostObj["Data"][[All,All,1]]]]];
			];

			(* Reconstruct image according to specified method *)
			image = Switch[method,
				"Exact" | "RknAdjoint", Exact[ostObj],
				"L2" | "L2adjoint", L2Adjoint[ostObj],
				"Summation" | _, Summation[ostObj]
			];

			(* Return the re-constructed image *)
			Return[Image[image,"Real32"]];

		];



		(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
		(* Summation - - - - - - - - - - - - - - - - - - - - - - - - *)
		(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)



		Summation[ostObj_] := Module[

			(* Local Variables Declaration *)
			{reconstruction},

			(* Integrate in theta direction and add the DC component *)
			reconstruction = Plus@@Reorder[ostObj["FullData","Real"],"OrientationFirst"] + ostObj[["DcFilteredImage"]];

			(* Return Reconstruction *)
			Return[reconstruction];
		];



		(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
		(* L2Adjoint - - - - - - - - - - - - - - - - - - - - - - - - *)
		(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)



		L2Adjoint[ostObj_] := Module[

			(* Local Variables Declaration *)
			{os, image, convMultiSpace},

			(* Reorder the OS *)
      		os = Reorder[ostObj["FullData","Real"], "OrientationFirst"];

			(* Convolve the layers once more with the kernel *)
      		convMultiSpace = MapThread[WaveletTransform[#1, {Re[#2]}] &, {os, ostObj[["Wavelets"]]["FullData"]}];

			(* Integrate in theta direction and add the dc-component *)
			image = Plus@@convMultiSpace + ostObj[["DcFilteredImage"]];

			(* Return the reconstructed image *)
			Return[image]

		];



		(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
		(* Exact - - - - - - - - - - - - - - - - - - - - - - - - - - *)
		(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)



		Exact[ostObj_, threshold_:10^(-4)] := Module[

			{os, image, convMultiSpace, Mpsi, TMpsi, MphiInv,cakeF},

    		(* Reorder the OS *)
      		os = Reorder[ostObj["FullData","Real"], "OrientationFirst"];

			(* Convolve all layers again with their corresponding wavelets *)
      		convMultiSpace = MapThread[WaveletTransform[#1, {Re[#2]}] &, {os, ostObj[["Wavelets"]]["FullData"]}];

			(* Compute Mphi-inverse *)
			cakeF = CenteredFourier/@ostObj[["Wavelets"]]["FullData"];
			Mpsi = Total[Abs[cakeF]^2] + ostObj[["Wavelets"]][["FourierDcFilter"]]^2;

			(* Chop to avoid division by small numbers *)
			TMpsi = Chop[Mpsi,threshold]/.{0->threshold};
			MphiInv = Re[CenteredInverseFourier[TMpsi^(-1)]];

			(* Convolve OS integrated over theta with Mphi and add the DC-component *)
			image = WaveletTransform[Plus@@convMultiSpace+ostObj[["DcFilteredImage"]],{MphiInv}];

			(* Return the reconstructed image*)
			Return[image]

		];



	End[];



EndPackage[];