(*************************************************************************************************************************
** OrientationScoreDerivatives.m
** This .m file contains functions to compute the orientation derivatives in se(2)
**
** Author: F.C. Martin <f.c.martin@tue.nl>
** Author: E.J. Bekkers <e.j.bekkers@tue.nl>
**************************************************************************************************************************)
BeginPackage["LieAnalysis`OrientationScores2D`OrientationScoreDerivatives`", {
    "LieAnalysis`"
  }
];



	Begin["`Private`"];

	    Needs["Classes`"];
	    Needs["LieAnalysis`Objects`ObjPositionOrientationData`"];
	    Get["LieAnalysis`Common`"];
	    
	    
	    
	    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
	    (* LeftInvariantDerivatives - - - - - - - - - - - - - - - *)
	    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)



	    LeftInvariantDerivatives[osObj_ /; ValidQ[osObj, ObjPositionOrientationData], {sigmaSpatial_, sigmaOrientation_}, derivativeIndex_, opts:OptionsPattern[] ] := Module[
	
	      	(*Local variable declaration *)
	      	{
	       		sigmaOD, derivatives
	      	},
	
	      	(* Convert to discrete orientation sigma *)
	      	sigmaOD = sigmaOrientation / osObj["AngularResolution"];
		
			(* Select the correct derivatives *)
			derivatives = LieAnalysis`OrientationScores2D`Derivatives`LeftInvariantDerivatives`Private`LeftInvariantDerivativesInternal[osObj, sigmaSpatial, sigmaOD, derivativeIndex];
			
			(* Append the used options to the object *)
			derivatives = Switch[Head[derivatives],
				List,
					Affix[#,"SpecifiedOptions"->{opts}]&/@derivatives,
				_,
					Affix[derivatives,"SpecifiedOptions"->{opts}]
			];
	      	
	      	(* Return the Derivatives *)
	      	Return[derivatives];
	
	    ];


	
		Options[LeftInvariantDerivatives] = Options[LieAnalysis`Common`GroupDifferences];
		
		
		LeftInvariantDerivatives[osObj_ /; ValidQ[osObj, ObjPositionOrientationData] && osObj["ComplexQ"], derivativeIndex_ /; MatchQ[derivativeIndex, _Integer | {_Integer ..}], opts:OptionsPattern[] ] := Module[
			
			{
				real = osObj["Data","Real"],
				imaginary = osObj["Data","Imaginary"],
				realObj, imObj, coObj
			},
		
			(* Create object with only real values Position Orientation Data objects *)
			realObj = Affix[osObj,"Data"->real];
			imObj = Affix[osObj,"Data"->imaginary];
			
			(* Compute Left Invariant Derivatives for both Real and Complex Orientation Scores *)
			realObj = LeftInvariantDerivatives[realObj, derivativeIndex, opts];
			imObj = LeftInvariantDerivatives[imObj, derivativeIndex, opts];
			
			(* Stitch together again *)
			coObj = Affix[osObj,"Data"->(realObj["Data"] + I imObj["Data"])];
			
			(* Return complex Orientation Scores *)
			Return[coObj]; 
			
		];
		
		LeftInvariantDerivatives[osObj_ /; ValidQ[osObj, ObjPositionOrientationData], derivativeIndex_ /; MatchQ[derivativeIndex, _Integer | {_Integer ..}], opts:OptionsPattern[] ] := Module[
		
			(*Local variable declaration *)
			{
				derivatives	
			},
			
			Switch[derivativeIndex,
				_Integer,
					derivatives = LieAnalysis`Common`GroupDifferences[Re@osObj["Data"], DerivativeIndexToReadableFormat[derivativeIndex], FilterRules[{opts,"Periodicity"->Abs[osObj[["Symmetry"]]]}, Options[LieAnalysis`Common`GroupDifferences]]];
					derivatives = LieAnalysis`Objects`ObjPositionOrientationData`ObjPositionOrientationData[{"Data"->derivatives, "Symmetry"->osObj[["Symmetry"]], "InputData"->osObj,"SpecifiedOptions"->{opts}}];
					,
				_List,
					derivatives = LieAnalysis`Common`GroupDifferences[Re@osObj["Data"], DerivativeIndexToReadableFormat[#], FilterRules[{opts,"Periodicity"->Abs[osObj[["Symmetry"]]]}, Options[LieAnalysis`Common`GroupDifferences]]]&/@derivativeIndex;
					derivatives = LieAnalysis`Objects`ObjPositionOrientationData`ObjPositionOrientationData[{"Data"->#, "Symmetry"->osObj[["Symmetry"]], "InputData"->osObj,"SpecifiedOptions"->{opts}}]&/@derivatives;
			];		
			
			
			(* Return the Derivatives *)
	      	Return[derivatives];
	      	
		];
		
		
		
		DerivativeIndexToReadableFormat[index_Integer] := "A" <> StringRiffle[IntegerDigits[index], "A"];



	End[];



EndPackage[];