(*************************************************************************************************************************
** CakeWavelets.m
** This .m file contains functions to create cake-wavelets.
** 
** Author: E.J. Bekkers <e.j.bekkers@tue.nl>
** Author: F.C. Martin <f.c.martin@tue.nl>
** Version: 0.2
** Notes:
**	- The option DcStandardDeviation is used to remove the low frequencies from the OS. The Standard Deviation is related
** 		to the sigma of a Gaussian in the SPATIAL domain. This is valid if designtype N is used, but when the cake is scaled
** 		to create design M (M = Sqrt[N]) this gaussian is scaled in the Fourier domain, changing the spatial shape. This
** 		might be resolved in a later version.
**	- The spatial window option is included to remove the long-tails of the filter in the spatial domain. This is more or
**		less the same as removing the low-frequencies in the Fourier domain, what is done by the Dc-Filter. Therefor the spatial
** 		window has become reduntant and might be removed in future releases.
**************************************************************************************************************************)
BeginPackage["LieAnalysis`OrientationScores2D`Filters`CakeWavelets`"];



	Begin["`Private`"];



		Get["LieAnalysis`Common`"];
		Needs["Classes`"];
		Needs["LieAnalysis`Objects`ObjCakeWavelet`"];



		(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
		(* CakeWaveletStack  - - - - - - - - - - - - - - - - - - - - *)
		(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)


		
		Options[CakeWaveletStack] = {			
			"Directional"->False, (* Determines whenever the filter goes in both directions *)
			"Design"->"N", (* Indicates which design is used N = Subscript[N, \[Psi]] or M = Subscript[M, \[Psi]] *)
			"InflectionPoint"->0.8, (* Is the location of the inflection point as a factor in (positive) radial direction *)
			"SplineOrder"->3, (* Order of the B-Spline that is used to construct the wavelet *)
			"MnOrder"->8, (* The order of the (Taylor expansion) gaussian decay used to construct the wavelet *)
			"DcStandardDeviation"->8, (* The standard deviation of the gaussian window (in the Spatial domain) that removes the center of the pie, to avoid long tails in the spatial domain *)
			"OverlapFactor"->1 (* How much the cakepieces overlaps in \[Phi]-direction, this can be seen as subsampling the angular direction *)
		};
		
		

		CakeWaveletStack::invalidInput = "The supplied value '`1`' for option `2` is invalid. (`3`)";
		


		CakeWaveletStack[nOrientations_, sizeIn_, opts:OptionsPattern[]] := Module[
	
			(* Local Variables Declaration *)
			{
				splineOrder          = OptionValue["SplineOrder"],
				overlapFactor        = OptionValue["OverlapFactor"],
				inflectionPoint      = OptionValue["InflectionPoint"],
				mnOrder              = OptionValue["MnOrder"],
				dcStdDev             = OptionValue["DcStandardDeviation"], (*ToDo: implement auto method, to ensure the Mn and Dc do not overlap *)
				directional          = OptionValue["Directional"],
				design               = OptionValue["Design"],
				size, cakeObj
			},

			(* Compute Wavelet Size Depending based on the inflectionPoint *)
			size = If[EvenQ[sizeIn],sizeIn+1,sizeIn];
		
			(* Validate the Input *)
			If[!Internal`PositiveIntegerQ[nOrientations], Message[CakeWaveletStack::invalidInput, nOrientations, "nOrientations", "Should be a positive integer"]];
			If[!BooleanQ[directional],Message[CakeWaveletStack::invalidInput, directional, "Directional","Should be a boolean"]];
			If[!IntegerQ[splineOrder], Message[CakeWaveletStack::invalidInput, splineOrder, "SplineOrder", "Should be an integer"]];
			If[splineOrder < 3, Message[CakeWaveletStack::invalidInput, splineOrder, "SplineOrder", "Should be greater than 3"]];
			If[!IntegerQ[overlapFactor],Message[CakeWaveletStack::invalidInput, overlapFactor, "OverlapFactor", "Should be an integer"]];
			If[overlapFactor < 1, Message[CakeWaveletStack::invalidInput, overlapFactor, "OverlapFactor", "Should be greater or equal to 1"]];
			If[!NumericQ[inflectionPoint] || inflectionPoint > 1 || inflectionPoint < 0, Message[CakeWaveletStack::invalidInput, inflectionPoint, "InflectionPoint","Should be an number between 0 and 1"]];
			If[!IntegerQ[mnOrder], Message[CakeWaveletStack::invalidInput, mnOrder, "MnOrder","Should be an integer"]];
			If[mnOrder < 3, Message[CakeWaveletStack::invalidInput, mnOrder, "MnOrder","Should be greater than 2"]];
			If[!NumericQ[dcStdDev], Message[CakeWaveletStack::invalidInput, dcStdDev, "DcStandardDeviation","Should be an number"]];
			If[dcStdDev < 0, Message[CakeWaveletStack::invalidInput, dcStdDev, "DcStandardDeviation","Should be greater than 0"]]; 
			If[design != "M" && design != "N", Message[CakeWaveletStack::invalidInput, design, "Design","Should be either \"M\" or \"N\""]];
		
			(* Compute the cakewavelet-stack *)
			cakeObj = CakeWaveletStack[
				size, nOrientations, design, inflectionPoint, mnOrder, splineOrder, overlapFactor, dcStdDev, directional
			];

			(* ToDo: do some post creation checking: 1) does the Fourier-cake go to 0? 2) do the Dc and Mn window interfere? *)

			(* Append the used options to the object *)
			AffixTo[cakeObj,"SpecifiedOptions"->{opts}];

			(* Return the cakewavelet stack *)
			Return[cakeObj];
			
		];



		(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
		(* CakeWaveletStack- - - - - - - - - - - - - - - - - - - - - *)
		(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
		

		
		CakeWaveletStack[size_, nOrientations_, design_, inflectionPoint_, mnOrder_, splineOrder_, overlapFactor_, dcStdDev_, directional_] := Module[
				
			{
				noSymmetry = OddQ[nOrientations],
				cakeObj, cake, filters, cakeF, dcFilter, dcSigma
			},

			(* Create contianer to store the cakewavelets in *)
			cakeObj = ObjCakeWavelet[{
				"WaveletSize"->size,
				"Directional"->directional,
				"Symmetry"->Pi,
				"Design"->design,
				"InflectionPoint"->inflectionPoint,
				"SplineOrder"->splineOrder,
				"MnOrder"->mnOrder,
				"DcStandardDeviation"->dcStdDev,
				"OverlapFactor"->overlapFactor
			}];
			
			(* Convert the dc sigma to the Fourier domain *)
			dcSigma = (1/dcStdDev)*(size/(2 Pi));
			
			(* Compute the Fourier cake *)
			filters = CakeWaveletStackFourier[size,N[2Pi/nOrientations],splineOrder,overlapFactor,inflectionPoint,mnOrder,N@dcSigma,noSymmetry];
			cakeF = filters[[1;;-2]];
			dcFilter = filters[[-1]];
		
			(* Scale the cake according to its design*)
			Switch[design,
				"M", cakeF = Sqrt[cakeF]; dcFilter = Sqrt[dcFilter]
			];
			cakeObj = Affix[cakeObj, {"FourierCake"->cakeF, "FourierDcFilter"->dcFilter}];
			
			(* Inverse (centered) Fourier Transform *)
			cake = CenteredInverseFourier/@cakeF;
			cakeObj = Affix[cakeObj, "PreSpatialFilters"->cake];
		
					
			(* Cut the single wavelets in two directional wavelets *)
			If[directional,
				cake = If[noSymmetry, cake, Join[cake, Conjugate[cake]]];
				cake = cake * ErfSet[size, (overlapFactor*nOrientations), 2Pi];
			];	
			
      		(* Append the DC-filter, if the spatial window is used this should be a spike *)
			cakeObj = Affix[cakeObj, {
				"Data"->cake, 
				"DcFilter"->Chop[CenteredInverseFourier[dcFilter]],
				"Symmetry"->If[directional || noSymmetry, 2Pi, Pi]
			}];
		
			(* Return the Stack *)
			Return[cakeObj];
			
		];



		(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
		(* CakeWaveletStackFourier - - - - - - - - - - - - - - - - - *)
		(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)


		
		CakeWaveletStackFourier = Compile[
			
			{
				{size,_Integer,0},{sPhi,_Real,0},{splineOrder,_Integer,0},{overlapFactor,_Integer,0},{inflectionPoint,_Real,0},{mnOrder,_Integer,0},{dcStdDev,_Real,0},{noSymmetry, True|False}
			},
			
			Module[
				
				{
					dcWindow = ConstantArray[1.,{size,size}] - WindowGauss[size,dcStdDev], 
					mnWindow, angleGrid,cake,filters, s
				},
				
				(* Gaussian decay window *)
				mnWindow  = MnWindow[size,mnOrder,inflectionPoint];
		
				(* Construct the cake pieces *)
				angleGrid = PolarCoordinateGridAngular[size];
				sPhiOverlapped = sPhi / overlapFactor;
				
				(* Check if symmetru can be used *)
				s = If[noSymmetry, 2Pi, Pi];
				
				(* Compute Fourier cakes *)
				cake = Table[
					dcWindow * mnWindow * BSplineMatrixFunc[splineOrder, Mod[angleGrid - theta - Pi/2 , 2Pi , -Pi ] / sPhi] / overlapFactor,
					{theta, 0, s - sPhiOverlapped, sPhiOverlapped}
				];
		
		
				(* Add Dc filter to the output *)
				filters = Append[cake,(1-dcWindow)];		
		
				(* Return the Fourier cake *)
				Return[filters]
		
			], {{WindowGauss[size,dcStdDev],_Real,2},{MnWindow[size,mnOrder,inflectionPoint],_Real,2},{PolarCoordinateGridAngular[size],_Real,2}}
		];



		(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
		(* BSplineMatrixFunc - - - - - - - - - - - - - - - - - - - - *)
		(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
		
		
		
		
		BSplineMatrixFunc = Compile[{{n,_Integer},{x,_Real,2}},
			Module[{function,intervalCheck},
				Total[
					Table[
						function=1/(2 (n+1-1)!) Total[Table[Binomial[n+1,k](x+(n+1)/2-k)^(n+1-1) (-1)^k Sign[i+(n+1)/2-k],{k,0,n+1}]];
						intervalCheck=Round[If[i<(n+1-1)/2,UnitStep[x-(i-1/2+$MachineEpsilon)]*UnitStep[-(x-(i+1/2))],UnitStep[x-(i-1/2+$MachineEpsilon)]*UnitStep[-(x-(i+1/2-$MachineEpsilon))]]];
						function*intervalCheck,
						{i,(1-n-1)/2,(n-1+1)/2}
					]
				]
			]
		];




		(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
		(* MnWindow - - - - - - - - - - - - - - - - - - - - *)
		(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
		
		
		
		MnWindow = Compile[{{size,_Integer},{n,_Integer},{inflectionPoint,_Real}},
			Module[{rhoMatrix},
					rhoMatrix = $MachineEpsilon + 1/Sqrt[(2*(inflectionPoint)^2)/(1+2 n)] PolarCoordinateGridRadial[size];
					Total[Table[E^-rhoMatrix^2*rhoMatrix^(2k)/k!,{k,0,n}]]
				]
				,{{k!,_Real},{PolarCoordinateGridRadial[size],_Real,2}}
		];
		


		(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
		(* PolarCoordinateGridRadial - - - - - - - - - - - - - - - - *)
		(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
		
		
		
		PolarCoordinateGridRadial = Compile[{{size,_Integer}},
			Table[
				(Sqrt[x^2+y^2]+$MachineEpsilon) /((size-1)/2),
				{x,-((size-1)/2)-Mod[(size-1)/2,1],(size-1)/2-Mod[(size-1)/2,1]},
				{y,-((size-1)/2)-Mod[(size-1)/2,1],(size-1)/2-Mod[(size-1)/2,1]}
			]
		];
				


		(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
		(* PolarCoordinateGridAngular- - - - - - - - - - - - - - - - *)
		(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
		
		
		
		PolarCoordinateGridAngular = Compile[{{size,_Integer}},
			Table[
				Arg[x+I y],
				{x,-((size-1)/2)-Mod[(size-1)/2,1],(size-1)/2-Mod[(size-1)/2,1]},
				{y,-((size-1)/2)-Mod[(size-1)/2,1],(size-1)/2-Mod[(size-1)/2,1]}
			]
		];
		
		
		
		(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
		(* WindowGauss - - - - - - - - - - - - - - - - - - - - - - - *)
		(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
		
		
		
		WindowGauss = Compile[{{size,_Integer},{\[Sigma]s,_Real}},
			Table[
				E^(-(x^2/(2\[Sigma]s^2))-y^2/(2\[Sigma]s^2)),
				{x,-((size-1)/2)-Mod[(size-1)/2,1],(size-1)/2-Mod[(size-1)/2,1]},
				{y,-((size-1)/2)-Mod[(size-1)/2,1],(size-1)/2-Mod[(size-1)/2,1]}
			]
		];
		
		
		
		(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
		(* ErfSet- - - - - - - - - - - - - - - - - - - - - - - - - - *)
		(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
		
		
		
		ErfSet = Compile[{{size,_Real},{No,_Integer},{periodicity, _Real}}, 
			Module[{d\[Theta]},
				Table[
					Table[
						1/2 (1+Erf[x Cos[(periodicity (-1+i))/No]+y Sin[(periodicity (-1+i))/No]]),
						{x,-((size-1)/2)-Mod[(size-1)/2,1],(size-1)/2-Mod[(size-1)/2,1]},
						{y,-((size-1)/2)-Mod[(size-1)/2,1],(size-1)/2-Mod[(size-1)/2,1]}
					],
					{i,1,No}
				]
			]
		];
		
		
				
	End[];

EndPackage[];
