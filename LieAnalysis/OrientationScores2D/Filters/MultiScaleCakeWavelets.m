(*************************************************************************************************************************
** MultiScaleCakeWavelets.m
** This .m file contains functions to create cake-wavelets.
** 
** Author: E.J. Bekkers <e.j.bekkers@tue.nl>
** Author: F.C. Martin <f.c.martin@tue.nl>
**************************************************************************************************************************)
BeginPackage["LieAnalysis`OrientationScores2D`Filters`MultiScaleCakeWavelets`"];



	Begin["`Private`"];
	
		(*ToDo: 90% of this code is re-used code from the Cake Wavelets, think this file could be a loooot shorter... *)
	
		Get["LieAnalysis`Common`"];
		Needs["Classes`"];
	
	
		(*ToDo: This is not correct... for now.. *)
		Options[MultiScaleCakeWaveletStack] = Options[LieAnalysis`OrientationScores2D`Filters`CakeWavelets`Private`CakeWaveletStack];
	
	
	
		MultiScaleCakeWaveletStack[nOrientations_Integer, size_Integer, {rhoMin_Real:0.1, rhoMax_Real:0.7,nScales_Integer:2}, opts:OptionsPattern[]] := Module[
			
			(* Local Variable Declaration *)
			{
				splineOrder          = OptionValue["SplineOrder"],
				overlapFactor        = OptionValue["OverlapFactor"],
				mnOrder              = OptionValue["MnOrder"],
				dcStandardDeviation  = OptionValue["DcStandardDeviation"], 
				directional          = OptionValue["Directional"],
				design               = OptionValue["Design"],
				angularResolution 	 = 2Pi / nOrientations, 
				noSymmetry 			 = OddQ[nOrientations],
				cakeObj, dcSigma, cake, cakeF
			},
			
			(* Create contianer to store the cakewavelets in *)
			cakeObj = LieAnalysis`Objects`ObjScaleCakeWavelet`ObjScaleCakeWavelet[{
				"WaveletSize"->size,
				"Directional"->directional,
				"Symmetry"->Pi,
				"Design"->design,
				"SplineOrder"->splineOrder,
				"MnOrder"->mnOrder,
				"OverlapFactor"->overlapFactor,
				"Scales"->nScales
			}];
			
			(* Convert the dc sigma to the Fourier domain *)
			dcSigma = (1/dcStandardDeviation)(1/Pi);
			
			(*Consturct Cake Wavelets in the Fourier-domain *)
			cakeF = ScaleCakeWaveletStackFourier[size, angularResolution, splineOrder, overlapFactor, noSymmetry,dcSigma,rhoMax,rhoMin,nScales];
			
			(* Scale the cake according to its design*)
			Switch[design,
				"M", cakeF = Sqrt[cakeF];
			];
			cakeObj = Affix[cakeObj, {"FourierCake"->cakeF}];
			
			(* Inverse (centered) Fourier Transform *)
			cake = Map[CenteredInverseFourier,Flatten[cakeF,1]];
			cakeObj = Affix[cakeObj, "PreSpatialFilters"->cake];
		
					
			(* Cut the single wavelets in two directional wavelets *)
			If[directional,
				cake = If[noSymmetry, cake, Join[cake, Conjugate[cake]]];
				cake = cake * LieAnalysis`OrientationScores2D`Filters`CakeWavelets`Private`ErfSet[size, (nScales*overlapFactor*nOrientations), 2Pi];
			];	
			
			(* Append the DC-filter, if the spatial window is used this should be a spike *)
			cakeObj = Affix[cakeObj, {
				"Data"->cake, 
				"rhoMax"->rhoMax,
				"rhoMin"->rhoMin,
				"Symmetry"->If[directional || noSymmetry, 2Pi, Pi],
				"SpecifiedOptions"->{opts}
			}];
			
			Return[cakeObj];
			
		];	
		
		
		
		ScaleCakeWaveletStackFourier = Compile[
			
			{
				{size,_Integer,0},
				{sPhi,_Real,0},
				{splineOrder,_Integer,0},
				{overlapFactor,_Integer,0},
				{noSymmetry, True|False},
				{dcStandardDeviation, _Real,0},
				{rhoMax, _Real, 0},
				{rhoMin, _Real, 0},
				{nScales, _Integer, 0}
			},
			
			Module[
				
				{
					s = If[noSymmetry, 2Pi, Pi],
					(*ToDo: not hard-coded...*)
					(*xHalfMax = {0.6339745962155614`, 0.7223517244643763`, 0.7971951696335496`, 0.8660920722545019`, 0.930199477785786`, 0.9901050695461315`, 1.046679509451254`, 1.1003644048621606`, 1.1515727121886963`},*)
					angleGrid,cake,radialWindows, sPhiOverlapped, radialGrid, tauMin, tauMax,sRho
				},
				
				 
				tauMin = -Log[rhoMax]; (* Maximal scale in Log domain *)
				(*tauMax = Echo[(tauMax xHalfMax[[splineOrder]] - nScales Log[dcStandardDeviation Sqrt[2Log[2]]])/(nScales + xHalfMax[[splineOrder]])]; (* Minimal scale in Log domain *)*)
				
				tauMax = -Log[rhoMin];
				(*tauMax = Echo[(-tauMin xHalfMax[[splineOrder-1]] + Log[Echo[dcStandardDeviation,"dc"] Sqrt[2 Log[2]]] - nScales Log[dcStandardDeviation Sqrt[2 Log[2]]])/(-1 + nScales - xHalfMax[[splineOrder-1]])];*)
				
				sRho = (tauMax-tauMin)/(nScales-1); (* = Log[g] *)
				
				(*Construct radial window *)
				radialGrid = LieAnalysis`OrientationScores2D`Filters`CakeWavelets`Private`PolarCoordinateGridRadial[size]; (* radial grid*)
				
				(* Construct radial envelope *)
				radialWindows = Table[
					LieAnalysis`OrientationScores2D`Filters`CakeWavelets`Private`BSplineMatrixFunc[splineOrder, Log[radialGrid] /sRho + l + tauMin/sRho]
					,{l,0,nScales-1}
				];
				
				(*
				Echo@ListLinePlot[radialWindows[[All, 51, 51 ;; 101]], PlotRange -> All, GridLines -> Automatic,DataRange->{0,1}];
				Echo[dcStandardDeviation Sqrt[2Log[2]],"Gaus Rho 1/2 "];
				Echo@ListLinePlot[LieAnalysis`OrientationScores2D`Filters`CakeWavelets`Private`WindowGauss[size,dcStandardDeviation*(size/2)][[51,51;;101]],PlotRange->All, GridLines -> Automatic,DataRange->{0,1}];
				Echo@ListLinePlot[Total@radialWindows[[All, 51, 51 ;; 101]],PlotRange -> All, GridLines -> Automatic,DataRange->{0,1}];
				*)
				
				(* Prepare for angular grid *)
				angleGrid = LieAnalysis`OrientationScores2D`Filters`CakeWavelets`Private`PolarCoordinateGridAngular[size];
				sPhiOverlapped = sPhi / overlapFactor;
				
				(* Compute Fourier cakes *)
				cake = Table[
					radialWindow*
						LieAnalysis`OrientationScores2D`Filters`CakeWavelets`Private`BSplineMatrixFunc[
							splineOrder, Mod[angleGrid - theta - Pi/2 , 2Pi , -Pi ] / sPhi
						] / overlapFactor
				,
					{radialWindow,radialWindows},{theta, 0, s - sPhiOverlapped, sPhiOverlapped}
				];
		
				(* Return the Fourier cake *)
				Return[cake]
		
			], {
				{RadialWindowGroupingSpatial[nScales, minScale, g, size],_Real,1},
				{PolarCoordinateGridAngular[size],_Real,2}
				}
		];
	
	
	
		(*ToDo: Cleanup *)
		RadialWindowGroupingSpatial[Ns_,rhoMax_,g_,dim_]:=Block[{a,\[Rho]min},

			a = Table[rhoMax g^i,{i,Ns-1,0,-1}];
			\[Rho]min=Min[.14628 dim/a];
		
			Flatten[Last@RadialWindowGroupingFourier[Ns,\[Rho]min,g,dim]]

		];
		
		(*ToDo: Cleanup *)
		RadialWindowGroupingFourier[Ns_,\[Rho]min_,g_,dim_]:=Block[{\[Rho]1,i0,i1,s\[Rho],padding,freqs,lSorted},

			\[Rho]1=(dim-1)/2.;(*Nyquist freq*)
			
			i0=Ceiling[Log[1./\[Rho]min]/Log[g]];
			i1=Floor[Log[\[Rho]1/\[Rho]min]/Log[g]];
			s\[Rho]=Log[g];
			
			(*Bspline control point padding*)
			padding=0;
			
			freqs=Join[{Table[\[Rho]min g^i,{i,Min[i0,0],Max[i0,-1]}]},Table[{\[Rho]min g^i},{i,Max[i0+1,0],Min[Ns-1,i1-1]}],{Table[\[Rho]min g^i,{i,Min[Ns,i1],i1}]}];
			lSorted=Join[
			{Range[Min[i0,0]-padding,Max[i0,-1]]},
			Table[{i},{i,Max[i0+1,0],Min[Ns-1,i1-1]}],
			{Range[Min[Ns,i1],i1+padding]}]+Log[\[Rho]min]/s\[Rho];
			
			N@{freqs,lSorted}
			
		]	
	
	
	
	End[]
	
	

EndPackage[];