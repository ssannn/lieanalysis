(*************************************************************************************************************************
** LeftInvariantDerivativesInternal.m
** This .m file contains functions to compute the left-invariant derivatives in se(2)
**
** Author: F.C. Martin <f.c.martin@tue.nl>
** Author: E.J. Bekkers <e.j.bekkers@tue.nl>
**************************************************************************************************************************)
BeginPackage["LieAnalysis`OrientationScores2D`Derivatives`LeftInvariantDerivatives`", {
  "LieAnalysis`"
}];



  	Begin["`Private`"];



	    Needs["Classes`"];
	    Get["LieAnalysis`Common`"];
	    Get["LieAnalysis`Ultilities`"];
	

	
	    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
	    (* LeftInvariantDerivativesInternal- - - - - - - - - - - - - - - - - *)
	    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
	
	
	
		LeftInvariantDerivativesInternal[osObj_ /; ValidQ[osObj, LieAnalysis`Objects`ObjPositionOrientationData`ObjPositionOrientationData], sigmaSpatial_, sigmaOrientation_, order_List] := Block[
			
			{
				map = If[OptionValue[LieAnalysis,"DynamicUpdates"], GeneralUtilities`MonitoredMap, Map]
			},
			
	    	Return[map[LeftInvariantDerivativesInternal[osObj, sigmaSpatial, sigmaOrientation, #]&,order]]
	    	
		];
	
	
	
		(* Special case: the orientation order is illegal, orientation derivatives should always be computed last *)
		LeftInvariantDerivativesInternal[osObj_ /; ValidQ[osObj, LieAnalysis`Objects`ObjPositionOrientationData`ObjPositionOrientationData], sigmaSpatial_, sigmaOrientation_, order_?(StringMatchQ[ToString[#], ___ ~~ "3" ~~ ("*1*" | "*2*")]&)] := Module[
	
			(*Local Variables Declaration *)
		  	{
		    	orderString = ToString[order],  (* illegal order *)
		   		derivatives, orders, derivativeString, operators, derivative, osObjOut
		  	},
		
			(* Recompute Order *)
			derivativeString = StringSplit[OrderTheDerivatives[orderString],{"+"->"+","-"->"-"}];
		
			{orders, operators} = GatherBy[derivativeString,MatchQ[#,"-"|"+"]&];  (* legal derivative orders that need to be computed *)
		
			(* Compute derivatives *)
		    derivatives = LeftInvariantDerivativesInternal[osObj, sigmaSpatial, sigmaOrientation, ToExpression/@orders];
		
			(* Add operators again and sum to the final derivative *)
			derivative = Plus @@ Prepend[MapThread[If[#2 == "-", -#1["Data"], #1["Data"]] &, {derivatives[[2 ;; -1]], operators}], derivatives[[1]]["Data"]];
		
			(* Store derivative in container *)
			osObjOut = LieAnalysis`Objects`ObjPositionOrientationData`ObjPositionOrientationData[{"Data"->derivative, "Symmetry"->derivatives[[1]][["Symmetry"]], "InputData"->osObj}];
		
			(* Return the derivative *)
		    Return[osObjOut];
	
	    ];



    (* Special Case: the input is a name for a set of derivatives *)
   (* LeftInvariantDerivativesInternal[osObj_ /; ValidQ[osObj, LieAnalysis`Objects`ObjPositionOrientationData`ObjPositionOrientationData], sigmaSpatial_, sigmaOrientation_, order_String] := Module[

		(* Local Variables Declaration *)
      	{derivatives},

    	(* Get derivatives *)
      	(MatchQ[ToUpperCase[order], "GRADIENT"])
        	/. True :> (derivatives = LeftInvariantDerivativesInternal[osObj, sigmaSpatial, sigmaOrientation, {1,2,3}]);

      	(MatchQ[ToUpperCase[order], "HESSIAN"])
          	/. True :> (derivatives = LeftInvariantDerivativesInternal[osObj, sigmaSpatial, sigmaOrientation, {11,21,31,12,22,32,13,23,33}]);

      	(MatchQ[ToExpression[order], _Integer])
          	/. True :> (derivatives = LeftInvariantDerivativesInternal[osObj, sigmaSpatial, sigmaOrientation, ToExpression[order]]);

      	(* Return Derivatives *)
      	Return[derivatives];

	];*)

	(* If we are dealing with complex data we need to run the code twice *)
	LeftInvariantDerivativesInternal[osObj_ /; ValidQ[osObj, LieAnalysis`Objects`ObjPositionOrientationData`ObjPositionOrientationData] && osObj["ComplexQ"], sigmaSpatial_, sigmaOrientation_, order_Integer] := Module[
	
		(* Local Variables Declaration *)
		{
			real = osObj["Data","Real"],
			imaginary = osObj["Data","Imaginary"],
			realObj, imObj, coObj
		},
		
		(* Create object with only real values Position Orientation Data objects *)
		realObj = Affix[osObj,"Data"->real];
		imObj = Affix[osObj,"Data"->imaginary];
		
		(* Compute Left Invariant Derivatives for both Real and Complex Orientation Scores *)
		realObj = LeftInvariantDerivativesInternal[realObj, sigmaSpatial, sigmaOrientation, order];
		imObj = LeftInvariantDerivativesInternal[imObj, sigmaSpatial, sigmaOrientation, order];
		
		(* Stitch together again *)
		coObj = Affix[osObj,"Data"->(realObj["Data"] + I imObj["Data"])];
		
		(* Return complex Orientation Scores *)
		Return[coObj]; 
	
	];
		
    (* Main function *)
    LeftInvariantDerivativesInternal[osObj_ /; ValidQ[osObj, LieAnalysis`Objects`ObjPositionOrientationData`ObjPositionOrientationData], sigmaSpatial_, sigmaOrientation_, order_Integer] := Module[

    (* Local Variables Declaration *)
      {
        angles, derivative, derOrder, scaledSigmaSpatial, n, symmetry,osObjOut, angularOrder
      },
		
      (* orientation angles *)
      angles = Range[0, Abs[osObj[["Symmetry"]]] - osObj["AngularResolution"], osObj["AngularResolution"]];

      (* List of Derivatives that need to be computed *)
      derOrder = Reverse[IntegerDigits[order]];

      (* Double check the input string has no illegal order *)
      MatchQ[derOrder, {3...,Except[3]...}] /. False :> (Message[General::argfail, "order", "spatial derivatives should go first (3's should be last)"]; Abort[];);

      (* Compute Derivative *)
      symmetry = osObj[["Symmetry"]];

      (* scale sigmas to the number of derivatives *)
      n = Count[derOrder,1 | 2] + 1;(*theta derivatives are always computed in 1 step, independent of the order*)
      scaledSigmaSpatial = 1/Sqrt[n] sigmaSpatial;

      (* Compute angular derivative(s) if any *)
      angularOrder = Count[derOrder,3];
      derivative = OrientationDerivative[osObj["Data","Real"], scaledSigmaSpatial, sigmaOrientation, osObj["AngularResolution"], symmetry, angularOrder];
      derOrder = DeleteCases[derOrder, 3];



      Do[

        derivative = SpatialDerivative[derivative, scaledSigmaSpatial, 0, dir, angles, symmetry];
        symmetry = Switch[symmetry, Pi, -Pi, -Pi,  Pi, 2Pi, 2Pi]

        ,{dir, derOrder}

      ];

      (* ObjOrientationScore Object *)
      osObjOut = LieAnalysis`Objects`ObjPositionOrientationData`ObjPositionOrientationData[{"Data"->derivative, "Symmetry"->symmetry, "InputData"->osObj}];
		
      (* Return Derivatives *)
      Return[osObjOut];

    ];



    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
    (* Spatial Derivative- - - - - - - - - - - - - - - - - - - - *)
    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)



    SpatialDerivative[derivativeIn_, scaledSigmaSpatial_, scaledSigmaOrientation_, dir_, angles_, symmetry_] := Module[

      (*Local Variables Declaration *)
      {orientationBlurredOST,dx,dy, periodicOS,orientationBlurredOS,derivative},

      (* Blur the OS in thera-direciton *)
      (* TODO: remove redundant steps *)
      periodicOS = CreatePeriodicOrientationAxes[derivativeIn, symmetry];
      orientationBlurredOS = periodicOS;
      
      orientationBlurredOS = If[MatchQ[symmetry, Pi | -Pi], orientationBlurredOS[[All,All,1;;Dimensions[derivativeIn][[3]]]], orientationBlurredOS];

      (* Reorder *)
      orientationBlurredOST = Reorder[orientationBlurredOS, "OrientationFirst"];
		
      (* Compute the x and y-derivative *)
      dx = GaussianDerivative[#, {scaledSigmaSpatial,scaledSigmaSpatial}, {1,0},"Fixed"]&/@orientationBlurredOST;
      dy = GaussianDerivative[#, {scaledSigmaSpatial,scaledSigmaSpatial}, {0,1},"Fixed"]&/@orientationBlurredOST;

      (* Rotate to the eta, xi frame *)
      derivative = Switch[dir,
        1, MapThread[( #1 Cos[#3] + #2 Sin[#3])&, {dx, dy, angles}],
        2, MapThread[(-#1 Sin[#3] + #2 Cos[#3])&, {dx, dy, angles}]
      ];

      (* Reorder x,y,theta *)
      derivative = Reorder[derivative, "SpatialFirst"];

      (* Return the derivative *)
      Return[derivative];

    ];



    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
    (* OrientationDerivative - - - - - - - - - - - - - - - - - - *)
    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)



    OrientationDerivative[derivativeIn_, scaledSigmaSpatial_, sigmaOrientation_, angularResolution_, symmetry_, order_] := Module[

      {spatialBlurredOs, derivative, periodicOS,periodicOST},

      (* Compute the theta-derivative *)
      periodicOS = CreatePeriodicOrientationAxes[derivativeIn, symmetry];

      (* Blur the spatial directions *)
      periodicOST = Reorder[periodicOS, "OrientationFirst"];
      spatialBlurredOs = (GaussianFilter[#, {4{scaledSigmaSpatial, scaledSigmaSpatial}+1,{scaledSigmaSpatial, scaledSigmaSpatial}}, Padding->"Fixed"])&/@periodicOST;

      (* Derivative in y-direction *)
      derivative = GaussianDerivative[spatialBlurredOs, {sigmaOrientation, 0.125, 0.125}, {order,0,0}, "Periodic"];(* TODO:spatial scale 0.125 is used instead of 0.0, because Mathematica has a bug for the case 0.0 with SetOptions[GaussianFilter,Method->"Gaussian"]. Scale 0.125 is still small enough to be ignored, but large enough for Mathematica to be stable..*)

      (* Cut off 'extra' period *)
      derivative = If[MatchQ[symmetry, Pi | -Pi], derivative[[1;;Dimensions[derivativeIn][[3]]]], derivative];

      (* Scale with the angular resolution *)
      derivative = derivative / (angularResolution^order);

      (* Reorder that the spatial dimensions are first x,y,theta *)
      derivative = Reorder[derivative, "SpatialFirst"];

      (* Return the derivative *)
      Return[derivative];

    ];



    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
    (* OrderTheDerivatives - - - - - - - - - - - - - - - - - - - *)
    (* Theta derivative should always be first derivative,
           see P116-119, Erik Franken PhD Thesis - - - - - - - - - - *)
    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)



    OrderTheDerivatives[order_Integer] := OrderTheDerivatives[ToString[order]];



    OrderTheDerivatives[order_String] := Module[

    (* Define substitution rules *)
      {
        thetaXi   = prior___~~"31"~~post___:>prior<>"2"<>post<>"+"<>prior<>"13"<>post,
        thetaEta  = prior___~~"32"~~post___:>prior<>"23"<>post<>"-"<>prior<>"1"<>post,
        orderTerms, derivativeOrderReWritten, previousResult, result
      },

    (* Save the original *)
      result = order;

      (* Repeat the process untill theta is the first derivative *)
      While[previousResult =!= result,

        (* Update the itteration condition *)
        previousResult = result;
        orderTerms = StringSplit[result,{"+"->"+","-"->"-"}];

        (* Substitute *)
        derivativeOrderReWritten = StringReplace[#,{thetaEta,thetaXi}]&/@orderTerms;
        result = StringJoin[derivativeOrderReWritten];

      ];

      (* Return the reorderd result *)
      Return[result];

    ];



    CreatePeriodicOrientationAxes[os_, symmetry_] := Switch[symmetry,
      Pi, os,
      -Pi, Join[os, -Conjugate[os],3],
      2Pi, os
    ];

  End[];



EndPackage[];