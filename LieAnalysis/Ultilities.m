BeginPackage["LieAnalysis`Ultilities`"];



	Begin["`Private`"];

		
		Options[LieAnalysis] = {
			"DynamicUpdates"->False,
			"UseCUDA"->False
		};
		
		LieAnalysis["Version"] := $LieAnalysisVersion;
		LieAnalysis["Colors"] := <|"Blue"->RGBColor[36/255,140/255,200/255]|>;
		
		(* Web URLS *)
		LieAnalysis["WebsiteURL"] := URL["http://lieanalysis.nl"];
		LieAnalysis["ExampleDataURL"] := URLBuild[{LieAnalysis["WebsiteURL"],"exampledata"}];
		LieAnalysis["CUDALibrariesURL"] := URLBuild[{LieAnalysis["WebsiteURL"],"cudalibraries"}];
		LieAnalysis["LibrariesURL"] := URLBuild[{LieAnalysis["WebsiteURL"],"libraries"}];
		
		

		(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
		(* $LieAnalysisVersion - - - - - - - - - - - - - - - - - - - *)
		(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)



		$LieAnalysisVersion := First["Version" /. PacletManager`PacletInformation[PacletManager`PacletFind["LieAnalysis"]]];
		
		(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
		(* StatusBar- - - - - - -  - - - - - - - - - - - - - - - - - *)
		(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
		
		
		
		$statusBar = <|
			"Message" -> "I have no clue what we are doing right now...",
			"Activated" -> False,
			"Clock"->Dynamic@GeneralUtilities`TimeString[Clock[{0,\[Infinity],1}]],
			"Appearance"->(Row[{
				ProgressIndicator[Appearance->"Percolate"],"  ",
				Style[#Clock,"Label",Gray,11]," | ", 
				Style[ToString[#Message],LieAnalysis["Colors"]["Blue"]]
				}]&)
		|>;
		
		
		
		StatusBar[msg_] := If[OptionValue[LieAnalysis,"DynamicUpdates"],
			If[LieAnalysis`Ultilities`Private`$statusBar["Activated"],
				LieAnalysis`Ultilities`Private`$statusBar["Message"] = msg;
				Return[None];
			,
				LieAnalysis`Ultilities`Private`$statusBar["Message"] = msg;
				LieAnalysis`Ultilities`Private`$statusBar["Clock"] = Refresh[LieAnalysis`Ultilities`Private`$statusBar["Clock"]];
				LieAnalysis`Ultilities`Private`$statusBar["Activated"] = True;
				Return[PrintTemporary[Dynamic[LieAnalysis`Ultilities`Private`$statusBar["Appearance"][LieAnalysis`Ultilities`Private`$statusBar]]]];
			],
			Return[None];
		];
		
		
		
		StatusBarDelete[token_] := If[token =!= None,
			NotebookDelete[token];
			LieAnalysis`Ultilities`Private`$statusBar["Activated"] = False;
		];



		(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
		(* EmbedLieAnalysisPackage - - - - - - - - - - - - - - - - - *)
		(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)



		(*DONOTINCLUDEINEMBEDDEDVERSION*)
		EmbedLieAnalysisPackage[]:= Module[

			{prevDir,path,packageFiles,order,txtFiles,txt},

			(* Store the previous folder *)
			prevDir = Directory[];

			(* Get The M file-names of the package *)
			path = FileNameTake[FindFile["LieAnalysis`"], {1, -3}];
			SetDirectory[path];
			packageFiles = FileNameJoin[{path, #}] & /@ Join[FileNames["*.m", "*", Infinity],FileNames["*.m"]];

			(* Remove the init file as it is no longer required *)
			packageFiles = DeleteCases[packageFiles, _?(StringMatchQ[#, "*init.m"] &)];

			(* Order the files *)
 	 		order = Reverse[Flatten[StringCases[$ContextPath,"LieAnalysis`"~~___]]];
 	 		order = PrependTo[order, "Classes`"];
  			order = StringRiffle[#,"\\"]&/@StringSplit[order,"`"];
      		packageFiles = Flatten[StringCases[packageFiles,___~~# <> ".m"]&/@order];

			(* Copy the m-files into the temporary folder as txt files *)
			SetDirectory[$TemporaryDirectory];
			txtFiles = CopyFile[#,StringJoin @@ (FileNameSplit[#][[2;;-2]])<>FileBaseName[FileNameTake[#]]<>".txt", OverwriteTarget->True]&/@packageFiles;

			(* Create Chapter cell to bring the package under *)
			CellPrint[TextCell["LieAnalysis Package","Chapter"]];

			(* Include the references *)
			SetDirectory[path];
      		CellPrint@NotebookImport[FindFile["LiteratureList.nb"], _ -> "Cell"];

			(* Import the .m files *)
			txt = Import[#]&/@txtFiles;

			(* Remove Functions that need to be excluded *)
			txt = StringReplace[#, "StringReplace[#, \"(*"~~Shortest[___]~~"*)\" ~~ Shortest[___] ~~ \"(*"~~Shortest[___]~~"*)\"(* embed-fix *) -> \"\"]&/@txt"->"txt (* embed-fix *)"]&/@txt;
			txt = StringReplace[#, "(*DONOTINCLUDEINEMBEDDEDVERSION*)" ~~ Shortest[___] ~~ "(*ENDEXCLUSIONFROMEMBEDDEDVERSION*)"(* embed-fix *) -> ""]&/@txt;

			(* Print them to the notebook *)
			PrintCellTemplate[#[[1]],FileNameTake[#[[2]]]]&/@Transpose[{txt,packageFiles}];

			(* Restore previous dir *)
			SetDirectory[prevDir];
		];
		PrintCellTemplate[txt_,filename_]:=Module[{},
			CellPrint[TextCell[filename, "Subchapter"]];
			CellPrint[Cell[BoxData[txt], "Code"]];
		];
		(*ENDEXCLUSIONFROMEMBEDDEDVERSION*)
	
	
	End[];
	
	

EndPackage[];

  