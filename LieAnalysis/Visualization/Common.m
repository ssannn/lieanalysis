(* ::Package:: *)

(*
	Copyright 2016 Tom Dela Haije

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*)

BeginPackage["LieAnalysis`Visualization`Common`", {"PolyhedronOperations`", "TetGenLink`"}];

Unprotect[
	
	AnyValueQ, 
	BiasedRound,
	BlockDiagonalMatrix,
	CachedSymbol, 
	ConformalAffineTransformationQ,
	ConvexHullVolume, 
	ConvexityMeasure,
	CorrectedSpatialTransformation,
	CorrectedTemporalTransformation,
	DataIntensityRange,
	DataType,
	DefinedQ,
	DiagonalMatrixQ,
	ExtractPolyhedronVertices,
	FlattenPositions,
	FromDiffusicaIndex,
	FullToSymmetricTensor, 
	GetPolyhedronComplex,
	IndependentComponentsLengthToOrder,
	LocalGeodesate,
	MapProduct, 
	MaskedExp,
	MaskedLog,
	MaskedMatrixExp,
	MaskedMatrixLog,
	MedicalAxesLabel, 
	MergeInput,
	NestedWith, 
	NormRescale,
	OrderToIndependentComponentsLength,
	OrientationConventionIdentifierQ,
	OrientationConventionStringQ,
	OrientationConventionTransform,
	OrientationFrameSort,
	ParseOptions,
	PolyhedraVolume,
	ResolveSpatialReflections,
	SlicePolygon,
	SphericalAngles,
	RealSphericalBasisCoefficientsQ,
	RealSphericalBasisLengthQ,
	SphericalDelaunayTriangulation,
	SplitTransformationFunction,
	SquareMatrixTestQ,
	SymmetricToFullTensor,
	ToDiffusicaIndex,
	TotalIndependentComponentsLengthToOrder,
	TraceRescale,
	UndefinedQ,
	UnitVectors
	
];

AnyValueQ::usage = "AnyValueQ[symbol] returns True when there are any attributes or down-, format-, up-, N-, own-, sub-, or up-values associated with symbol, and False otherwise.";
BiasedRound::usage = "BiasedRound[x, a] rounds x to the nearest multiple of a.";
BlockDiagonalMatrix::usage = "BlockDiagonalMatrix[m_1, ..] creates a block diagonal matrix from the matrices m_i.";
CachedSymbol::usage = "CachedSymbol[name] refers to a cached symbol with the specified name.";
ComponentsLengthToOrder::usage = "ComponentsLengthToOrder[length] returns the order of a three dimensional tensor with length components.";
ConformalAffineTransformationQ::usage = "ConformalAffineTransformationQ[expr] returns True if expr is a TransformationFunction object representing a conformal affine transformation and False otherwise.";
ConvexHullVolume::usage = "ConvexHullVolume[points] computes the volume of the polyhedra forming the convex hull of the point cloud specified by the coordinate list points.";
ConvexityMeasure::usage = "ConvexityMeasure[points] computes the convexity measure of the point cloud specified by the coordinate list points.";
CorrectedSpatialTransformation::usage = "CorrectedSpatialTransform[tfun, {crop_z, crop_y, crop_x}, {scale_z, scale_y, scale_x}] gives a version of tfun corrected for spatial cropping and subsampling a structured data set.";
CorrectedTemporalTransformation::usage = "CorrectedTemporalTransformation[tfun, crop_t, scale_t] gives a version of tfun corrected for temporal cropping and subsampling a structured data set.";
DataIntensityRange::usage = "DataIntensityRange[data, range] substitutes the missing range information in the supplied range based on data."; 
DataType::usage = "DataType[expr] returns the number type of the elements of expression if it is a number or a PackedArray object, and False otherwise.";
DefinedQ::usage = "DefinedQ[expr] returns False if expr is identical to Undefined, and True otherwise.";
DiagonalMatrixQ::usage = "DiagonalMatrixQ[expr] returns True when expr is a diagonal matrix, and False otherwise.";
ExtractPolyhedronVertices::usage = "ExtractPolyhedronVertices[name, n] gives the vertices of an order n tessellated version of the polyhedron name.";
FlattenPositions::usage = "FlattenPositions[expr] removes and adds levels to expr if it is a list such that is has depth two, and acts as Identity otherwise.";
FromDiffusicaIndex::usage = "FromDiffusicaIndex[tfun] gives a new TransformationFunction object that maps vectors in tfun coordinates to Diffusica array coordinates.";
FullToSymmetricTensor::usage = "FullToSymmetricTensor[array, depth] returns a copy of array where the tensors at all positions deeper than depth are replaced by their independent components.";
GetPolyhedronComplex::usage = "GetPolyhedronComplex[name, n] gives a GraphicsComplex of an order n tessellated version of the polyhedron name.";
IndependentComponentsLengthToOrder::usage = "IndependentComponentsLengthToOrder[length] returns the order of a fully symmetric three dimensional tensor with length independent components.";
LocalGeodesate::usage = "LocalGeodesate[gc, n, boole] computes the nth order geodesate of the polygons indicated by boole in the spherical tessellation gc.";
MapProduct::usage = "MapProduct[elem, list] multiplies elem with vectors in list.";
MaskedExp::usage = "MaskedExp[data, mask] returns Exp[data] for all positions indicated by ones in the binary indicator list mask, leaving the remainder unchanged.";
MaskedLog::usage = "MaskedLog[data, mask] returns Log[data] for all positions indicated by ones in the binary indicator list mask, leaving the remainder unchanged.";
MaskedMatrixExp::usage = "MaskedMatrixExp[data, mask] returns MatrixExp[data] if mask is 1, returning data otherwise.";
MaskedMatrixLog::usage = "MaskedMatrixLog[data, mask] returns MatrixLog[data] if mask is 1, returning data otherwise.";
MedicalAxesLabel::usage = "MedicalAxesLabel[spatialUnit, opts] generates axis labels for medical images.";
MergeInput::usage = "MergeInput[gradients, measurements] constructs a single input matrix by joining gradients and measurements at the last level of measurements.";
NestedWith::usage = "NestedWith[{vars}, expr] is a nested form of With.";
NormRescale::usage = "NormRescale[list] rescales each element of list to have a norm that runs from 0 to 1 over the range Min[norm /@ list] to Max[norm /@ list].";
OrderToIndependentComponentsLength::usage = "OrderToIndependentComponentsLength[order] returns the number of independent components in a fully symmetric three dimensional tensor of order order.";
OrderToComponentsLength::usage = "OrderToComponentsLength[order] returns the number of components in a three dimensional tensor of order order.";
OrientationConventionIdentifierQ::usage = "OrientationConventionIdentifierQ[string] tests whether string is a valid three character orientation convention identifier.";
OrientationConventionStringQ::usage = "OrientationConventionStringQ[string] tests whether string is a valid orientation convention string.";
OrientationConventionTransform::usage = "OrientationConventionTransform[conv] gives a TransformationFunction that represents a series of reflections that transform a frame from the neurological (default) convention to conv.";
OrientationFrameSort::usage = "OrientationFrameSort[frame, relframe] sorts the vectors in frame such that their order corresponds to the ordering of the frame rel.";
ParseOptions::usage = "ParseOptions[opt] checks if the provided opt either is of the form \"string\" or the form {\"string\", opts}, and if so returns \"string\" and opts separately.";
PolyhedraVolume::usage = "PolyhedraVolume[points, triangles] computes the volume of a polyhedra specified by polygons defined with the integer lists triangles, indexing the coordinate list points.";
RealSphericalBasisCoefficientsQ::usage = "RealSphericalBasisCoefficientsQ[array] checks whether the dimensions of array conform to a structure containing real spherical basis coefficients.";
RealSphericalBasisLengthQ::usage = "RealSphericalBasisLengthQ[length] checks whether length is a valid length for a vector containing real spherical basis coefficients.";
ResolveSpatialReflections::usage = "ResolveSpatialReflections[array, tfun] returns a list {arrayFixed, tfunFixed} in which a sequence of reflections are applied to array, such that the new object has the same orientation, while tfunFixed is free of reflections.";
SlicePolygon::usage = "SlicePolygon[{{c, n}, ...}, dim] gives polygons that correspond to slices of planes specified by combinations of centers and normals {c, n} with a axes-aligned cuboid with points {0, 0, 0} and dim.";
SphericalAngles::usage = "SphericalAngles[vertices] returns of a list of three dimensional unit vectors the angles theta and phi used in spherical harmonics calculations.";
SphericalBasisCoefficientsQ::usage = "SphericalBasisCoefficientsQ[array] checks whether the dimensions of array conform to a structure containing spherical basis coefficients.";
SphericalBasisLengthQ::usage = "SphericalBasisLengthQ[length] checks whether length is a valid length for a vector containing spherical basis coefficients.";
SphericalDelaunayTriangulation::usage = "SphericalDelaunayTriangulation[vertices] triangulates a set of three-dimensional unit vectors.";
SplitTransformationFunction::usage = "SplitTransformationFunction[TransformationFunction[...]] returns to TransformationFunction objects corresponding to the affine transform (without rotation) and the translation transform present in the supplied transform.";
SquareMatrixTestQ::usage = "SquareMatrixTestQ[expr, test] tests whether expr is a square matrix with elements satisfying test.";
SymmetricToFullTensor::usage = "SymmetricToFullTensor[tensorComponents] generates a nested list such that the part {i_1, i_2, ...} corresponds to the {i_1, i_2, ...} element of the tensor.";
ToDiffusicaIndex::usage = "ToDiffusicaIndex[tfun] gives a new TransformationFunction object that maps vectors in Diffusica array coordinates to tfun coordinates.";
TotalIndependentComponentsLengthToOrder::usage = "TotalIndependentComponentsLengthToOrder[length] returns the order of a sum of fully symmetric three dimensional tensors with length independent components.";
TraceRescale::usage = "TraceRescale[list] rescales each element of list so that the corresponding symmetric matrices have traces running from 0 to 1.";
UndefinedQ::usage = "UndefinedQ[expr] returns True if expr is identical to Undefined, and False otherwise.";
UnitVectors::usage = "UnitVectors[angles] returns a list of unit vectors corresponding to the given spherical angles.";

DataIntensityRange::argval = "The second argument should be either Automatic, or be a list of length two with elements that are either numeric or Automatic.";

Attributes[AnyValueQ] = {HoldFirst};
Attributes[BiasedRound] = {Listable};
Attributes[NestedWith] = {HoldAll};

Options[MedicalAxesLabel] = {"OrientationConvention" -> "Neurological"};
Options[NormRescale] = {NormFunction -> Norm};
Options[RealSphericalBasisLengthQ] = {"Offset" -> 0};
Options[RealSphericalBasisCoefficientsQ] = {"Offset" -> 0};
Options[SphericalBasisLengthQ] = {"Offset" -> 0};
Options[SphericalBasisCoefficientsQ] = {"Offset" -> 0};

SyntaxInformation[NestedWith] = {"ArgumentsPattern" -> {_, _}, "LocalVariables" -> {"Solve", {1, Infinity}}};

Begin["`Private`"]

	AnyValueQ[symbol_Symbol] := Not[
		Attributes[symbol] ===
		DownValues[symbol] === 
		FormatValues[symbol] === 
		NValues[symbol] === 
		OwnValues[symbol] === 
		SubValues[symbol] === 
		UpValues[symbol] === {}
	];
	AnyValueQ[___] := False;
	
	BiasedRound[x_, a_: 1] := a Floor[(x + a/2)/a];

	BlockDiagonalMatrix[base_?MatrixQ] := base;
	BlockDiagonalMatrix[base_?MatrixQ, blocks : _?MatrixQ ..] := Fold[ArrayFlatten[{{#1, 0}, {0, #2}}] &, base, {blocks}];

	CachedSymbol[str_String] := Module[{syms}, Internal`WithLocalSettings[syms = Unprotect[CachedSymbol], CachedSymbol[str] = Symbol[str], Protect[syms]]];

	ComponentsLengthToOrder[len_Integer /; IntegerQ[Sqrt[len]] && len > 0] := Sqrt[len] - 1;

	ConformalAffineTransformationQ[TransformationFunction[mat_]] := Equal @@ Append[Dot @@@ Subsets[mat[[;; -2, ;; -2]]\[Transpose], {2}], 0];
	ConformalAffineTransformationQ[___] := False;

	ConvexHullVolume[points : {{_, _, _} ..}] := Module[{hull = Quiet[TetGenConvexHull[points], {TetGenTetrahedralize::reterr, TetGenConvexHull::tetinst}]}, If[ListQ[hull], PolyhedraVolume @@ hull, 0.]];
	
	ConvexityMeasure[points : {{_, _, _} ..}, surface : {{_Integer, _Integer, _Integer} ...}] := PolyhedraVolume[points, surface] / ConvexHullVolume[points];
	ConvexityMeasure[points : {{_, _, _} ..}] := Block[{vol = PolyhedraVolume[points]}, If[vol =!= 0., vol / ConvexHullVolume[points], 0.]];

	CorrectedSpatialTransformation[tfun : TransformationFunction[mat_List /; SquareMatrixQ[mat] && Length[mat] === 4], cropping : {_Integer, _Integer, _Integer} : {0, 0, 0}, upsampling : {u1_?NumericQ /; u1 != 0, u2_?NumericQ /; u2 != 0, u3_?NumericQ /; u3 != 0} : {1, 1, 1}] := Module[
		{
			correctedMat = mat
		},
  
	  	correctedMat[[;; 3, 4]] = tfun[Reverse[cropping]];
	  	correctedMat[[;; 3, ;; 3]] = DiagonalMatrix[1/Reverse[upsampling]].mat[[;; 3, ;; 3]];
	  
	  	TransformationFunction[correctedMat]
  
  	];

	CorrectedTemporalTransformation[tfun : TransformationFunction[mat_List /; SquareMatrixQ[mat] && Length[mat] === 2], crop_Integer : 0, scale : (u_?NumericQ /; u != 0) : 1] := Module[
		{
			correctedMat = mat
		},
  
	  	correctedMat[[1, 2]] = First[tfun[{crop}]];
	  	correctedMat[[1, 1]] = mat[[1, 1]] / scale;
	  
	  	TransformationFunction[correctedMat]
  
  	];
	
  	DataIntensityRange[data_List] := DataIntensityRange[data, Automatic];
	DataIntensityRange[data_List, Automatic] := {Min[data], Max[data]};
	DataIntensityRange[data_List, {Automatic, max_ ? NumericQ}] := {Min[data], max};
	DataIntensityRange[data_List, {min_ ? NumericQ, Automatic}] := {min, Max[data]};
	DataIntensityRange[data_List, range : {Repeated[_ ? NumericQ, {2}]}] := range;
	DataIntensityRange[data_List, __] := Null /; Message[DataIntensityRange::argval];
	
	DataType[data : _Integer | _Real | _Complex] := Head[data];
	DataType[data_] := NDSolve`FEM`PackedArrayType[data];
	
	DefinedQ[Undefined] := False;
	DefinedQ[___] := True;
	
	DiagonalMatrixQ[m_?SquareMatrixQ] := m === DiagonalMatrix[Diagonal[m]];
	DiagonalMatrixQ[_] := False;
  	
	ExtractPolyhedronVertices[polyhedron_ /; MemberQ[PolyhedronData[], polyhedron], order : _Integer?NonNegative : 0] := DeleteDuplicates[Normalize /@ N[Geodesate[PolyhedronData[polyhedron, "Image"], order + 1][[1, 1]]]];
	ExtractPolyhedronVertices[] := Module[{syms}, Internal`WithLocalSettings[syms = Unprotect[ExtractPolyhedronVertices], ExtractPolyhedronVertices[] = ExtractPolyhedronVertices["Icosahedron", 3], Protect[syms]]];

	FlattenPositions[positions_List] := Flatten[Developer`ToPackedArray @ {positions}, ArrayDepth[positions] - 1];
	FlattenPositions[positions_] := positions;

	FromDiffusicaIndex[n_] := TranslationTransform[ConstantArray[1, {n}]].AffineTransform[Reverse[IdentityMatrix[n], {2}]];
	FromDiffusicaIndex[vec_List /; ArrayQ[vec, 1 | 2]] := ToDiffusicaIndex[Length[vec]][vec];
	FromDiffusicaIndex[tfun_TransformationFunction] := With[{n = Length[First@tfun] - 1}, TranslationTransform[ConstantArray[1, {n}]].AffineTransform[Reverse[IdentityMatrix[n], {2}]].tfun];

	GetPolyhedronComplex[polyhedron_ : "Icosahedron" , order : _Integer?NonNegative : 0] /; MemberQ[PolyhedronData[], polyhedron] := Module[{syms}, 
		
		Internal`WithLocalSettings[
			
			syms = Unprotect[GetPolyhedronComplex], 
			
			GetPolyhedronComplex[polyhedron, order] = Block[{vertices, polygons, indices},
  
		  		{vertices, polygons} = Extract[N@Geodesate[PolyhedronData[polyhedron], order + 1], {{1, 1}, {1, 2, 1}}];
		  		indices = Union[Flatten[polygons]];
		  
		  		GraphicsComplex[
		   			Developer`ToPackedArray@N@Chop@vertices[[indices]],
		   			Polygon[polygons /. Dispatch[Thread[indices -> Range[Length[indices]]]]]
		   		]
		  
		  	],
		
			Protect[syms]
			
		]
		
	]; 
	GetPolyhedronComplex[] := Module[{syms}, Internal`WithLocalSettings[syms = Unprotect[GetPolyhedronComplex], GetPolyhedronComplex[] = GetPolyhedronComplex["Icosahedron", 6], Protect[syms]]];

  	IndependentComponentsLengthToOrder[len_Integer /; len > 0 && IntegerQ[Sqrt[len]-1]] := Sqrt[len]-1;
	
	IndependentComponentsLengthToOrder[len_Integer /; OddQ[Sqrt[1 + 8 len]] && len > 0] := 1/2 (Sqrt[1 + 8 len] - 3);
	
	LocalGeodesateFace[GraphicsComplex[ipts_, Polygon[ind : {{__Integer}..}]], n_, boole_] := Block[{pts = ipts, newpts, PolyhedronOperations`Private`index, count = 0},

		PolyhedronOperations`Private`index[pt_] := PolyhedronOperations`Private`index[pt] = (newpts[++count] = pt; count);

		PolyhedronOperations`Private`index /@ pts;

		With[{prim = Polygon[Flatten[MapThread[If[#2, PolyhedronOperations`Private`geodesatePolygon[pts[[#1]], #1, n, {0, 0, 0}, 1], {#1}] &, {ind, boole}], 1]]},

			GraphicsComplex[newpts /@ Range[count], prim]

		]

	];
	LocalGeodesateFace[{ipts_, ind : {{__Integer}..}}, n_, boole_] := Block[{pts = ipts, newpts, PolyhedronOperations`Private`index, count = 0},

		PolyhedronOperations`Private`index[pt_] := PolyhedronOperations`Private`index[pt] = (newpts[++count] = pt; count);
	
		PolyhedronOperations`Private`index /@ pts;
	
		With[{prim = Flatten[MapThread[If[#2, PolyhedronOperations`Private`geodesatePolygon[pts[[#1]], #1, n, {0, 0, 0}, 1], {#1}] &, {ind, boole}], 1]},
	
			{newpts /@ Range[count], prim}
	
		]

	];
	LocalGeodesate[g_, n_, boole : {(True | False)...}] := (Message[Geodesate::tessv, n, PolyhedronOperations`Private`$DefaultGeodesateN]; LocalGeodesate[g, PolyhedronOperations`Private`$DefaultGeodesateN, boole]) /; !(IntegerQ[n] && Positive[n]);
	LocalGeodesate[g : GraphicsComplex[_, Polygon[ind : {{__Integer}..}]], n_ : 2, boole : {(True | False)...}] := LocalGeodesateFace[g, n, PadRight[boole, Length[ind], True]];
	LocalGeodesate[g : {_, ind : {{__Integer}..}}, n_ : 2, boole : {(True | False)...}] := LocalGeodesateFace[g, n, PadRight[boole, Length[ind], True]];

	MapProduct[f_: Times, elem_List, list_List, depth_Integer: 2] := Map[f[elem, #] &, list, {-depth}];
	
	MaskedExp[values_, mask_] /; Length[values] === Length[mask] := Exp[values (1. - mask)] - mask;
	
	MaskedLog[values_, mask_] /; Length[values] === Length[mask] := Log[values (1. - mask) + mask];
	MaskedLog[values_] := With[{mask = Unitize[Clip[values, {0, Infinity}]]}, Log[mask values + (1 - mask)] + (1 - mask) values];
	
	MaskedMatrixExp[mat_] := MatrixExp[mat];
	MaskedMatrixExp[mat_, 1] := MatrixExp[mat];
	MaskedMatrixExp[mat_, 0] := mat;
	
	MaskedMatrixLog[mat_] := MatrixLog[mat];
	MaskedMatrixLog[mat_, 1] := MatrixLog[mat];
	MaskedMatrixLog[mat_, 0] := mat;
	
	MedicalAxesLabelRule["L", spatialUnit: (_Quantity | _?NumericQ)] := Row[{"Right \[Rule] Left [", spatialUnit, "]"}];
	MedicalAxesLabelRule["R", spatialUnit: (_Quantity | _?NumericQ)] := Row[{"Left \[Rule] Right [", spatialUnit, "]"}];
	MedicalAxesLabelRule["A", spatialUnit: (_Quantity | _?NumericQ)] := Row[{"Posterior \[Rule] Anterior [", spatialUnit, "]"}];
	MedicalAxesLabelRule["P", spatialUnit: (_Quantity | _?NumericQ)] := Row[{"Anterior \[Rule] Posterior [", spatialUnit, "]"}];
	MedicalAxesLabelRule["I", spatialUnit: (_Quantity | _?NumericQ)] := Row[{"Superior \[Rule] Inferior [", spatialUnit, "]"}];
	MedicalAxesLabelRule["S", spatialUnit: (_Quantity | _?NumericQ)] := Row[{"Inferior \[Rule] Superior [", spatialUnit, "]"}];

	MedicalAxesLabel[spatialUnit: (_Quantity | _?NumericQ), str_String ? OrientationConventionIdentifierQ] := MedicalAxesLabelRule[#, spatialUnit] & /@ Characters[str];
	MedicalAxesLabel[spatialUnit: (_Quantity | _?NumericQ), "Neurological"] := MedicalAxesLabel[spatialUnit, "RAS"];	
	MedicalAxesLabel[spatialUnit: (_Quantity | _?NumericQ), "Radiological"] := MedicalAxesLabel[spatialUnit, "LAS"];
	MedicalAxesLabel[spatialUnit: (_Quantity | _?NumericQ)] := MedicalAxesLabel[spatialUnit, "RAS"];
	
	MergeInput[gradients_List /; ArrayDepth[gradients] == 2, measurements_List] := Map[Join[gradients, Partition[#, 1], 2] &, measurements, {-2}];
	
	NestedWith /: (assign : SetDelayed | RuleDelayed)[lhs_,rhs : HoldPattern[NestedWith[{__}, _]]] := Block[{With}, 
    
    	Attributes[With] = {HoldAll};
    	
    	assign[lhs, Evaluate[rhs]]
  
  	];
	NestedWith[{}, expr_] := expr;
	NestedWith[{head_}, expr_] := With[{head}, expr];
	NestedWith[{head_, tail__}, expr_] := Block[{With}, 
		
		Attributes[With] = {HoldAll};
    	
    	With[{head}, Evaluate[NestedWith[{tail}, expr]]]
    	
    ];

	NormRescale[data : {__List}, args : Repeated[{_, _}, {0, 2}], OptionsPattern[]] := Module[{norms = Map[OptionValue[NormFunction], data, {-2}]}, Unitize[norms] Rescale[norms, args]/(norms + 1 - Unitize[norms]) data];
	
	OrderToComponentsLength[n_Integer /; n > 0] := (n + 1)^2;
	
	OrderToIndependentComponentsLength[n_Integer /; n > 0] := 1/2 (n + 1)(n + 2);

	OrientationConventionStringQ["Neurological"] = True;
	OrientationConventionStringQ["Radiological"] = True;
	OrientationConventionStringQ[expr_] := OrientationConventionIdentifierQ[expr];  

	OrientationConventionIdentifierQ[str_String /; StringLength[str] === 3] := TrueQ[(Times @@ Characters[str] /. {"R" | "L" -> 1, "A" | "P" -> 2, "S" | "I" -> 3}) === 6];
	OrientationConventionIdentifierQ[___] := False;

	OrientationAngle = Compile[{{u, _Real, 1}, {v, _Real, 1}}, Re@ArcCos@Abs[u.v], RuntimeAttributes -> Listable];
	
	OrientationConventionTransform[convention_String ? OrientationConventionIdentifierQ] := AffineTransform[
	
		Transpose[DiagonalMatrix[(Characters[ToUpperCase@convention] /. {"R" | "A" | "S" -> 1, "L" | "P" | "I" -> -1})][[(StringPosition[convention, #, IgnoreCase -> True] & /@ {"L" | "R", "P" | "A", "I" | "S"})[[All, 1, 1]]]]]
		
	];
	OrientationConventionTransform["Neurological"] = OrientationConventionTransform["RAS"];
	OrientationConventionTransform["Radiological"] = OrientationConventionTransform["LAS"];
	OrientationConventionTransform[] = OrientationConventionTransform["RAS"];

	OrientationFrameDistance = Compile[{{frame1, _Real, 2}, {frame2, _Real, 2}}, Total[OrientationAngle[frame1, frame2]], RuntimeAttributes -> Listable];
	
	OrientationFrameOrdering[frame_List /; SquareMatrixTestQ[frame, NumericQ], rel : (_List | "Cartesian") : "Cartesian"] := Module[
	  	{
	   		len = Length[frame],
	   		ordering
	   	},
		  
	  	Which[
		   
	   		rel === "Cartesian",
	   		ordering = Ordering[OrientationFrameDistance[#, IdentityMatrix[len]] & /@ Permutations[frame]],
	   
	   		SquareMatrixTestQ[rel, NumericQ] && Det[rel] =!= 0 && Length[rel] === len,
	   		ordering = Ordering[OrientationFrameDistance[#, rel] & /@ Permutations[frame]],
	   
	   		True,
	   		(*Message*)Return[$Failed]
	   	
	   	];
	   	
	   	Permutations[Range[len]][[First@ordering]]
	  
	];
	
	OrientationFrameSort[frame : {{_Real ..} ..}, rel : {{_Real ..} ..}] := Block[{optimum, permFrames},
 
  		optimum = Ordering[OrientationFrameDistance[permFrames = Permutations[frame, {Length[rel]}], rel], 1];
  		OrientVector[rel, Extract[permFrames, optimum]]

  	];
  	
  	OrientVector = Compile[{{ref, _Real, 1}, {vec, _Real, 1}}, If[NonNegative[vec.ref], vec, -vec], RuntimeAttributes -> Listable];

	ParseOptions[{name : (_String | _Symbol), opts : OptionsPattern[]}] := {name, {opts}};
	ParseOptions[name : (_String | _Symbol)] := {name, {}};
	
	PlaneCuboidIntersections[{c_, n : {n1_, n2_, n3_}}, d : {dx_, dy_, dz_}] := Block[{dn1, dn2, dn3, cn, pts, p, v1, v2}, 
		
		{dn1, dn2, dn3} = d n; 
		cn = c.n;
  
  		pts = DeleteDuplicates[Join @@ Pick[Hold[
  			Select[{{cn/n1, 0, 0}, {(cn - dn3)/n1, 0, dz}, {(cn - dn2)/n1, dy, 0}, {(cn - dn2 - dn3)/n1, dy, dz}}, 0 <= #[[1]] <= dx &],
  			Select[{{0, cn/n2, 0}, {0, (cn - dn3)/n2, dz}, {dx, (cn - dn1)/n2, 0}, {dx, (cn - dn1 - dn3)/n2, dz}}, 0 <= #[[2]] <= dy &],
       		Select[{{0, 0, cn/n3}, {0, dy, (cn - dn2)/n3}, {dx, 0, (cn - dn1)/n3}, {dx, dy, (cn - dn1 - dn2)/n3}}, 0 <= #[[3]] <= dz &]
       	], Unitize @ n, 1], Equal[#1, #2] &]; (*Because of bug with Equal in v9 *)
       	
       	If[pts =!= {},
       	
       		p = With[{center = -First@pts}, 
       	
	       		Compile[{{v1, _Real, 1}, {v2, _Real, 1}},
	       	
	       			NonNegative[Cross[v1 + center, v2 + center].n]
	       			
	       		]
	       	
       		];
       	
       		Sort[pts, p],
       	
       		{}
       		
       	]
       	
	];
	
	(*COMPILE*)(**)
	PolyhedraSubarea = Compile[{{u, _Real, 1}, {v, _Real, 1}, {w, _Real, 1}}, Norm[Cross[v - u, w - u]]/2];
	PolyhedraSubvolume = Compile[{{u, _Real, 1}, {v, _Real, 1}, {w, _Real, 1}}, Abs[u.Cross[v - u, w - u]]/2];
	PolyhedraAreaList[points : {{_,_,_}..}, surface : {{_Integer, _Integer, _Integer}...}] := Module[{triangles = points[[#]] & /@ surface}, PolyhedraSubarea@@@triangles];
	PolyhedraVolumeList[points : {{_,_,_}..}, surface : {{_Integer, _Integer, _Integer}...}] := Module[{triangles = points[[#]] & /@ surface}, PolyhedraSubvolume@@@triangles];
	PolyhedraVolume[points : {{_, _, _} ..}, surface : {{_Integer, _Integer, _Integer} ...}] := Total[PolyhedraVolumeList[points, surface]]/3;
	PolyhedraVolume[points : {{_, _, _} ..}] := Module[{gc = SphericalDelaunayTriangulation[points]}, If[Head[gc] === GraphicsComplex, PolyhedraVolume[points, gc[[2, 1]]], 0.]]; 

	RealSphericalBasisLengthQ[len_Integer, OptionsPattern[]] := True /; OddQ[Sqrt[1 + 8 (len - OptionValue["Offset"])]];
	RealSphericalBasisLengthQ[___] := False;

	RealSphericalBasisCoefficientsQ[array_List /; ArrayQ[array], order_Integer /; NonNegative[order] && EvenQ[order], OptionsPattern[]] := True /; (Last[Dimensions[array]] - OptionValue["Offset"]) === OrderToIndependentComponentsLength[order];
	RealSphericalBasisCoefficientsQ[array_List /; ArrayQ[array], opts : OptionsPattern[]] := RealSphericalBasisLengthQ[Last[Dimensions[array]], opts];
	RealSphericalBasisCoefficientsQ[___] := False;
	
	(*Goes Virtual -> VirtualAligned*)
	ResolveSpatialReflections[array_List?ArrayQ, tfun : TransformationFunction[mat_]] /; (ArrayDepth[array] >= Length[mat] - 1) := Module[
		{
			frame = Transpose[mat[[;; -2, ;; -2]]],
			ordering, signs
		},
		
		ordering = OrientationFrameOrdering[frame];
		frame = frame[[ordering]];
		signs = Sign@Diagonal[(*Transpose*)frame];
		
		{Reverse[Transpose[array, ordering], Position[Reverse @ signs, -1][[All, 1]]], AffineTransform[Transpose[frame] signs]}
		
	];
	
	ResolveSpatialReflections[tfun : TransformationFunction[mat_]] := Module[
		{
			frame = Transpose[mat[[;; -2, ;; -2]]],
			ordering, signs
		},
		
		ordering = OrientationFrameOrdering[frame];
		frame = frame[[ordering]];
		signs = Sign@Diagonal[(*Transpose*)frame];
		
		AffineTransform[Transpose[frame] signs]
		
	];
	
	SlicePolygon[mats : {{{_, _, _}, {_, _, _}} ...}, dim : {_, _, _} /; VectorQ[dim, Positive]] := With[{pts = N@DeleteCases[PlaneCuboidIntersections[#, dim] & /@ mats, {}]},
 
 		If[
 			Length[pts] >= 1,
 			Polygon[pts, VertexTextureCoordinates -> Map[#/dim &, pts, {-2}]],
 			Polygon[{}]
 		]

  	];
  	SlicePolygon[mat : {{_, _, _}, {_, _, _}}, dim : {_, _, _} /; VectorQ[dim, Positive]] := SlicePolygon[{mat}, dim];

	SphericalAngles[vectors : {{_, _, _} ..}] := Transpose[{Arg[vectors[[All, 3]] + I Norm /@ vectors[[All, {1, 2}]]], Arg[vectors[[All,1]] + I vectors[[All,2]]]}]; 

	SphericalBasisLengthQ[len_Integer, OptionsPattern[]] := True /; IntegerQ[Sqrt[len - OptionValue["Offset"]]];
	SphericalBasisLengthQ[___] := False;

	SphericalBasisCoefficientsQ[array_List /; ArrayQ[array], order_Integer /; NonNegative[order], OptionsPattern[]] := True /; (Last[Dimensions[array]] - OptionValue["Offset"]) === OrderToComponentsLength[order];
    SphericalBasisCoefficientsQ[array_List /; ArrayQ[array], opts : OptionsPattern[]] := SphericalBasisLengthQ[Last[Dimensions[array]], opts];
	SphericalBasisCoefficientsQ[___] := False;

	SphericalDelaunayTriangulation[vertices_List /; ArrayQ[vertices, 2, NumericQ] && Length[vertices] >= 3 && Dimensions[vertices][[2]] === 3] := Module[{syms}, 
		
		Internal`WithLocalSettings[
			
			syms = Unprotect[SphericalDelaunayTriangulation], 
			
			SphericalDelaunayTriangulation[vertices] = Block[
				{
					unitvecs = Normalize /@ vertices, 
					instance
				},
		
				(*Check*)
				If[DeleteDuplicates[unitvecs] =!= unitvecs, (*Message[]*)Return[$Failed]];
		
				(*Create and initialize instance*)		
				instance = TetGenCreate[];
				TetGenSetPoints[instance, unitvecs];
		
				(*Triangulate sphere*)
		   		GraphicsComplex[unitvecs, Polygon[TetGenGetFaces[TetGenTetrahedralize[instance, ""]]]]
		   		
		   	], 
   	
   			Protect[syms]
   			
   		]
   	
   	]; 
	
	SplitTransformationFunction[TransformationFunction[mat_]] := TransformationFunction /@ QRDecomposition[mat];
	
	SquareMatrixTestQ[expr_, test_] := TrueQ[SquareMatrixQ[expr] && MatrixQ[expr, test]];
	
	SymmetricToFullTensor[tensorComponents_List ? RealSphericalBasisCoefficientsQ] := Module[
		{
			compdim, dim, n
		},
		
		compdim = Last[Dimensions[tensorComponents]];
		n = IndependentComponentsLengthToOrder[compdim];
		dim = ConstantArray[3, {n}];
		
		With[{f = Evaluate[Quiet[Normal@SymmetrizedArray[Thread[SymmetrizedIndependentComponents[dim, Symmetric[All]] -> Table[Slot[][[i]], {i, compdim}]], dim, Symmetric[All]]]] &},
			
			Map[
				f,
				tensorComponents, 
				{ArrayDepth[tensorComponents] - 1}
			]
		
		]
		
	];
	SymmetricToFullTensor[Undefined, _] = SymmetricToFullTensor[Undefined] = Undefined;
	
	FullToSymmetricTensor[tensors_List, depth_Integer ? Positive] := Module[
		{
			dim = Dimensions[tensors][[depth + 1 ;; ]]
		},
		
		Transpose[Part[tensors, ##] & @@@ (Join[ConstantArray[All, {depth}], #] & /@ SymmetrizedIndependentComponents[dim, Symmetric[All]]), Prepend[Range[depth], depth + 1]] /; Equal @@ dim
		
	];
	FullToSymmetricTensor[Undefined, _] = FullToSymmetricTensor[Undefined] = Undefined;
	
	ToDiffusicaIndex[n_] := AffineTransform[Reverse[IdentityMatrix[n], {2}]].TranslationTransform[ConstantArray[-1, {n}]];
	ToDiffusicaIndex[vec_List /; ArrayQ[vec, 1 | 2]] := ToDiffusicaIndex[Length[vec]][vec];
	ToDiffusicaIndex[tfun_TransformationFunction] := With[{n = Length[First@tfun] - 1}, tfun.AffineTransform[Reverse[IdentityMatrix[n], {2}]].TranslationTransform[ConstantArray[-1, {n}]]];

	With[{f = Function[len, Evaluate[N[-(5/2) + 7/(2 3^(1/3) (405 + 432 len + 2 Sqrt[3] Sqrt[13583 + 29160 len + 15552 len^2])^(1/3)) + (405 + 432 len + 2 Sqrt[3] Sqrt[13583 + 29160 len + 15552 len^2])^(1/3)/(2 3^(2/3)), $MaxExtraPrecision]]]},
		
		TotalIndependentComponentsLengthToOrder[len_Integer] := Module[{order = Internal`TestIntegerQ[f[len]]},
			
			Round[order[[1]]] /; order[[2]]
			
		]
  	
  	];

	TraceRescale[data : {__List} /; Last[Dimensions[data]] === 6, args : Repeated[{_, _}, {0, 2}]] := Module[{traces = Map[(#[[1]] + #[[4]] + #[[6]]) / 3 &, data, {-2}]}, Rescale[traces, args]/traces data];

	UndefinedQ[Undefined] := True;
	UndefinedQ[___] := False;
	
	UnitVectors[angles : {{_, _} ..}] := Transpose[{Sin[angles[[All, 1]]] Cos[angles[[All, 2]]], Sin[angles[[All, 1]]] Sin[angles[[All, 2]]], Cos[angles[[All, 1]]]}];

End[];

(*Bulletproofing*)
SetAttributes[
	{
		AnyValueQ, 
		BiasedRound,
		BlockDiagonalMatrix,
		CachedSymbol, 
		ConformalAffineTransformationQ,
		ConvexHullVolume, 
		ConvexityMeasure,
		CorrectedSpatialTransformation,
		CorrectedTemporalTransformation,
		DataIntensityRange,
		DataType,
		DefinedQ,
		DiagonalMatrixQ,
		ExtractPolyhedronVertices,
		FromDiffusicaIndex,
		FullToSymmetricTensor, 
		GetPolyhedronComplex,
		IndependentComponentsLengthToOrder,
		LocalGeodesate,
		MapProduct, 
		MaskedExp,
		MaskedLog,
		MaskedMatrixExp,
		MaskedMatrixLog,
		MedicalAxesLabel, 
		MergeInput, 
		NormRescale,
		OrderToIndependentComponentsLength,
		OrientationConventionIdentifierQ,
		OrientationConventionStringQ,
		OrientationConventionTransform,
		OrientationFrameSort,
		ParseOptions,
		PolyhedraVolume,
		ResolveSpatialReflections,
		SlicePolygon,
		SphericalAngles,
		RealSphericalBasisCoefficientsQ,
		RealSphericalBasisLengthQ,
		SphericalDelaunayTriangulation,
		SplitTransformationFunction,
		SquareMatrixTestQ,
		SymmetricToFullTensor,
		ToDiffusicaIndex,
		TotalIndependentComponentsLengthToOrder,
		TraceRescale,
		UndefinedQ,
		UnitVectors
	}, 
	{Protected, ReadProtected}
];

EndPackage[]
