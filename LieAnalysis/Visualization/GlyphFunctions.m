(*************************************************************************************************************************
** GlyphFunctions.m
** This .m file contains functions to visualize glyphs
**
** Author: F.C. Martin <f.c.martin@tue.nl>
**************************************************************************************************************************)

(*
	Copyright 2016 Tom Dela Haije

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*)

BeginPackage["LieAnalysis`Visualization`GlyphFunctions`",{
  "LieAnalysis`Visualization`ColorFunctions`",
  "LieAnalysis`Visualization`Common`",
  "LieAnalysis`Common`"
}];



  Glyph::usage = "Glyph[id][args] generates glyphs of type id from args.";

  Options[Glyph] = {"ConeOrientation" -> "Outside", "DualCones" -> True, "SphereGeometry" -> Automatic};



  Begin["`Private`"];



    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
    (* PolygonNormals- - - - - - - - - - - - - - - - - - - - - - *)
    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)



    PolygonNormals = Compile[{{p1, _Real, 1}, {p2, _Real, 1}, {p3, _Real, 1}}, Normalize[(p2 - p1)\[Cross](p3 - p1)], RuntimeAttributes -> {Listable}];

    GraphicsComplexNormals[GraphicsComplex[points_List, Polygon[triangles_List]]] := Module[
      {
        normals = PolygonNormals @@ (points[[#]] & /@ Transpose[triangles]),
        members = Position[triangles, #][[All, 1]] & /@ Range[Length[points]](*Speed-up*)
      },

      Mean[normals[[#]]] & /@ members

    ];

    SphericalHarmonicGlyphFunction[GraphicsComplex[vertices_List, polygon_Polygon], n_Integer ? NonNegative] := SphericalHarmonicGlyphFunction[GraphicsComplex[vertices, polygon], n] = Module[
      {
        angles = SphericalAngles[vertices],
        order = IndependentComponentsLengthToOrder[n],
        (*order = (n - 1)/2,*)
        list, listNormals
      },

      list = Developer`ToPackedArray @ Chop @ Flatten[Table[Apply[RealSphericalHarmonicY[l, m, ##] &, angles, {1}], {l, 0, order}, {m, -l, l}], 1];
      listNormals = Developer`ToPackedArray @ Chop @ Flatten[Table[Apply[RealSphericalHarmonicYNormal[l, m, ##] &, angles, {1}], {l, 0, order}, {m, -l, l}], 1];

      GraphicsComplex[vertices #.list, polygon(*, VertexNormals -> With[{normals = #.listNormals}, normals Sign@MapThread[Dot, {vertices, normals}]]*)] &

    ];

    NSphericalHarmonicGlyphFunction[GraphicsComplex[vertices_List, polygon_Polygon], n_Integer ? NonNegative] := NSphericalHarmonicGlyphFunction[GraphicsComplex[vertices, polygon], n] = Module[
      {
        angles = SphericalAngles[vertices],
        order = IndependentComponentsLengthToOrder[n],
        list
      },

      list = Developer`ToPackedArray @ Chop @ Flatten[Table[Apply[RealSphericalHarmonicY[l, m, ##] &, angles, {1}], {l, 0, order, 1}, {m, -l, l}], 1];

      With[{points = vertices #.list}, GraphicsComplex[points, polygon, VertexNormals -> GraphicsComplexNormals[GraphicsComplex[points, polygon]]]] &

    ];

    (*FIX*)
    TensorGlyphFunction[GraphicsComplex[vertices_List, polygon_Polygon], n_Integer ? NonNegative] := TensorGlyphFunction[GraphicsComplex[vertices, polygon], n] = Module[
      {
        angles = SphericalAngles[vertices],
        order = IndependentComponentsLengthToOrder[n],
        list, listNormals
      },

      list = Developer`ToPackedArray @ Chop @ Flatten[Table[Apply[RealSphericalHarmonicY[l, m, ##] &, angles, {1}], {l, 0, order, 2}, {m, -l, l}], 1];
      listNormals = Developer`ToPackedArray @ Chop @ Flatten[Table[Apply[RealSphericalHarmonicYNormal[l, m, ##] &, angles, {1}], {l, 0, order, 2}, {m, -l, l}], 1];

      GraphicsComplex[vertices #.list, polygon, VertexNormals -> With[{normals = #.listNormals}, normals (2 UnitStep[MapThread[Dot, {vertices, normals}]] - 1)]] &

    ];

    VertexGlyphFunction[GraphicsComplex[vertices_List, polygon_Polygon]] := VertexGlyphFunction[GraphicsComplex[vertices, polygon]] = With[{points = vertices #}, GraphicsComplex[points, polygon, VertexNormals -> GraphicsComplexNormals[GraphicsComplex[points, polygon]]]] &

    Glyph[Automatic][positions_, coefficients_, args__] := Glyph[

      Switch[

        Dimensions[coefficients][[2]],

        1,
        "Sphere",

        3,
        "Arrow",

        4,
        "Cylinder",

        6,
        "Ellipsoid",

        _,
        "SphericalHarmonic"

      ]

    ][positions, coefficients, args];

    Glyph["Arrow"][positions_, coefficients_ /; Dimensions[coefficients][[2]] === 3, colorFunction_, colorFunctionScaling_, plotStyle_, glyphScaling_, performance_, ctfun_, OptionsPattern[]] := Module[
      {
        color = colorFunction /. Automatic -> OrientationRGBColor,
        scale = colorFunctionScaling /. Automatic -> False
      },

      Append[
        Flatten[{plotStyle /. {Automatic -> {}, None -> {}}}],
        GeometricTransformation[{
          MapThread[{color[Sequence @@ #3], Arrow[{#1, #1 + #2}]} &, {positions, glyphScaling coefficients, If[TrueQ[scale], NormRescale, Identity][coefficients]}]
        }, ctfun]
      ]

    ];

    Glyph["Cone"][positions_, coefficients_ /; Dimensions[coefficients][[2]] === 4, colorFunction_, colorFunctionScaling_, plotStyle_, glyphScaling_, performance_, ctfun_, opts : OptionsPattern[]] := Module[
      {
        dual = TrueQ[OptionValue[Glyph, {opts}, "DualCones"]],
        inside = OptionValue[Glyph, {opts}, "ConeOrientation"] === "Inside",
        color = colorFunction /. str_String :> Check[ColorData[str][#4] &, Automatic] /. Automatic -> OrientationRGBColor,
        scale = colorFunctionScaling /. {False | {False} -> {False, False}, True | {True} -> {True, True}, Except[{(True | False) ..}] -> {False, False}}
      },

      Append[
        Flatten[{EdgeForm[], plotStyle /. {Automatic -> {}, None -> {}}}],
        GeometricTransformation[{
          With[
            {
              cfun = Which[
                Not[dual] && inside,
                Cone[{#1 + #2, #1 - #2}, #3],
                Not[dual] && Not[inside],
                Cone[{#1 - #2, #1 + #2}, #3],
                dual && inside,
                Cone[{{#1, #1 + #2}, {#1, #1 - #2}}, #3],
                dual && Not[inside],
                Cone[{{#1 - #2, #1}, {#1 + #2, #1}}, #3]
              ]
            },

            MapThread[{color[Sequence @@ #4, #5], cfun} &,
              {
                positions,
                glyphScaling coefficients[[All, ;; 3]],
                glyphScaling coefficients[[All, 4]],
                If[scale[[1]], NormRescale, Identity] @ coefficients[[All, 1 ;; 3]],
                If[scale[[2]], Rescale, Identity] @ coefficients[[All, 4]]
              }
            ]

          ]
        }, ctfun]
      ]

    ];

    Glyph["Cuboid"][positions_, coefficients_, colorFunction_, colorFunctionScaling_, plotStyle_, glyphScaling_, performance_, ctfun_, OptionsPattern[]] := Module[
      {
        color = colorFunction /. Automatic -> EllipsoidRGBColor,
        scale = colorFunctionScaling /. Automatic -> False
      },

      Append[
        Flatten[{plotStyle /. {Automatic -> {}, None -> {}}}],
        GeometricTransformation[{
          MapThread[{color[Sequence @@ #3], GeometricTransformation[Sphere[], {#2, #1}]} &, {positions, SymmetricToFullTensor[glyphScaling coefficients], If[TrueQ[scale], Rescale, Identity][coefficients]}]
        }, ctfun]
      ]
    ];

    Glyph["Cylinder"][positions_, coefficients_ /; Dimensions[coefficients][[2]] === 4, colorFunction_, colorFunctionScaling_, plotStyle_, glyphScaling_, performance_, ctfun_, OptionsPattern[]] := Module[
      {
        color = colorFunction /. str_String :> Check[ColorData[str][#4] &, Automatic] /. Automatic -> OrientationRGBColor,
        scale = colorFunctionScaling /. {False | {False} -> {False, False}, True | {True} -> {True, True}, Except[{(True | False) ..}] -> {False, False}}
      },

      Append[
        Flatten[{EdgeForm[], plotStyle /. {Automatic -> {}, None -> {}}}],
        GeometricTransformation[{
          MapThread[{color[Sequence @@ #4, #5], Cylinder[{#1 - #2, #1 + #2}, #3]} &,
            {
              positions,
              glyphScaling coefficients[[All, ;; 3]],
              glyphScaling coefficients[[All, 4]],
              If[scale[[1]], NormRescale, Identity] @ coefficients[[All, 1 ;; 3]],
              If[scale[[2]], Rescale, Identity] @ coefficients[[All, 4]]
            }
          ]
        }, ctfun]
      ]

    ];

    Glyph["Dot"][positions_, coefficients_, colorFunction_, colorFunctionScaling_, plotStyle_, glyphScaling_, performance_, ctfun_, OptionsPattern[]] := Append[Flatten[{PointSize[0], plotStyle /. {Automatic -> {}, None -> {}}}], GeometricTransformation[Point[positions], ctfun]];

    Glyph["Ellipsoid"][positions_, coefficients_ /; Dimensions[coefficients][[2]] === 6, colorFunction_, colorFunctionScaling_, plotStyle_, glyphScaling_, performance_, ctfun_, OptionsPattern[]] := Module[
      {
        color = colorFunction /. str_String :> Check[ColorData[str][(#1 + #4 + #6)/3] &, Automatic] /. Automatic -> EllipsoidRGBColor[1],
        scale = colorFunctionScaling /. Automatic -> False
      },

	
      Append[
        Flatten[{plotStyle /. {Automatic -> {}, None -> {}}}],
        GeometricTransformation[{
          MapThread[{color[Sequence @@ #3], GeometricTransformation[Sphere[], {LinearAlgebra`MatrixSqrt[#2], #1}]} &, {positions, SymmetricToFullTensor[glyphScaling coefficients], If[TrueQ[scale], TraceRescale, Identity][coefficients]}]
        }, ctfun]
      ]

    ];

    Glyph["EllipsoidSharp"][positions_, coefficients_ /; Dimensions[coefficients][[2]] === 6, colorFunction_, colorFunctionScaling_, plotStyle_, glyphScaling_, performance_, ctfun_, OptionsPattern[]] := Module[
      {
        color = colorFunction /. str_String :> Check[ColorData[str][(#1 + #4 + #6)/3] &, Automatic] /. Automatic -> EllipsoidRGBColor[1],
        scale = colorFunctionScaling /. Automatic -> False
      },

      Append[
        Flatten[{plotStyle /. {Automatic -> {}, None -> {}}}],
        GeometricTransformation[{
          MapThread[{color[Sequence @@ #3], GeometricTransformation[Sphere[], {#2, #1}]} &, {positions, SymmetricToFullTensor[glyphScaling coefficients], If[TrueQ[scale], TraceRescale, Identity][coefficients]}]
        }, ctfun]
      ]

    ];

    Glyph["Line"][positions_, coefficients_ /; Dimensions[coefficients][[2]] === 3, colorFunction_, colorFunctionScaling_, plotStyle_, glyphScaling_, performance_, ctfun_, OptionsPattern[]] := Module[
      {
        color = colorFunction /. Automatic -> OrientationRGBColor,
        scale = colorFunctionScaling /. Automatic -> False
      },

      Append[
        Flatten[{plotStyle /. {Automatic -> {}, None -> {}}}],
        GeometricTransformation[{
          MapThread[{color[Sequence @@ #3], Line[{#1 - #2, #1 + #2}]} &, {positions, glyphScaling coefficients, If[TrueQ[scale], NormRescale, Identity][coefficients]}]
        }, ctfun]
      ]

    ];

    Glyph["Point"][positions_, coefficients_ /; Dimensions[coefficients][[2]] === 1, colorFunction_, colorFunctionScaling_, plotStyle_, glyphScaling_, performance_, ctfun_, OptionsPattern[]] := Module[
      {
        color = colorFunction /. str_String :> Check[ColorData[str], Automatic] /. Automatic -> ColorData["ThermometerColors"],
        scale = colorFunctionScaling /. Automatic -> True
      },

      Append[
        Flatten[{plotStyle /. {Automatic -> {}, None -> {}}}],
        GeometricTransformation[{
          MapThread[{color[#3[[1]]], PointSize[#2[[1]]], Point[#1]} &, {positions, glyphScaling coefficients, If[TrueQ[scale], Rescale, Identity][coefficients]}]
        }, ctfun]
      ]

    ];

    Glyph["Sphere"][positions_, coefficients_ /; Dimensions[coefficients][[2]] === 1, colorFunction_, colorFunctionScaling_, plotStyle_, glyphScaling_, performance_, ctfun_, OptionsPattern[]] := Module[
      {
        color = colorFunction /. str_String :> Check[ColorData[str], Automatic] /. Automatic -> ColorData["ThermometerColors"],
        scale = colorFunctionScaling /. Automatic -> True
      },

      Append[
        Flatten[{plotStyle /. {Automatic -> {}, None -> {}}}],
        GeometricTransformation[{
          MapThread[{color[#3[[1]]], Sphere[#1, #2]} &, {positions, glyphScaling coefficients, If[TrueQ[scale], Rescale, Identity][coefficients]}]
        }, ctfun]
      ]

    ];

    Glyph["SphericalHarmonic"][positions_, coefficients_, colorFunction_, colorFunctionScaling_, plotStyle_, glyphScaling_, performance_, ctfun_, opts : OptionsPattern[]] := Module[
      {
        color = colorFunction /. Automatic -> OrientationRGBColor,
        scale = colorFunctionScaling /. Automatic -> False,
        sphere = OptionValue[Glyph, {opts}, "SphereGeometry"] /. Automatic -> If[performance, GetPolyhedronComplex[], GetPolyhedronComplex[3]]
      },
		
      With[{glyph = SphericalHarmonicGlyphFunction[sphere, Last[Dimensions[coefficients]]]},

        Append[
          Flatten[{plotStyle /. {Automatic -> {}, None -> {}}}],
          GeometricTransformation[{
            EdgeForm[],
            Specularity[White, 10],
            MapThread[Translate[ApplyGraphicsComplexColorFunction[color, glyph[#2], scale], #1] &, {positions, glyphScaling coefficients}]
          }, ctfun]
        ]

      ]

    ];

    Glyph["Superquadric"][positions_, coefficients_, colorFunction_, colorFunctionScaling_, plotStyle_, glyphScaling_, performance_, ctfun_, OptionsPattern[]] := Module[
      {
        color = colorFunction /. Automatic -> EllipsoidRGBColor,
        scale = colorFunctionScaling /. Automatic -> False
      },

      Append[
        Flatten[{plotStyle /. {Automatic -> {}, None -> {}}}],
        GeometricTransformation[{
          MapThread[{color[Sequence @@ #3], GeometricTransformation[Sphere[], {#2, #1}]} &, {positions, SymmetricToFullTensor[glyphScaling coefficients], If[TrueQ[scale], Rescale, Identity][coefficients]}]
        }, ctfun]
      ]
    ];

    Glyph["Tensor"][positions_, coefficients_, colorFunction_, colorFunctionScaling_, plotStyle_, glyphScaling_, performance_, ctfun_, OptionsPattern[]] := Module[
      {
        color = colorFunction /. Automatic -> EllipsoidRGBColor,
        scale = colorFunctionScaling /. Automatic -> False
      },

      Append[
        Flatten[{plotStyle /. {Automatic -> {}, None -> {}}}],
        GeometricTransformation[{
          MapThread[{color[Sequence @@ #3], GeometricTransformation[Sphere[], {#2, #1}]} &, {positions, SymmetricToFullTensor[glyphScaling coefficients], If[TrueQ[scale], Rescale, Identity][coefficients]}]
        }, ctfun]
      ]
    ];

    (*Add symmetrization option*)
    Glyph["Vertex"][positions_, coefficients_, colorFunction_, colorFunctionScaling_, plotStyle_, glyphScaling_, performance_, ctfun_, opts : OptionsPattern[]] := Module[
      {
        color = colorFunction /. Automatic -> OrientationRGBColor,
        scale = colorFunctionScaling /. Automatic -> False,
        sphere = OptionValue[Glyph, {opts}, "SphereGeometry"] /. Automatic -> If[performance, GetPolyhedronComplex[], GetPolyhedronComplex[3]]
      },

      With[{glyph = VertexGlyphFunction[sphere]},

        Append[
          Flatten[{plotStyle /. {Automatic -> {}, None -> {}}}],
          GeometricTransformation[{
            EdgeForm[],
            Specularity[White, 10],
            MapThread[Translate[ApplyGraphicsComplexColorFunction[color, glyph[#2], scale], #1] &, {positions, glyphScaling coefficients}]
          }, ctfun]
        ]

      ]

    ];

    (*Glyph[sphere : GraphicsComplex[_List, Polygon[_List]]][positions_, coefficients_, colorFunction_, colorFunctionScaling_, plotStyle_, glyphScaling_, performance_, ctfun_, opts : OptionsPattern[]] := Module[
        {
          color = colorFunction /. Automatic -> OrientationRGBColor,
          scale = colorFunctionScaling /. Automatic -> False
        },

        With[{glyph = VertexGlyphFunction[sphere]},

          Append[
            Flatten[{plotStyle /. {Automatic -> {}, None -> {}}}],
            GeometricTransformation[{
              EdgeForm[],
              Specularity[White, 10],
              MapThread[Translate[ApplyGraphicsComplexColorFunction[color, glyph[#2], scale], #1] &, {positions, glyphScaling coefficients}]
            }, ctfun]
          ]

        ]

      ];*)

    (*Glyph[vertices : {{_, _, _} ..}][args__] := Glyph[SphericalDelaunayTriangulation[vertices]][args];*)

    Glyph[_String | _Symbol][args___] := (*Message[]*){};



  End[];



EndPackage[];