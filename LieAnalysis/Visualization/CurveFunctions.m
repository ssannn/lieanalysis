(*************************************************************************************************************************
** CurveFunctions.m
** This .m file contains functions for curve visualization
**
** Author: F.C. Martin <f.c.martin@tue.nl>
** Version: 0.1
**************************************************************************************************************************)

(*
	Copyright 2016 Tom Dela Haije

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*)

BeginPackage["LieAnalysis`Visualization`CurveFunctions`"];



  GenerateCurve::usage = "Curve[id][args] generates curves of type id from args.";

  Options[GenerateCurve] = {"LineFunction" -> Automatic};



  Begin["`Private`"];


    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
    (* GenerateCurve - - - - - - - - - - - - - - - - - - - - - - *)
    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)



    Options[GenerateCurve] = {
      "LineFunction" -> Automatic
    };



    GenerateCurve[Automatic][coefficients_, colorFunction_, colorFunctionScaling_, plotStyle_, performance_, ctfun_, opts : OptionsPattern[]] := If[performance,
      GenerateCurve["Tube"],
      GenerateCurve["Line"]
    ][coefficients, colorFunction, colorFunctionScaling, plotStyle, performance, ctfun, opts];



    GenerateCurve["Tube"][coefficients_, colorFunction_, colorFunctionScaling_, plotStyle_, performance_, ctfun_, opts : OptionsPattern[]] := Module[
      {
        color = colorFunction /. str_String :> Check[ColorData[str][#] &, Automatic] /. Automatic -> (ColorData["ThermometerColors"][#] &),
        scale = colorFunctionScaling /. {False | {False} -> {False, False}, True | {True} -> {True, True}, Except[{(True | False) ..}] -> {True, True}},
        line = OptionValue[GenerateCurve, {opts}, "LineFunction"] /. Automatic -> If[performance, BSplineCurve, Line]
      },

      GeometricTransformation[{
        MapThread[
          Tube[line[#2], .02, VertexColors -> MapThread[Function[{x, y, z}, color[#1, x, y, z]], Transpose[#3]]]&,
            {

              If[scale[[1]],
                Rescale,
                Identity
              ][Range[Length[coefficients]]],

              coefficients,

              If[scale[[2]],
                NormRescale,
                Identity
              ][coefficients]

            }
        ]
      }, ctfun]
    ];



  End[];



EndPackage[];