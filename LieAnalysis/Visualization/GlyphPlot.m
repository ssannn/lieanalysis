(*************************************************************************************************************************
** GlyphPlot.m
** Glyphplot to visualize SE(3) space using glyphs
**
** Author: M.H.J.Janssen <t.j.c.dela.haije@tue.nl>
** Author: F.C. Martin <f.c.martin@tue.nl>
**************************************************************************************************************************)
BeginPackage["LieAnalysis`Visualization`GlyphPlot`"];



  	Begin["`Private`"];



    	Needs["Classes`"];
    	Needs["LieAnalysis`Common`"];
    	Needs["LieAnalysis`Objects`Obj3DPositionOrientationData`"];
    	Needs["LieAnalysis`Visualization`CurveFunctions`"];
	    Needs["LieAnalysis`Visualization`ColorFunctions`"];
	    Needs["LieAnalysis`Visualization`GlyphFunctions`"];



		(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
	    (* GlyphPlot - - - - - - - - - - - - - - - - - - - - - - - - *)
	    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
	    
	    
	    
		Options[GlyphPlot] = Sort[{
			ColorFunction -> Automatic,
			ColorFunctionScaling -> Automatic,
			Downsampling -> 1,
			GlyphFunction -> Automatic,
			GlyphScaling -> 0.4,
			OrientationConvention -> "Neurological",
			PlotStyle -> Automatic,
			RegionFunction -> Automatic,
			SpatialCoordinateSystem -> "Virtual",
			SpatialTransformation -> Automatic,
			SpatialUnit -> 1,
			PerformanceGoal :> $PerformanceGoal
		}~Join~Options[Graphics3D]];
  
	    
	    
	    (* OS3d object *)
	    GlyphPlot[osObj_Obj3DPositionOrientationData /; ValidQ[osObj, Obj3DPositionOrientationData], opts:OptionsPattern[]] := Block[
	    	
	    	(* Local Variable Declaration *)
	    	{
	    		orientationList = osObj["FullOrientationList"],
	    		data = Re[osObj["FullData"]],
	    		glyph = GraphicsComplex[osObj["FullOrientationList"][[All,{3,2,1}]],Polygon[osObj["FullTopology"]]],
	    		scale = 0.8/Max[osObj["FullData","Real"]],
	    		options
	    	},
	    	
	    	(* Contruct options list *)
	    	options = DeleteDuplicatesBy[FilterRules[{opts}, Options[GlyphPlot]]~Join~{GlyphFunction->glyph, GlyphScaling->scale},Keys];
	    	
	    	(* Call main function *)
	    	Return[GlyphPlot[data,options]];
	    	
	    ];
	    
	    
	    
	    (* SE(3) no object, specifying orientations *)
	    (*ToDo: finish this *)
	    GlyphPlot[data_List /; ArrayQ[data,4] && Depth[data] == 5, tesselation_String ] := Block[
	    	
	    	(* Local Variable Declaration *)
	    	{
	    		orientationList
	    	},
	    
	    	orientationList = Check[N@PolyhedronOperations`Geodesate[PolyhedronData["Icosahedron"], 1][[1,1]],$Failed];
	    	
	    	(**)
	    	If[orientationList === $Failed, 
	    		Message[General::argfail, "GlyphPlot", "Tesselation","be a valid tessellation name"];
	    		Abort[];
	    	];
	    	
	    	Return[GlyphPlot[data, orientationList]];
	    	
	    ];
	    
	    
	    
	    (* SE(3) no object, specifying orientations *)
		GlyphPlot[data_List /; ArrayQ[data,4] && Depth[data] == 5, orientationList_ /; MatrixQ[orientationList] && List@Dimensions@orientationList === 3] := Block[
			
			(* Local variable declaration *)
			{
				
			},
			
			Return[GlyphPlot[orientationList, data]];
			
		];
		
		
		
		(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
    	(* GlyphPlot - - - - - - - - - - - - - - - - - - - - - - - - *)
    	(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)


	(* In case of a matrix of coefficients NOT SE3 data! *)
    expr : GlyphPlot[input_List /; ArrayQ[input, 3 | 4, Internal`RealValuedNumberQ], opts : OptionsPattern[]] :=
        GlyphPlot[Flatten[Array[List, Dimensions[input, 3]], 2], Flatten[input, 2], "SpatialCoordinateSystem" -> "Virtual", opts];



    expr : GlyphPlot[inputPositions_List /; MatrixQ[inputPositions, Internal`RealValuedNumberQ] && Last[Dimensions[inputPositions]] === 3, inputCoefficients_List /; ArrayQ[inputCoefficients, 1 | 2, Internal`RealValuedNumberQ], opts : OptionsPattern[]] /; Length[inputPositions] === Length[inputCoefficients] := Module[
      
      	{
			(*Options*)
	        colorFunction = OptionValue[ColorFunction],
	        colorFunctionScaling = OptionValue[ColorFunctionScaling],
	        convention = OptionValue[OrientationConvention],
	        coords = OptionValue[SpatialCoordinateSystem],
	        glyphFunction = OptionValue[GlyphFunction],
	        graphicsOpts = FilterRules[{opts}, Options[Graphics3D]],
	        performance = OptionValue[PerformanceGoal],
	        plotStyle = OptionValue[PlotStyle],
	        glyphScaling = OptionValue[GlyphScaling],
	        regionFunction = OptionValue[RegionFunction],
	        spatialUnit = OptionValue[SpatialUnit],
	        sub = OptionValue[Downsampling],
	        tfun = OptionValue[SpatialTransformation],

      		(*Toggle*)
        	toggle = True,

      		(*Variables*)
        	gtfun, ctfun, positions, coefficients, glyph, glyphOptions
      
      },



      (*Define glyph*)(*To GlyphFunctions*)
      {glyph, glyphOptions} = Which[

        MatrixQ[glyphFunction, Internal`RealValuedNumberQ] && Last[Dimensions[glyphFunction]] === 3,
        {"Vertex", "SphereGeometry" -> SphericalDelaunayTriangulation[glyphFunction]},

        MatchQ[glyphFunction, GraphicsComplex[_List, Polygon[_List]]],
        {"Vertex", "SphereGeometry" -> glyphFunction},

        True,
        With[{parsedOptions = ParseOptions[glyphFunction]}, If[ListQ[parsedOptions], parsedOptions, toggle = False; {"", {}}]]

      ];



      (*Check options formats*)
      If[Not[MatchQ[colorFunctionScaling, True | False | {(True | False) ...}]], colorFunctionScaling = Automatic];
      If[Not[OrientationConventionStringQ[convention]], convention = "Neurological"];
      If[Not[OptionQ[glyphOptions]], glyphOptions = {}];
      If[performance === "Speed", performance = False, performance = True];
      If[Not[NumberQ[glyphScaling] && Positive[glyphScaling]], glyphScaling = 0.4];
      If[Not[IntegerQ[sub] && Positive[sub]], sub = 1];
      If[Not[MatchQ[tfun, TransformationFunction[mat_ /; ArrayQ[mat, 2, NumberQ] && Dimensions[mat] === {4, 4}]]], tfun = TransformationFunction[IdentityMatrix[4]]];

      (*Join input and additional scaling*)
      glyphScaling *= sub Det[First[tfun]];

      (*Define transformations*)
      ctfun = OrientationConventionTransform[convention];
      gtfun = N[ToDiffusicaIndex[tfun]];

      (*Get positions and coefficients from input*)
      positions = Which[

        coords === "VirtualAligned",
        ResolveSpatialReflections[gtfun][inputPositions[[;; ;; sub]]],

        coords === "Dimensional",
        inputPositions[[;; ;; sub]],

        True,
        gtfun[inputPositions[[;; ;; sub]]]

      ];
      coefficients = If[VectorQ[inputCoefficients], List /@ inputCoefficients[[;; ;; sub]], inputCoefficients[[;; ;; sub]]];

      (*Determine exclusions*)(*BUG IN MAPTHREAD*)(*CHANGE TO BOOLEAN 01 TYPE TO PREVENT UNPACKING*)
      With[
        {
          selector = If[regionFunction === Automatic, AnyTrue[Thread[# != 0] &] /@ coefficients, (*Replace[*)MapThread[TrueQ@regionFunction[Sequence @@ #1, Sequence @@ #2, Sequence @@ #3] &, {inputPositions, positions, coefficients}]](*, {1}]*)
        },

        positions = Developer`ToPackedArray@Pick[positions, selector];
        coefficients = Developer`ToPackedArray@Pick[coefficients, selector];

      ];

      (*Create graphics*)
      Graphics3D[

        Glyph[glyph][positions, coefficients, colorFunction, colorFunctionScaling, plotStyle, glyphScaling, performance, ctfun, glyphOptions],

        If[performance,
          graphicsOpts,
          {graphicsOpts, Method -> {"ConePoints" -> 6, "CylinderPoints" -> 6, "SpherePoints" -> 6}}
        ],

        Axes -> True,
        AxesLabel -> MedicalAxesLabel[spatialUnit, convention],
        SphericalRegion -> True
      ] /; toggle

    ];
		


	End[]



EndPackage[]