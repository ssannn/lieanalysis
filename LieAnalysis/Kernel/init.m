(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
(* Bullet-proof- - - - - - - - - - - - - - - - - - - - - - - *)
(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)



With[{
		prot = Unprotect[
			LieAnalysis`LieAnalysis,
			LieAnalysis`$LieAnalysisVersion,
			LieAnalysis`EmbedLieAnalysisPackage,
			LieAnalysis`CakeWaveletStack,
			LieAnalysis`CakeWaveletStack3D,
			LieAnalysis`GaborWaveletStack,
			LieAnalysis`GlyphPlot,
			LieAnalysis`InverseOrientationScoreTransform,
			LieAnalysis`InverseOrientationScoreTransform3D,
			LieAnalysis`LeftInvariantDerivatives3D,
			LieAnalysis`MultiScaleCakeWaveletStack,
			LieAnalysis`LieAnalysisData,
			LieAnalysis`LeftInvariantDerivatives,
			LieAnalysis`OrientationScoreGaugeFrames,
			LieAnalysis`OrientationScoreGaugeFrames3D,
			LieAnalysis`OrientationScoreTensor,
			LieAnalysis`OrientationScoreTensor3D,
			LieAnalysis`OrientationScoreTransform,
			LieAnalysis`OrientationScoreTransform3D
		]
	},


	(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
	(* Classes - - - - - - - - - - - - - - - - - - - - - - - - - *)
	(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
	
	
	
	(* If the classes is called twice: mute the message *)
	Quiet[
	  Get["Classes`", Path -> FileNameJoin[Append[FileNameSplit[FindFile["LieAnalysis`"]][[1 ;; -3]], "classes"]]],
	  {Notation`Symbolize::bsymbexs,Protect::locked}
	];
	
	Needs[ "LieAnalysis`Objects`Object`"];
	Needs[ "LieAnalysis`Objects`ObjWavelet`"];
	Needs[ "LieAnalysis`Objects`ObjOrientationWavelet`"];
	Needs[ "LieAnalysis`Objects`ObjCakeWavelet`"];
	Needs[ "LieAnalysis`Objects`ObjScaleCakeWavelet`"];
	Needs[ "LieAnalysis`Objects`ObjGaborWavelet`"];
	Needs[ "LieAnalysis`Objects`ObjWavelet3D`"];
	Needs[ "LieAnalysis`Objects`ObjOrientationWavelet3D`"];
	Needs[ "LieAnalysis`Objects`ObjCakeWavelet3D`"];
	Needs[ "LieAnalysis`Objects`ObjWaveletTransform`"];
	Needs[ "LieAnalysis`Objects`ObjPositionOrientationData`"];
	Needs[ "LieAnalysis`Objects`ObjScaleOrientationScores2D`"];
	Needs[ "LieAnalysis`Objects`Obj3DPositionOrientationData`"];
	Needs[ "LieAnalysis`Objects`ObjTensor`"];
	
	

	(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
	(* LieAnalysis - - - - - - - - - - - - - - - - - - - - - - - *)
	(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
	
	
	
	Get[ "LieAnalysis`LieAnalysis`"];
	Get[ "LieAnalysis`Ultilities`"];
	Get[ "LieAnalysis`Visualization`"];
	
	
	Get[ "LieAnalysis`Visualization`"];
	Get[ "LieAnalysis`Visualization`GlyphPlot`"];
	Get[ "LieAnalysis`ExampleData`LieAnalysisData`"];
	
	
	
	(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
	(* OrientationScores2D - - - - - - - - - - - - - - - - - - - *)
	(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
	
	
	
	Get[ "LieAnalysis`OrientationScores2D`Filters`CakeWavelets`"];
	Get[ "LieAnalysis`OrientationScores2D`Filters`MultiScaleCakeWavelets`"];
	Get[ "LieAnalysis`OrientationScores2D`Filters`GaborWavelets`"];
	Get[ "LieAnalysis`OrientationScores2D`Filters`KalitzinWavelets`"];
	Get[ "LieAnalysis`OrientationScores2D`Construction`"];
	Get[ "LieAnalysis`OrientationScores2D`Reconstruction`"];
	Get[ "LieAnalysis`OrientationScores2D`OrientationScoreDerivatives`"];
	Get[ "LieAnalysis`OrientationScores2D`Derivatives`LeftInvariantDerivatives`"];
	Get[ "LieAnalysis`OrientationScores2D`OrientationScoreTensor`"];
	Get[ "LieAnalysis`OrientationScores2D`GaugeFrames`ExponentialCurveFitting`"];
	
	
	
	(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
	(* OrientationScores3D - - - - - - - - - - - - - - - - - - - *)
	(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
	
	
	
	Get[ "LieAnalysis`OrientationScores3D`Filters`CakeWavelets`"];
	Get[ "LieAnalysis`OrientationScores3D`Construction`"];
	Get[ "LieAnalysis`OrientationScores3D`Reconstruction`"];
	Get[ "LieAnalysis`OrientationScores3D`OrientationScoreDerivatives`"];
	Get[ "LieAnalysis`OrientationScores3D`Derivatives`LeftInvariantDerivatives`"];
	Get[ "LieAnalysis`OrientationScores3D`OrientationScoreTensor`"];
	Get[ "LieAnalysis`OrientationScores3D`GaugeFrames`ExponentialCurveFit`"];
	
	(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
	
	
	
	(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
	(* Bullet-proof- - - - - - - - - - - - - - - - - - - - - - - *)
	(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
	
	Protect@prot;
	
];

(* For CUDA Implementations the Enviroment var Path has to be added *)
If[$OperatingSystem === "Windows",
	SetEnvironment["Path"->
		Environment["Path"]<>FileNameJoin[Append[FileNameSplit[FindFile["LieAnalysis`"]][[1 ;; -3]], "CUDALibraries"]]
	];
];

(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
(* Banner- - - - - - - - - - - - - - - - - - - - - - - - - - *)
(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
(*CellPrint[
		Cell[TextData[{
	 StyleBox["Lie Analysis Package, Release: "~StringJoin~ToString[$LieAnalysisVersion],
	  FontSize->16,
	  FontWeight->"Bold",
	  FontColor->RGBColor[
	   0.24313725490196078`, 0.24313725490196078`, 0.24313725490196078`]],
	 "\n\nThank you for using or LieAnalysis Package. Please make sure to check our website: ",
	 ButtonBox["www.lieanalysis.nl",
	  BaseStyle->"Hyperlink",
	  ButtonData->{
	    URL["http://lieanalysis.nl"], None},
	  ButtonNote->"http://lieanalysis.nl"],
	 ". If you have any difficulties or questions please contact Frank Martin ",
	 ButtonBox["f.c.martin@tue.nl",
	  BaseStyle->"Hyperlink",
	  ButtonData->{
	    URL["mailto:f.c.martin@tue.nl"], None},
	  ButtonNote->"mailto:f.c.martin@tue.nl"],
	 " or ",
	 ButtonBox["e.j.bekkers@tue.nl",
	  BaseStyle->"Hyperlink",
	  ButtonData->{
	    URL["mailto:e.j.bekkers@tue.nl"], None},
	  ButtonNote->"mailto:e.j.bekkers@tue.nl"],
	 ". \n\nThis software is released under the FreeBSD License which is enclosed.\n",
	 StyleBox["Copyright (c) 2016, Regents of the University of Eindhoven All rights reserved.",
	  FontWeight->"Bold"]
	}], "Text",
	 CellFrame->1,
	 CellFrameColor->RGBColor[0, 0.67, 0],
	 CellChangeTimes->{{3.7040867496478977`*^9, 3.704086845214794*^9}, {
	  3.7040868981268215`*^9, 3.704086908086793*^9}, {
	  3.704086949213582*^9, 3.704087041657305*^9}},
	 FontColor->RGBColor[0, 0.67, 0],
	 Background->RGBColor[0.88, 1, 0.88]]
	 
 ]*)
