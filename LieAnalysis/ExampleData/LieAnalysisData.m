(*************************************************************************************************************************
** LieAnalysisData.m
** This .m file contains functions to load example data to demonstrate the functions
**
** Author: F.C. Martin <f.c.martin@tue.nl>
**************************************************************************************************************************)
BeginPackage["LieAnalysis`ExampleData`LieAnalysisData`"];



	Begin["`Private`"];
	
	Get["LieAnalysis`Common`"]
	
	(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
    (* - - - - - - - - - - - - - - - - - - - - - - - *)
    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
    
    
    LieAnalysisData[___] := Keys[FetchExampleKeys[]];		
    
    
    
    LieAnalysisData[type_] := Module[
    	
    	(* Local Variables *)
    	{
    		keys,
    		typeU = ToLowerCase@type
    	},
    	
    	(* Fetch Keys *)
    	keys = FetchExampleKeys[];
    		
    	(* Check that type does exist *)
    	If[KeyExistsQ[keys,typeU] === True,
    		Return[keys[typeU]]
    		,
    		Message[General::argfail, "type", "the key does not exists"];
    		Return[$Failed]
    	];
    	
    ];
	
	
	
	LieAnalysisData[{type_String, name_String}] := Module[
		
		(* Local Variables *)
		{
			baseUrl = LieAnalysis["ExampleDataURL"],
			typeU = ToLowerCase@type,
			nameU = ToLowerCase@name,
			ext = ".mx",
			exampleDataFolder = FileNameJoin[Join[(FileNameSplit@FindFile["LieAnalysis`"])[[1;;-3]],{"ExampleData"}]],
			filename, file, keys
		},
			
			(* fetch keys *)
			keys = FetchExampleKeys[];
			
			filename = typeU<>"-"<>nameU<>ext;
			
			If[KeyExistsQ[keys,typeU],
				If[MemberQ[keys[typeU],nameU],
					
					If[FileExistsQ[FileNameJoin[{exampleDataFolder, filename}]],
						
						(* Use Local File *)	
						file = Import[FileNameJoin[{exampleDataFolder,filename}]]
						
						,
						
						(* Download *)
						file = Check[
							Import@DownloadFile[
								URLBuild[{baseUrl,filename}], (* Download location *)
								exampleDataFolder (* Local Example data folder *)
							]
							,
								Message[General::netfail, URLBuild[{baseUrl,filename}], "can't be reached"]; $Failed
						];
					
					],
					Message[General::argfail, "name", "name does not exists in the key"];
					file = $Failed;
				],
				Message[General::argfail, "type", "type does not excists"];
				file = $Failed;
			];
			
			Return[file];
			
	];
	
	
	
	(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
    (* FetchExampleKeys- - - - - - - - - - - - - - - - - - - - - *)
    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
    
    
    FetchExampleKeys[] := Module[
    	
    	{
    		localFile = FileNameJoin[Join[(FileNameSplit@FindFile["LieAnalysis`"])[[1;;-3]],{"ExampleData","example-data-keys.txt"}]],
    		remoteFile = URLBuild[{LieAnalysis["ExampleDataURL"],"example-data-keys.txt"}],
    		keysLocal, keysRemote
    	},
    	
    	(* Fetch Remote Keys *)
    	keysRemote = Quiet@Check[ToExpression@Import[remoteFile],$Failed];
    	
    	(* Store the keys local *)
    	If[(AssociationQ@keysRemote), 
    		Export[localFile, keysRemote];
    	];
    	
    	(* Import Local keys *)
    	keysLocal = Quiet@Check[ToExpression@Import[localFile], $Failed];
    	
    	(* Check that local keys could be fetched *)
    	If[keysRemote === $Failed && keysLocal === $Failed, 
    		Message[General::warning, "Keys can not be found local or Remote..."];
    		Return[$Failed]
    	];
    	
    	(* return the association *)
    	Return[keysLocal];
	
    ];
    
    
	
	End[];
	

EndPackage[];
