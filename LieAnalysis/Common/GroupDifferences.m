(*************************************************************************************************************************
** GroupDifferences.m
** This .m file contains functions to compute finite differences in SE2
**
** Author: T.C.J. Dela Haije <E.J.Bekkers@tue.nl>
** Author: F.C. Martin <f.c.martin@tue.nl>
**************************************************************************************************************************)
BeginPackage["LieAnalysis`Common`GroupDifferences`"];



	Begin["`Private`"];



		(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
	    (* InterpolationKernel - - - - - - - - - - - - - - - - - - - *)
	    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)



		Options[InterpolationKernel] = Options[ListInterpolation];
		
		
		
		InterpolationKernel[vector_,OptionsPattern[]] := Module[
			
			(* Local variable declaration *)
			{
				emptyKernel, kernelFinal, kernelOneElement, kernelOneElementInt, center
			},
			
			center=OptionValue[InterpolationOrder]+1;
			emptyKernel=ConstantArray[0,{2*center-1,2*center-1}];
			kernelFinal=emptyKernel;
			
			Do[kernelOneElement=emptyKernel;
				kernelOneElement[[i,j]]=1;
				kernelOneElementInt=ListInterpolation[kernelOneElement,Method->OptionValue[Method],InterpolationOrder->OptionValue[InterpolationOrder]];
				kernelFinal[[i,j]]=kernelOneElementInt[center+vector[[1]],center+vector[[2]]];
			,{i,1,2*center-1},{j,1,2*center-1}];
			
			(*This result is a correlation kernel,we want a convolution kernel so we mirror the result:*)
			kernelFinal=Reverse[kernelFinal,{1,2}];
			kernelFinal=Developer`ToPackedArray[kernelFinal,Real];
			
			Return[kernelFinal]
		];



		SplineInterpolationKernel[vector_] := InterpolationKernel[vector,Method->"Spline",InterpolationOrder->3];



		(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
	    (* A Kernels - - - - - - - - - - - - - - - - - - - - - - - - *)
	    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)



		(* Spatial Kernels Kernel *)
		SpatialKernels[theta_, direction_String /; MatchQ[direction,"A1"|"A2"], type_String, order:1] := 
			SpatialKernels[theta, direction, type, order] = Module[
			
			(* Local Variable Declaration *)
			{
				kernel,eA
			},
			
			(* get A-frame *)
			eA = Switch[direction,
				"A1", {Cos[theta],Sin[theta]},
				"A2", {-Sin[theta],Cos[theta]}
			];
			
			(* Construct kernel *)
			kernel = Subtract@@Switch[ToLowerCase[type],
				"forward", 
					SplineInterpolationKernel/@{eA,{0,0}},
				"backward",
					SplineInterpolationKernel/@{{0,0},-eA},
				_ | "central",
					.5 SplineInterpolationKernel/@{eA,-eA}
			];
			
			Return[kernel]
			
		];
		
		
		
		SpatialKernels[theta_, direction_String /; MatchQ[direction,"A1A1"|"A2A2"], type_String, order:2] := 
			SpatialKernels[theta, direction, type, order] = Module[
			
			(* Local Variable Declaration *)
			{
				kernel,eA
			},
			
			(* get A-frame *)
			eA = Switch[direction,
				"A1A1", {Cos[theta],Sin[theta]},
				"A2A2", {-Sin[theta],Cos[theta]}
			]; 
			
			(* Construct kernel *)
			kernel = (#1 - 2 #2 + #3)&@@Switch[ToLowerCase[type],
				"forward",
					SplineInterpolationKernel/@{2*eA,eA,{0,0}},
				"backward",
					SplineInterpolationKernel/@{{0,0},-eA,-2 eA},
				"central" | _,
					SplineInterpolationKernel/@{eA,{0,0},-eA}
			];
			
			Return[kernel];
			
		];



		(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
	    (* Spatial Derivatives - - - - - - - - - - - - - - - - - - - *)
	    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)



		LeftInvariantDerivative[os_, periodicity_: Pi, method_String , direction_ /; MatchQ[direction, "A1"|"A2"|"A1A1"|"A2A2"], order_] := Module[
			
			(* Local Variables *)
			{
				angularResolution, kernel, derivative, 
				orientations = Length[os]
			},
			
			(* Compute Angular Resolution *)
			angularResolution = periodicity/orientations;
			
			(* Apply Spatial Interpolation Kernels *)
			derivative = Map[	
				(
					kernel = SpatialKernels[angularResolution*(#-1),direction, method, order];
					ListConvolve[kernel,os[[#]],Ceiling[Dimensions[kernel]/2]]
				)&,
				Range[1,orientations,1]
			];
			
			(* Return the derivative *)
			Return[derivative];
			
		];



		(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
	    (* Angular Derivative- - - - - - - - - - - - - - - - - - - - *)
	    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
	    
	    
		(*TODO: Check what to do with -Pi symmetry*)			    
		LeftInvariantDerivative[os_,periodicity_: Pi,method_String, direction:"A3", order:1]:=Module[
			
			{
				angularResolution = periodicity/Length[os], 
				derivative
			},
			
			(* Compute derivatives *)
			derivative = (1/angularResolution) Subtract@@
				Switch[ToLowerCase[method],
					"forward",
						 RotateLeft[os,#]&/@{1,0}
					,"backward",
						RotateLeft[os,#]&/@{0,-1},
					"central" | _,
						0.5 RotateLeft[os,#]&/@{1,-1}
				
			];
			
			(* Return Derivative *)
			Return[derivative];
			
		];
		


		LeftInvariantDerivative[os_,periodicity_:Pi, method_String, direction:"A3A3", order:2]:=Module[
			{
				angularResolution = periodicity/Length[os], 
				derivative
			},
			
			(* Compute derivatives *)
			derivative = (1/angularResolution^2)(#1 - 2#2 + #3)&@@Switch[ToLowerCase[method],
				"forward",
					RotateLeft[os,#]&/@{0,1,2},
				"backward",
					RotateLeft[os,#]&/@{-2,-1,0},
				"central" | _ ,
					RotateLeft[os,#]&/@{1,0,-1}
			];
			
			(* Return Derivative *)
			Return[derivative]
			
		];



		(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
	    (* GroupDifferences- - - - - - - - - - - - - - - - - - - - - *)
	    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)



		Options[GroupDifferences] = {
			"Periodicity" -> Pi,
			"Method" -> "Central"
		};
		
		
		
		GroupDifferences[os_,orderIn_String,OptionsPattern[]]:=Module[
			
			{
				order = orderIn,
				orientationFirst = Transpose[os,{2,3,1}],
				periodicity = OptionValue["Periodicity"], 
				method = OptionValue["Method"],
				derivative
			},
			
			(* Check input *)
			If[!ContainsOnly[StringPartition[order, 2], {"A1", "A2", "A3"}],
				Message[GroupDifferences::invalidOrders, "GroupDifferences", "Should be a string consisting of A1, A2 and A3"];
				Abort[];
			];
		
			(* Compute derivative *)
			derivative = Transpose[
				Switch[order,
				
					"A1"|"A2"|"A3",
						LeftInvariantDerivative[orientationFirst,periodicity,method,order,1],
					"A1A1" | "A2A2" | "A3A3",
						LeftInvariantDerivative[orientationFirst,periodicity,method,order,2],
					_String,
						Do[
							orientationFirst = LeftInvariantDerivative[orientationFirst,periodicity,method,direction,1]
							,{direction, StringPartition[order,2]}
						];
						orientationFirst
						
				],{3,1,2}
			]
			
		];



	End[];



EndPackage[];