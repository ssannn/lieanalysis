(*************************************************************************************************************************
** CoordinateSystemConversions.m
** This .m file contains functions to go from one coordinate system to another
**
** Author: F.C. Martin <f.c.martin@tue.nl>
** Author:
** Author:
**************************************************************************************************************************)
BeginPackage["LieAnalysis`Common`CoordinateSystemConversions`"];



	Begin["`Private`"];
	
	
	
	    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
	    (* CartesianToSphericalCoordinates - - - - - - - - - - - - - *)
	    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
	
	
	
	   	CartesianToSphericalCoordinates[qvecs_] := CartesianToSphericalCoordinatesC[qvecs];
	    
	    
	    
	    CartesianToSphericalCoordinatesC = Compile[{{vec,_Real,1}}, Module[
	
			(* Local Variables *)
			{
		  		R = Sqrt[Abs[vec[[1]]]^2+Abs[vec[[2]]]^2+Abs[vec[[3]]]^2], 
		  		qx,qy,qz,theta=0.,phi=0.,nVec
		  	},
			
		    (* Check if the vector has a length greater than zero *)
		    If[Chop[R]==0,
		    	Return[{0.,0.,0.}]
			];
		    
		    (* Normalize vector *)
		    nVec = {qx,qy,qz} = vec/R;
			
			(* Compute Spherical coordinate *)
		    If[qz < 1-10^-8,
		      If[qz > -1+10^-8,
		        {theta,phi} = {ArcCos[qz],ArcTan[qx,qy]};,
		        {theta,phi} = {Pi,0};
		      ];,
		      {theta,phi} = {0,0};
		    ];
		    
		    (*Return Spherical Coordinates *)
			Return[{R,theta,phi}]
			
	      ],RuntimeAttributes->{Listable}
	
	    ];
	
	
	
	    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
	    (* CartesianToSphericalCoordinates - - - - - - - - - - - - - *)
	    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
	
	
	
	    RToAlphaBetLaGamma = Compile[{{R,_Real,2}},
	      Module[{\[Gamma],\[Beta],\[Alpha],cb},
	        cb=Chop[R[[3,3]]];
	        If[cb<1-10^-8,
	          If[cb>-1+10^-8,
	            \[Beta]=ArcCos[cb];
	            \[Gamma]=ArcTan[R[[1,3]],R[[2,3]]];
	            \[Alpha]=ArcTan[-R[[3,1]],R[[3,2]]];,
	            \[Gamma]=0;\[Beta]=\[Pi];\[Alpha]=ArcTan[R[[2,2]],R[[2,1]]];
	          ],
	          \[Gamma]=\[Beta] =0;\[Alpha]=ArcTan[R[[1,1]],R[[2,1]]];
	        ];
	        {\[Gamma],\[Beta],\[Alpha]}
	      ],RuntimeAttributes->{Listable}
	    ];
	    
	    
	    
	    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
	    (* FromLeftInvariantFrame- - - - - - - - - - - - - - - - - - *)
	    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
	    
	    LeftInvariantFrame[\[Theta]_]:={
			{Cos[\[Theta]],Sin[\[Theta]],0},
			{-Sin[\[Theta]],Cos[\[Theta]],0},
			{0,0,1}
		}
	    
	    (* First Oder 2D OS *)
		FromLeftInvariantFrame[orientationList_ /; VectorQ[orientationList], gradient_ /; MatchQ[Dimensions[gradient],{_,_,_,3}]] := Module[
		
			{
				leftInvariantFrame = LeftInvariantFrame/@orientationList,
				cartesian
			},
		
			(* Second order *)
			cartesian = Compile[{{R,_Real,3},{gradientC,_Real,2}},
				MapThread[Function[{rot,ten},Transpose[rot].ten],{R,gradientC}],
				RuntimeAttributes->{Listable}
			][leftInvariantFrame,gradient]
		
		];
		
		
		
		(* Second order 2D OS *)
		FromLeftInvariantFrame[orientationList_ /; VectorQ[orientationList], tensor_ /; MatchQ[Dimensions[tensor],{_,_,_,3,3}]] := Module[
			{
				leftInvariantFrame = LeftInvariantFrame/@orientationList,
				cartesian
			},
			
			(* Second order *)
			cartesian = Compile[{{R,_Real,3},{tensorC,_Real,3}},
				MapThread[Function[{rot,ten},ten.rot],{R,tensorC}],
				RuntimeAttributes->{Listable}
			][leftInvariantFrame,tensor];
			
			Return[cartesian]
		]
		
		
		
		(* Second order 3D OS *)
	    FromLeftInvariantFrame[orientationList_ , tensor_ /; MatchQ[Dimensions[tensor], {_, _, _, _, 6, 6}]] := Module[
   			{
				rot = LieAnalysis`Common`Rotations`Private`RnWithAlphaIs0[orientationList],
				cartesian
			},
   
			cartesian = Compile[{{tensorIn, _Real, 3}, {RIn, _Real, 3}},
      
      		MapThread[Function[{R, tensorC},
        
	        	(#.tensorC.Transpose[#] &)@{
		          {R[[1, 1]], R[[1, 2]], R[[1, 3]], 0., 0., 0.},
		          {R[[2, 1]], R[[2, 2]], R[[2, 3]], 0., 0., 0.},
		          {R[[3, 1]], R[[3, 2]], R[[3, 3]], 0., 0., 0.},
		          {0., 0., 0., R[[1, 1]], R[[1, 2]], R[[1, 3]]},
		          {0., 0., 0., R[[2, 1]], R[[2, 2]], R[[2, 3]]},
		          {0., 0., 0., R[[3, 1]], R[[3, 2]], R[[3, 3]]}
	          	}
        		]
       		, {RIn, tensorIn}
       		]
   
      		, RuntimeAttributes -> {Listable}
      
      		][tensor, rot]
   
   		];
	    
	    
	    
	    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
	    (* ToLeftInvariantFrame- - - - - - - - - - - - - - - - - - - *)
		(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
	    
	    
	    
	    (* First Oder 2D OS *)
		ToLeftInvariantFrame[orientationList_ /; VectorQ[orientationList], gradient_ /; MatchQ[Dimensions[gradient],{_,_,_,3}]] := Module[
		
			{
				leftInvariantFrame = LeftInvariantFrame/@orientationList,
				cartesian
			},
		
			(* Second order *)
			cartesian = Compile[{{R,_Real,3},{gradientC,_Real,2}},
				MapThread[Function[{rot,ten},rot.ten],{R,gradientC}],
				RuntimeAttributes->{Listable}
			][leftInvariantFrame,gradient]
		
		];
		
		
		
		(* Second Order 2D OS *)
		ToLeftInvariantFrame[orientationList_ /; VectorQ[orientationList], tensor_ /; MatchQ[Dimensions[tensor],{_,_,_,3,3}]] := Module[
		
			{
				leftInvariantFrame = LeftInvariantFrame/@orientationList,
				cartesian	
			},
			
			cartesian = Compile[{{R,_Real,3},{tensorC,_Real,3}},
				MapThread[Function[{rot,ten},rot.ten.Transpose[rot]],{R,tensorC}],
				RuntimeAttributes->{Listable}
			][leftInvariantFrame,tensor];
			
			Return[cartesian];
			
		];
		
		
		
		(* Second order 3D OS *)
		 ToLeftInvariantFrame[orientationList_ , tensor_ /; MatchQ[Dimensions[tensor], {_, _, _, _, 6, 6}]] := Module[
   			{
				rot = LieAnalysis`Common`Rotations`Private`RnWithAlphaIs0[orientationList],
				cartesian
			},
   
			cartesian = Compile[{{tensorIn, _Real, 3}, {RIn, _Real, 3}},
      
      		MapThread[Function[{R, tensorC},
        
	        	(Transpose[#].tensorC.# &)@{
		          {R[[1, 1]], R[[1, 2]], R[[1, 3]], 0., 0., 0.},
		          {R[[2, 1]], R[[2, 2]], R[[2, 3]], 0., 0., 0.},
		          {R[[3, 1]], R[[3, 2]], R[[3, 3]], 0., 0., 0.},
		          {0., 0., 0., R[[1, 1]], R[[1, 2]], R[[1, 3]]},
		          {0., 0., 0., R[[2, 1]], R[[2, 2]], R[[2, 3]]},
		          {0., 0., 0., R[[3, 1]], R[[3, 2]], R[[3, 3]]}
	          	}
        		]
       		, {RIn, tensorIn}
       		]
   
      		, RuntimeAttributes -> {Listable}
      
      		][tensor, rot]
   
   		];
	    
	    
    
    End[];


EndPackage[];