(*************************************************************************************************************************
** Common.m
** This .m file contains functions to compute the orientation derivatives in se(2)
**
** Author: T.C.J. Dela Haije <T.C.J.Dela.Haije@tue.nl>
** Author: F.C. Martin <f.c.martin@tue.nl>
**************************************************************************************************************************)
BeginPackage["LieAnalysis`Common`SphericalHarmonics`"];



  (* Dependancies *)
  Begin["`Private`"];



    Needs["LieAnalysis`Common`CoordinateSystemConversions`"];
    Needs["LieAnalysis`Common`GaussianDerivative`"];



    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
    (* RealSphericalHarmonicParameterList- - - - - - - - - - - - *)
    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)



    RealSphericalHarmonicParameterList[order_Integer: 8] /; NonNegative[order] :=
        RealSphericalHarmonicParameterList[order] = Developer`ToPackedArray@Flatten[Table[{l, m}, {l, 0, order, 1}, {m, -l, l}], 1];



    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
    (* RealSphericalHarmonicOrderList- - - - - - - - - - - - - - *)
    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)



    RealSphericalHarmonicOrderList[order_Integer: 8] /; NonNegative[order] :=
        RealSphericalHarmonicOrderList[order] = Developer`ToPackedArray@Flatten[Table[l, {l, 0, order, 1}, {m, -l, l}], 1];



    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
    (* RealSphericalHarmonicOrderList- - - - - - - - - - - - - - *)
    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)



    Options[RealSphericalHarmonicValues] = {"Basis" -> RealSphericalHarmonicY};



    RealSphericalHarmonicValues[angles : {{_, _} ..}, order_Integer /; NonNegative[order], opts : OptionsPattern[]] :=
        RealSphericalHarmonicValues[angles, order, opts] = Module[
          {
            values = OptionValue["Basis"][##, Sequence @@ Transpose[angles]] & @@@ RealSphericalHarmonicParameterList[order]
          },

          Transpose[values, RotateRight@Range[ArrayDepth[values]]]
        ];

    RealSphericalHarmonicValues[vectors : {{_, _, _} ..}, order_Integer /; NonNegative[order], opts : OptionsPattern[]] := RealSphericalHarmonicValues[LieAnalysis`Common`CartesianToSphericalCoordinates[vectors][[All, 2 ;; 3]], order, opts];



    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
    (* RealSphericalHarmonicFit- - - - - - - - - - - - - - - - - *)
    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)



    Options[RealSphericalHarmonicFit] = {"RegularizationParameter" -> 0.};



    RealSphericalHarmonicFit[pts_ ?MatrixQ, data_List, order_Integer: 8, OptionsPattern[]] /; (Length[pts] == Last[Dimensions[data]]) := With[

      {
        B = RealSphericalHarmonicValues[pts, order],
        L = With[{lList = RealSphericalHarmonicOrderList[order]}, DiagonalMatrix@(lList^2 (lList + 1)^2)],
        lambda = OptionValue["RegularizationParameter"]
      },

      With[{mat = PseudoInverse[Transpose[B].B + lambda L].Transpose[B]}, Map[Dot[mat, #] &, data, {-2}]]

    ];



    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
    (* RealSphericalHarmonicY- - - - - - - - - - - - - - - - - - *)
    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)



    RealSphericalHarmonicY[l_, m_, theta_, phi_] := Module[

      {
        yPlus0Plus0 = SphericalHarmonicY[l, m, theta, phi]
      },

      Piecewise[
        {
          {Sqrt[2] Re[yPlus0Plus0], 	m < 0},
          {Re[yPlus0Plus0],			m == 0},
          {Sqrt[2] Im[yPlus0Plus0],	m > 0}
        }
      ]

    ];



    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
    (* A4RealSphericalHarmonicY - - - - - - - - - - - - - *)
    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)



    A4RealSphericalHarmonicY[l_, m_, theta_, phi_] := Module[

      {
        yMinus1Minus1 = E^(I phi) SphericalHarmonicY[l - 1, m - 1, theta, phi],
        yMinus1Plus1 = E^(-I phi) SphericalHarmonicY[l - 1, m + 1, theta, phi]
      },

      Piecewise[
        {
          {-1/2 Sqrt[2] Sqrt[(2 l + 1)/(2 l - 1)] (Sqrt[(l + m) (l + m - 1)] Im[yMinus1Minus1] + Sqrt[(l - m) (l - m - 1)] Im[yMinus1Plus1]), 	m < 0},
          {0, 																											m == 0},
          {1/2 Sqrt[2] Sqrt[(2 l + 1)/(2 l - 1)] (Sqrt[(l + m) (l + m - 1)] Re[yMinus1Minus1] + Sqrt[(l - m) (l - m - 1)] Re[yMinus1Plus1]), m > 0}
        }
      ]

    ];



    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
    (* A5RealSphericalHarmonicY - - - - - - - - - - - - - - - *)
    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)



    A5RealSphericalHarmonicY[l_, m_, theta_, phi_] := Module[

      {
        yPlus0Minus1 = E^(I phi) SphericalHarmonicY[l, m - 1, theta, phi],
        yPlus0Plus1 = E^(-I phi) SphericalHarmonicY[l, m + 1, theta, phi]
      },

      Piecewise[
        {
          {1/2 Sqrt[2] (Sqrt[(l - m) (l + m + 1)] Re[yPlus0Plus1] - Sqrt[(1 + l - m) (l + m)] Re[yPlus0Minus1]), 	m < 0},
          {1/2 Sqrt[l (l + 1)] (Re[yPlus0Plus1] - Re[yPlus0Minus1]), 												m == 0},
          {1/2 Sqrt[2] (Sqrt[(l - m) (l + m + 1)] Im[yPlus0Plus1] - Sqrt[(1 + l - m) (l + m)] Im[yPlus0Minus1]), 	m > 0}
        }
      ]

    ];



    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
    (* A5A5RealSphericalHarmonicY- - - - - - - - - - - - - *)
    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)



    A5A5RealSphericalHarmonicY[l_, m_, theta_, phi_] := Module[

      	{
	        yPlus0Minus2 = E^(2 I phi) SphericalHarmonicY[l, m - 2, theta, phi],
	        yPlus0Plus0 = SphericalHarmonicY[l, m, theta, phi],
	        yPlus0Plus2 = E^(-2 I phi) SphericalHarmonicY[l, m + 2, theta, phi]
      	},

		Piecewise[
			{
  				{1/4 Sqrt[2] (Sqrt[(l - m + 2) (l + m - 1) (l - m + 1) (l + m)] Re[yPlus0Minus2] - 2 (l^2 + l - m^2) Re[yPlus0Plus0] + Sqrt[(l + m + 2) (l - m - 1) (l + m + 1) (l - m)] Re[yPlus0Plus2]), 	m < 0},
  				{1/4 (Sqrt[(-1 + l) l (1 + l) (2 + l)] Re[yPlus0Minus2 + yPlus0Plus2] - 2 l (1 + l) Re[yPlus0Plus0]), 																						m == 0},
  				{1/4 Sqrt[2] (Sqrt[(l - m + 2) (l + m - 1) (l - m + 1) (l + m)] Im[yPlus0Minus2] - 2 (l^2 + l - m^2) Im[yPlus0Plus0] + Sqrt[(l + m + 2) (l - m - 1) (l + m + 1) (l - m)] Im[yPlus0Plus2]), 	m > 0}
        	}
      ]

    ];



    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
    (* A5A4RealSphericalHarmonicY- - - - - - - - - - - *)
    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)



    A4A4RealSphericalHarmonicY[l_,m_,theta_,phi_] := Module[

      {
        Sh = RealSphericalHarmonicY[l,m,theta,phi],
        ShThetaTheta = A5A5RealSphericalHarmonicY[l,m,theta,phi],
        derivative
      },

      (* Using the laplace-beltrami operator as relation *)
      derivative = -l(l+1)Sh - ShThetaTheta;

      (* Return the derivative *)
      Return[derivative];

    ];



    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
    (* A5A4RealSphericalHarmonicY- - - - - - - - - - - *)
    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)



	A5A4RealSphericalHarmonicY[l_,m_,theta_,phi_] := Module[

		{
			yMinus1Minus2 = E^(2 I phi) SphericalHarmonicY[l-1, m - 2, theta, phi],
			yMinus1Plus0 =  SphericalHarmonicY[l-1, m, theta, phi],
			yMinus1Plus2 =  E^(-2 I phi) SphericalHarmonicY[l-1, m + 2, theta, phi]
		},
		
		Piecewise[
			{
				{-1/4 Sqrt[2] Sqrt[(2l + 1)/(2l - 1)](-Sqrt[(1 + l - m)(l + m - 2)(l + m - 1)(l + m)] Im[yMinus1Minus2] + 2m Sqrt[(l + m)(l - m)]Im[yMinus1Plus0] + Sqrt[(l - m - 2)(l - m - 1)(l - m)(1 + l + m)] Im[yMinus1Plus2]),m < 0},
				{0, m == 0},
				{ 1/4 Sqrt[2] Sqrt[(2l + 1)/(2l - 1)](-Sqrt[(1 + l - m)(l + m - 2)(l + m - 1)(l + m)] Re[yMinus1Minus2] + 2m Sqrt[(l + m)(l - m)]Re[yMinus1Plus0] + Sqrt[(l - m - 2)(l - m - 1)(l - m)(1 + l + m)] Re[yMinus1Plus2]),m > 0}
			}
		]
	
	];



    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
    (* RealSphericalHarmonicYNormal- - - - - - - - - - - - - - - *)
    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)



    RealSphericalHarmonicYNormal[l_, m_, theta_, phi_] := With[
      {
        y = RealSphericalHarmonicY[l, m, theta, phi],
        yTheta = A5RealSphericalHarmonicY[l, m, theta, phi],
        yPhiScaled = A4RealSphericalHarmonicY[l, m, theta, phi]
      },

      {
        Cos[phi] Sin[theta] y + Sin[phi] yPhiScaled - Cos[theta] Cos[phi] yTheta,
        Sin[theta] Sin[phi] y - Cos[phi] yPhiScaled - Cos[theta] Sin[phi] yTheta,
        Cos[theta] y + Sin[theta] yTheta
      }

    ];
    
    
    
    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
    (* SphericalHarmonicsAngularBlur - - - - - - - - - - - - - - *)
    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
    
    
    (* todo: no matrix multiplication req *)
    SphericalHarmonicsAngularBlur[s_List, sigma_] := 
    	s.SphericalHarmonicsAngularBlur[SphericalHarmonicsIndexToL[Last@Dimensions[s]],sigma];
	
	SphericalHarmonicsAngularBlur[lMax_Integer,sigma_] := Module[
		
		{
			kt = 1/2 sigma^2,
			J = SphericalHarmonicsLMToIndex[lMax,lMax]
		},
		
		DiagonalMatrix[
			Table[
				E^(-SphericalHarmonicsIndexToL[j](SphericalHarmonicsIndexToL[j]+1)kt),
				{j,1,J}
			]
		]
	];
	


    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
    (* SphericalHarmonicsLMax- - - - - - - - - - - - - - - - - - *)
    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)



    SphericalHarmonicsLMax[n_, all_:True] := If[all,
      Floor[Sqrt[n-1]]-1
      ,
      Floor[1/2 (-3+Sqrt[1+8 n])-1]
    ];



	(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
    (* Constants - - - - - - - - - - - - - - - - - - - - - - - - *)
    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)


	


    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
    (* LinearIndexFunctions- - - - - - - - - - - - - - - - - - - *)
    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)



    SphericalHarmonicsIndexToL[j_] :=  Floor[Sqrt[j-1]];
    SphericalHarmonicsLMToIndex[l_,m_] :=Round[(l^2+l+1)+m];
    SphericalHarmonicsIndexToM[j_] := Round[j-1-(SphericalHarmonicsIndexToL[j])^2-SphericalHarmonicsIndexToL[j]];
    
    
    
    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
    (* SphericalHarmonicsTable - - - - - - - - - - - - - - - - - *)
    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)



	SphericalHarmonicsTable[0, lMax_Integer] := Compile[{beta,gamma},
        Evaluate@Table[
          RealSphericalHarmonicY[Sequence@@lm, beta, gamma ],{lm, RealSphericalHarmonicParameterList[lMax]}
        ]
      , RuntimeAttributes->Listable, Parallelization -> True
    ];
	
	

    SphericalHarmonicsTable[4, lMax_Integer] := Compile[{beta,gamma},
        Evaluate@Table[
          A4RealSphericalHarmonicY[Sequence@@lm, beta, gamma ],{lm, RealSphericalHarmonicParameterList[lMax]}
        ]
      , RuntimeAttributes->Listable, Parallelization -> True
    ];



    SphericalHarmonicsTable[5, lMax_Integer] := Compile[{beta,gamma},
        Evaluate@Table[
          A5RealSphericalHarmonicY[Sequence@@lm, beta, gamma ],{lm, RealSphericalHarmonicParameterList[lMax]}
        ]
      , RuntimeAttributes->Listable, Parallelization -> True
    ];



    SphericalHarmonicsTable[44, lMax_Integer] := Compile[{beta,gamma},
        Evaluate@Table[
          A4A4RealSphericalHarmonicY[Sequence@@lm, beta, gamma ],{lm, RealSphericalHarmonicParameterList[lMax]}
        ]
      , RuntimeAttributes->Listable, Parallelization -> True
    ];



    SphericalHarmonicsTable[54, lMax_Integer] := Compile[{beta,gamma},
        Evaluate@Table[
          A5A4RealSphericalHarmonicY[Sequence@@lm, beta, gamma ],{lm, RealSphericalHarmonicParameterList[lMax]}
        ]
      , RuntimeAttributes->Listable, Parallelization -> True
    ];
    
    
    
    SphericalHarmonicsTable[45, lMax_Integer] := SphericalHarmonicsTable[54, lMax];



    SphericalHarmonicsTable[55, lMax_Integer] := Compile[{beta,gamma},
        Evaluate@Table[
          A5A5RealSphericalHarmonicY[Sequence@@lm, beta, gamma ],{lm, RealSphericalHarmonicParameterList[lMax]}
        ]
      , RuntimeAttributes->Listable
    ];



  End[];


EndPackage[];