(*************************************************************************************************************************
** GaussianDerivative.m
** This .m file contains functions to compute gaussian derivatives in n dimensions
**
** Author: F.C. Martin <f.c.martin@tue.nl>
**************************************************************************************************************************)
BeginPackage["LieAnalysis`Common`GaussianDerivative`"];



  Begin["`Private`"];



    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
    (* GaussianDerivatives - - - - - - - - - - - - - - - - - - - *)
    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)



    GaussianDerivative::msg = "Input format not supported.";



    GaussianDerivative[data_, sigmas_, orders_, padding_:"Periodic"] := Module[

      (* Local Variables Declaration *)
      {
        singlePaddingMethod = MatchQ[padding,"Periodic" | "Fixed" | "Reflected" | "Reversed" | None],
        derivative
      },

      (* Compute the derivative(s) depending on input *)
      And[

        (MatchQ[orders, {_Integer..}] && singlePaddingMethod)
            /. True :> (derivative = GaussianFilter[data, {4 sigmas+1, sigmas}, orders, Padding->padding, Method->"Gaussian"]);

        (MatchQ[orders, {{_Integer..}..}] && singlePaddingMethod)
            /. True :> (derivative = GaussianFilter[data, {4 sigmas+1, sigmas}, #, Padding->padding, Method->"Gaussian"]&/@orders);

        (MatchQ[orders, {{_Integer..}..}] && !singlePaddingMethod)
            /. True :> (derivative = MapThread[GaussianFilter[data, {4 sigmas+1, sigmas}, #1, Padding->#2, Method->"Gaussian"]&,{orders, padding}]);

      ] /. False :> (Message[GaussianDerivative::msg]);

      (* Return the derivative *)
      Return[derivative];

    ];


  End[];



EndPackage[];