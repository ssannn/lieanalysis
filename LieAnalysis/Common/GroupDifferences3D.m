(*************************************************************************************************************************
** GroupDifferences3D.m
** This .m file contains functions to compute finite differnces in SE3
**
** Author: T.C.J. Dela Haije <T.C.J.Dela.Haije@tue.nl>
** Author: F.C. Martin <f.c.martin@tue.nl>
** Author: E.J. Creusen <~>
**************************************************************************************************************************)
BeginPackage["LieAnalysis`Common`GroupDifferences3D`"];



	Begin["`Private`"];



	    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
	    (* GroupDifferences3D- - - - - - - - - - - - - - - - - - - - *)
	    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)



		Options[GroupDifferences3D]={
			"DifferentiationOrder"->1,
		  	"SpatialStepSize"->0.88,
		  	"AngularStepSize"->N[Pi/25],
		  	"DifferentiationScheme"->"Central"
		};



    	(* Spatial Derivatives *)
    	GroupDifferences3D[data_, {orientations_, topology_}, direction_String /; MemberQ[{"A1","A2","A3"}, direction], OptionsPattern[]]:= Module[

	      	(* Local Variable Declaration *)
	      	{
		        order = OptionValue["DifferentiationOrder"],
		        type = OptionValue["DifferentiationScheme"],
		        spatialstepsize = OptionValue["SpatialStepSize"],
		        U = data,
		        n = Last[Dimensions[data]],
		        scheme, diffStencil
	      	},

      		(* Differentiate Scheme*)
      		scheme = type <> " " <> ToString[order];

      		(* Create Stencil *)
      		diffStencil = Check[DifferentiationStencil[direction,scheme,spatialstepsize,orientations],Abort[]];

      		Transpose[
		        Table[
		          ListCorrelate[
		            diffStencil[[i]], U[[All,All,All,i]],{2,2,2},0],{i,1,n}
		        ],{4,1,2,3}
      		]//N

    	];



    	(* Angular Derivatives *)
    	GroupDifferences3D[data_, {orientations_,topology_}, direction_String/;MemberQ[{"A4","A5"}, direction],OptionsPattern[]] := Module[
	
			(* Local Variables Declaration *)
			{
				order = OptionValue["DifferentiationOrder"],
				type = OptionValue["DifferentiationScheme"],
				angularstepsize = OptionValue["AngularStepSize"],
				U = data,
				scheme, diffStencil
			},

      		(* Differentiate Scheme *)
      		scheme = type <> " " <> ToString[order];

      		(* Create Stencil *)
      		diffStencil = Check[DifferentiationStencil[direction,scheme,angularstepsize,{orientations, topology}],Abort[]];

      		Map[diffStencil.#&,U,{3}]

    	];



	    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
	    (* DifferentiationStencil- - - - - - - - - - - - - - - - - - *)
	    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)



	    DifferentiationStencil[direction_String /; MemberQ[{"A1","A2","A3"},direction],scheme_String /; MemberQ[{"Forward 1","Backward 1","Central 1","Central 2","Averaging Forward 1","Averaging Backward 1"},scheme], spatialstepsize_ /; spatialstepsize > 0, orientations_ ] :=
	        DifferentiationStencil[direction,scheme,spatialstepsize] = Module[
	
	          (* Local Variables Declaration  *)
	          {
	            rotationmatrices = Rotate2[Sequence@@#]&/@orientations,
	            directionvector = IdentityMatrix[3][[ToExpression[StringTake[direction,-1]]]],
	            vals, stencil
	          },
	
	          vals=Map[SpatialInterpolate,# rotationmatrices.directionvector&/@{-spatialstepsize,0,spatialstepsize},{2}];
	
	          stencil = Switch[scheme,
	            "Forward 1",                    (vals[[3]]-vals[[2]])/spatialstepsize,
	            "Backward 1",                   (vals[[2]]-vals[[1]])/spatialstepsize,
	            "Central 1",                    (vals[[3]]-vals[[1]])/(2*spatialstepsize),
	            "Central 2",                    (vals[[3]]-2vals[[2]]+vals[[1]])/(2*spatialstepsize)^2,
	            "Averaging Forward 1",          (vals[[3]]+vals[[2]])/2,
	            "Averaging Backward 1",         (vals[[2]]+vals[[1]])/2
	          ];
	
	          (* Return the Stencil *)
	          Return[stencil];
	
	    ];



	    DifferentiationStencil[direction_String/;MemberQ[{"A4","A5"},direction],scheme_String/;MemberQ[{"Central 1","Central 2","Forward 1","Backward 1"},scheme],angularstepsize_/;angularstepsize>0, {orientations_, topology_}] :=
	    	DifferentiationStencil[direction,scheme,angularstepsize]=Module[
	
		        {
		          	rotationmatrices= Rotate2[Sequence@@#]&/@orientations,
		          	directionvector= IdentityMatrix[3][[ToExpression[StringTake[direction,-1]]-3]],
		          	vals
		        },
		        
	        	vals=Map[AngularInterpolate[#,orientations, topology]&,(rotationmatrices.Rotate3[directionvector,{0,0,1},#]&/@{-angularstepsize,0,angularstepsize}),{2}];
	
		        SparseArray[
		          	Piecewise[
		            	{
			            	{(vals[[3]] - vals[[1]])/(2*angularstepsize), scheme == "Central 1"},
			              	{(vals[[3]] - 2*vals[[2]] + vals[[1]])/(2*angularstepsize)^2, scheme == "Central 2"},
			              	{(vals[[3]] - vals[[2]])/angularstepsize, scheme == "Forward 1"},
			              	{(vals[[2]] - vals[[1]])/angularstepsize, scheme == "Backward 1"}
		            	}
		          	]
		        ]
	    	];



    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
    (* DifferentiationStencil- - - - - - - - - - - - - - - - - - *)
    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)



    SpatialInterpolate[position_List/;Depth[position]!=2]:=Module[{weights=Table[WeightFunction[p,x],{p,1,3},{x,position}]},

      Map[Times[Sequence@@#]&,MapIndexed[weights[[#1,#2[[-1]]]]&,Nest[Partition[#,3]&,Tuples[Range[3],5],5-1],{-1}],{-2}]

    ];



    SpatialInterpolate[position_List/;Depth[position]==2]:= Module[{weights=Table[WeightFunction[p,x],{p,1,3},{x,position}]},

      Table[weights[[i,1]]weights[[j,2]]weights[[k,3]],{i,1,3},{j,1,3},{k,1,3}]

    ];


    WeightFunction[p_Integer,x_Real]:=Piecewise[{
      {1/2 (Abs[x]-x), p==1},
      {1-Abs[x], p==2},
      {1/2 (Abs[x]+x), p==3}
    }];



    Rotate2=Compile[{{x,_Real},{y,_Real},{z,_Real}},

      Module[{\[Beta]=ArcCos[z],\[Gamma]=Arg[x+I y]},

        {{Cos[\[Beta]] Cos[\[Gamma]],-Sin[\[Gamma]],Cos[\[Gamma]] Sin[\[Beta]]},{Cos[\[Beta]] Sin[\[Gamma]],Cos[\[Gamma]],Sin[\[Beta]] Sin[\[Gamma]]},{-Sin[\[Beta]],0,Cos[\[Beta]]}}

      ]

    ];



    AngularInterpolate[x_List, orientations_, topology_]:=Module[

      {
        triangles=Triangles[orientations,topology],
        joinedtriangles=JoinedTriangles[orientations,topology],
        crossproducts,sides,range,e1,e2,u,row,triangle,triangleid,pos
      },

      If[MemberQ[orientations,x],

        Return[SparseArray[Nearest[orientations->Automatic,x]->1,Length[orientations]]];

      ,

        range = joinedtriangles[[First[Nearest[orientations->Automatic,x]]]];
        crossproducts = CrossProducts[triangles];
        sides = Sides[triangles];


        pos=Position[Transpose[(UnitStep[crossproducts[[range,All,#]].x]*sides[[range,#]]&/@Range[3])],{1,1,1}];

        triangleid=If[Length[pos]>=1,
          range[[pos[[1,1]]]],
          range[[1]]
        ];

        triangle=triangles[[triangleid]];

        e1=triangle[[2]]-triangle[[1]];
        e2=triangle[[3]]-triangle[[1]];

        u=Inverse[{e1,e2,x}\[Transpose]].(x-triangle[[1]]);

        row=SparseArray[topology[[triangleid]]->{(1-u[[1]]-u[[2]]),u[[1]],u[[2]]},Length[orientations]];

        Return[row/Total[row]];

      ]
    ];

    Triangles[orientations_List, topology_List]:=
        Triangles[orientations,topology] = Map[orientations[[#]]&,topology,{2}];

    JoinedTriangles[orientations_List,topology_List]:=
        JoinedTriangles[orientations,topology] = Position[topology,elem_/;elem==#][[All,1]]&/@Range[Length[orientations]];

    CrossProducts[triangles_List] :=
        CrossProducts[triangles] = Map[{#[[1]]\[Cross]#[[2]],#[[2]]\[Cross]#[[3]],#[[3]]\[Cross]#[[1]]}\[Transpose]&,triangles,{1}];

    Sides[triangles_List] :=
        Sides[triangles]=Map[{Sign[#[[1]]\[Cross]#[[2]].#[[3]]],Sign[#[[2]]\[Cross]#[[3]].#[[1]]],Sign[#[[3]]\[Cross]#[[1]].#[[2]]]}&,triangles,{1}];

    Rotate3=Compile[{{v,_Real,1},{x,_Real,1},{\[Phi],_Real}},

      Cos[\[Phi]] x+ v(1-Cos[\[Phi]]) (v.x) + Sin[\[Phi]] (v\[Cross]x)

    ];

  End[];

EndPackage[];