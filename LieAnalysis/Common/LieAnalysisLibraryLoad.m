(*************************************************************************************************************************
** LieAnalysisLibraryLoad.m
** This .m file contains functions to load custom CUDA functions
**
** Author: F.C. Martin <f.c.martin@tue.nl>
**************************************************************************************************************************)
BeginPackage["LieAnalysis`Common`LieAnalysisLibraryLoad`"];



	Begin["`Private`"];
	
		Get["LieAnalysis`Common`"];
		
		LoadCppFunction[functionName_] := Module[
			{
				baseUrl = LieAnalysis["LibrariesURL"],
				libraryName = "libMathematicaHFM",
				libraryLocation = FileNameJoin[Join[(FileNameSplit@FindFile["LieAnalysis`"])[[1;;-3]],{"libraries"}]],
				function, libFile 
			},
			
			libFile = FileNameJoin[{libraryLocation, libraryName<>".dll"}];
			
			(* Create Directory if needed *)
			If[!DirectoryQ[libraryLocation],CreateDirectory[libraryLocation]];
			
			(* Download File if needed *)
			If[!FileExistsQ[libFile],
				DownloadFile[Echo[URLBuild[{baseUrl, libraryName<>".dll"}],"Downloading: "], cudaLibraryLocation, 10];
			];
			
			(* Load the function *)
			function = Switch[functionName,
				
				"orientationScoreTransform", 
					LibraryFunctionLoad[libFile, "RunModel", {}, Real],
				
				"GetScalar",
					LibraryFunctionLoad[libFile, "GetScalar", {String(*key*)}, Real(*keyval*)],
					
				"GetString",
					LibraryFunctionLoad[libFile, "GetString", {String(*key*)}, String(*keyval*)],
					
				"GetVector",
					LibraryFunctionLoad[libFile, "GetVector", {String(*key*)}, {Real,1}(*keyval*)],
				
				"GetArray1",LibraryFunctionLoad[libFile, "GetArray", {String(*key*), Integer(*Dimension*)}, {Real,1}(*keyval*)],
				"GetArray2",LibraryFunctionLoad[libFile, "GetArray", {String(*key*), Integer(*Dimension*)}, {Real,2}(*keyval*)],
				"GetArray3",LibraryFunctionLoad[libFile, "GetArray", {String(*key*), Integer(*Dimension*)}, {Real,3}(*keyval*)],
				"GetArray4",LibraryFunctionLoad[libFile, "GetArray", {String(*key*), Integer(*Dimension*)}, {Real,4}(*keyval*)],
				"GetArray5",LibraryFunctionLoad[libFile, "GetArray", {String(*key*), Integer(*Dimension*)}, {Real,5}(*keyval*)],
				"GetArray6",LibraryFunctionLoad[libFile, "GetArray", {String(*key*), Integer(*Dimension*)}, {Real,6}(*keyval*)],
				
				"SetScalar",
					LibraryFunctionLoad[libFile, "SetScalar", {String(*key*), Real(*val*)}, "Void"],
					
				"SetString",
					LibraryFunctionLoad[libFile, "SetString", {String(*key*), String(*val*)}, "Void"],
				
				"SetVector",
					LibraryFunctionLoad[libFile, "SetVector", {String(*key*), {Real,1}(*val*)}, "Void"],
				
				"SetArray1",LibraryFunctionLoad[libFile, "SetArray", {String(*key*), {Real,1}}(*keyval*),"Void"],
				"SetArray2",LibraryFunctionLoad[libFile, "SetArray", {String(*key*), {Real,2}}(*keyval*),"Void"],
				"SetArray3",LibraryFunctionLoad[libFile, "SetArray", {String(*key*), {Real,3}}(*keyval*),"Void"],
				"SetArray4",LibraryFunctionLoad[libFile, "SetArray", {String(*key*), {Real,4}}(*keyval*),"Void"],
				"SetArray5",LibraryFunctionLoad[libFile, "SetArray", {String(*key*), {Real,5}}(*keyval*),"Void"],
				"SetArray6",LibraryFunctionLoad[libFile, "SetArray", {String(*key*), {Real,6}}(*keyval*),"Void"],
				
				"SetArray",
							LibraryFunctionLoad[libFile, "SetArray", {String(*key*), {Real,d}(*val (d-dimensional array)*)}, "Void"]
				
			];
			
			(* Return function object *)
			Return[function]
			
			
		]
	
	
	End[];
	
	
	
EndPackage[];
