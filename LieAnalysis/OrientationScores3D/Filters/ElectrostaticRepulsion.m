(*************************************************************************************************************************
** ElectrostaticRepulsion.m
** This .m file contains common function used for evenly sampled sphere by using electrostatic repulsion on a sphere.
**
** Author: M. Froeling <~>
** Author: F.C. Martin <f.c.martin@tue.nl>
** Version: 0.1
**************************************************************************************************************************)
BeginPackage["LieAnalysis`OrientationScores3D`Filters`ElectrostaticRepulsion`"];

  SphereSampleCoordinates::usage = "
    todo
    ";

  Begin["`Private`"];



    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
    (* SphereSampleCoordinates - - - - - - - - - - - - - - - - - *)
    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)



    SphereSampleCoordinates[n_, iterations_:1000] := Block[

      (* Local Variables Declaration*)
      {points},

      (* Random sample the sphere *)
      points = SphereRandomSampleCoordinates[n];

      (* Optimize the position of 'n' points on a sphere in 'iterations' steps *)
      Do[
        points = GradOptimize[points],
        {iterations}
      ];

      (* Return the (alsmost) uniformly sampled sphere coordinates *)
      Return[points];

    ];



    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
    (* SphereRandomSampleCoordinates - - - - - - - - - - - - - - *)
    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)



    SphereRandomSampleCoordinates[n_] := Sign[#[[3]]+10.^-16] Normalize[#]&/@RandomReal[NormalDistribution[],{n,3}];



    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
    (* GradOptimize  - - - - - - - - - - - - - - - - - - - - - - *)
    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)



    GradOptimize = Compile[{{points,_Real,2}},Block[

      {n,pointsi,pointsmat,distmatxyz,diag,distmat,velocity,pointsnew},

      n=Length[points];
      pointsi=Join[points,-points];
      pointsmat=ConstantArray[pointsi,2 n];
      distmatxyz=pointsmat-Transpose[pointsmat];
      diag=DiagonalMatrix[ConstantArray[10.^32,2 n]];
      distmat=Total[Transpose[distmatxyz,{2,3,1}]^2]+diag;
      velocity=Total[(distmatxyz/distmat)];
      pointsnew=(Sign[#[[3]]+10.^-16]*Normalize[#]&/@(pointsi+velocity));
      pointsnew[[;;n]]]

    ];



  End[];



EndPackage[];