(*************************************************************************************************************************
** CakeWavelets.m
** This .m file contains functions to create a 3D Cake Wavelet Stack.
**
** Author: M.H.J.Janssen <m.h.j.janssen@tue.nl>
** Author: F.C. Martin <f.c.martin@tue.nl>
** Version: 0.1
**************************************************************************************************************************)
BeginPackage["LieAnalysis`OrientationScores3D`Filters`CakeWavelets`",{
  "PolyhedronOperations`",
  "LieAnalysis`OrientationScores3D`Filters`ElectrostaticRepulsion`"
}];



  Begin["`Private`"];

    Needs["Classes`"];
    Needs["LieAnalysis`Objects`ObjCakeWavelet3D`"];
    Get["LieAnalysis`Common`"];
    Get["LieAnalysis`Ultilities`"];
    
    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
    (* CakeWaveletStack3D- - - - - - - - - - - - - - - - - - - - *)
    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)



    Options[CakeWaveletStack3D] = {
      "Epsilon" -> 0.00001,
      "SigmaAngle" -> Automatic,
      "Gamma" -> 0.8,
      "DcStandardDeviation" -> 8
    };



    CakeWaveletStack3D::invalidInput = "The supplied value '`1`' for option `2` is invalid. (`3`)";



    CakeWaveletStack3D[nOrientations_, size_, opts:OptionsPattern[]] := Module[

      (*Local Variables Declaration *)
      {
        epsilon = OptionValue["Epsilon"],
        sigmaAngle = OptionValue["SigmaAngle"],
        gamma = OptionValue["Gamma"],
        dcstddev = OptionValue["DcStandardDeviation"],
        sigmaRadialHigh, n, cws3dObj, radius
      },

      (* Input Validation *)
      And[

        (Internal`PositiveIntegerQ[nOrientations] || MatchQ[nOrientations, {{_,_,_}..}])
            /. False :> (Message[CakeWaveletStack3D::invalidInput, nOrientations, "Orientations", "Sould be a positive integer"]; False),

        (OddQ[size] && Internal`PositiveIntegerQ[size])
            /. False :> (Message[CakeWaveletStack3D::invalidInput, size, "Size", "Sould be an odd positive integer"]; False),

        (Positive[epsilon])
            /. False :> (Message[CakeWaveletStack3D::invalidInput, epsilon, "Epsilon", "Sould be an odd positive number"]; False),

        (Positive[sigmaAngle] || sigmaAngle === Automatic)
            /. False :> (Message[CakeWaveletStack3D::invalidInput, sigmaAngle, "SigmaAngle", "Should be a positive number"]; False),

        Between[gamma, {0,1}]
            /. False :> (Message[CakeWaveletStack3D::invalidInput, gamma, "Gamma", "Should be a number between 0 and 1"]; False),

        (Between[sigmaAngle, {0,1}] || sigmaAngle === Automatic)
            /. False :> (Message[CakeWaveletStack3D::invalidInput, sigmaAngle, "SigmaAngle", "Should be a positive number between 0 and 1"]; False),

        Positive[dcstddev]
            /. False :> (Message[CakeWaveletStack3D::invalidInput, dcstddev, "DcStandardDeviation", "Should be a positive integer"]; False)

      ] /. False :> Abort[];

      (* Compute the radial (gaussian) cut-off *)
      sigmaRadialHigh = (1 - gamma)/2;

      (* Because of symmetry we only want half the orientations *)
      n = If[IntegerQ[nOrientations],
        Ceiling[nOrientations/2],
        n = nOrientations (* in case an orientation list is provided *)
      ];
      
      (* size to radius *)
      radius = (size - 1)/2;

      (* Create cakewavelet 3d stack *)
      cws3dObj = CakeWaveletStack3D[n, radius, sigmaAngle, gamma, sigmaRadialHigh, dcstddev, epsilon];
      
      (* Store used options in object *)
      AffixTo[cws3dObj,"SpecifiedOptions"->{opts}];

      (*Return the Orientation Score Transformed image *)
      Return[cws3dObj]

    ];



    CakeWaveletStack3D[nOrientations_, size_, sigmaAngle2_, gamma_, sigmaRadialHigh_, DcStandardDeviation_, epsilon_] := Module[

      (* Local Variables Declaration *)
      {
        DcStandardDeviationF = (*size*)1/(DcStandardDeviation*Pi), (* Convert Dc Standard deviation to the Fourier Domain *)
        map = If[OptionValue[LieAnalysis,"DynamicUpdates"], GeneralUtilities`MonitoredMap, Map],
        scale, sigmaAngle = sigmaAngle2, shCoefficientsVector, shAntiSymmetrizationMatrix,shFunkTransformMatrix,shRotationMatrix,
        sphericalHarmonicsTable, dcFilter, wavelet, waveletF, gridOrientationCoords, lMax, waveletOrientationCoords, cake3DObj,
        mnWindow3D, dcWindow3D, topology, mesh, meanVertexDistance, waveletOrientationCoordsFull
      },

      (* Create container to store the wavelets in *)
      cake3DObj = ObjCakeWavelet3D[];

      (* Compute uniform sampled coordinates on the sphere *)
      If[IntegerQ[nOrientations],
      	symmetry = "AntiPodal";
        waveletOrientationCoords = SphereSampleCoordinates[nOrientations];
        waveletOrientationCoordsFull = Join[waveletOrientationCoords,-waveletOrientationCoords];
        cake3DObj = Affix[cake3DObj, 
        	{
        		"OrientationList" -> waveletOrientationCoords, 
        		"Symmetry" -> "AntiPodal"
        	}
        ];
        ,
        waveletOrientationCoords = N@nOrientations;
        waveletOrientationCoordsFull = waveletOrientationCoords;
        cake3DObj = Affix[cake3DObj, 
        	{
        		"OrientationList" -> waveletOrientationCoords,
        		"Symmetry" -> None
        	}
        ];
      ];

      (* Determine the neighbors of each orientation *)
      mesh = ConvexHullMesh[waveletOrientationCoordsFull];
      topology = MeshCells[mesh,2][[All,1]];

      (* Compute the sigmaAngle based on the orientations *)
      If[sigmaAngle === Automatic,
        meanVertexDistance = Mean[Re[ArcCos[#[[1]].#[[2]]]] & /@ MeshPrimitives[mesh, 1][[All, 1]]];
        sigmaAngle = (meanVertexDistance/2)/Sqrt[Log[2]];
      ];
      scale = (sigmaAngle^2) / 2;(* Scale of the spherical harmonics *)

      (* Compute maximal SH order L *)
      lMax = DetermineMaxOrderL[scale, epsilon];

      (* Compute grid upon which the Spherical Harmonics can be defined *)
      gridOrientationCoords = GridOrientationsTable[size];

      (* Compute the spherical harmonics table upon the gridOrientations up to order lMax *)
      sphericalHarmonicsTable = SphericalHarmonicTable[gridOrientationCoords, lMax, False]; (* = Y_{m}^{l}(\theta, \phi) *)

      (* Create the matrix used to perform the rotation of a function expressed in spherical harmonics towards the input orientation *)
      shRotationMatrix = SphericalHarmonicsRotateMatrix[waveletOrientationCoords, lMax, True];(* R_{\alpha, \beta, \gamma} *)

      (* Create the matrix used to perform the Funk transform of a function expressed in spherical harmonics *)
      shFunkTransformMatrix = SphericalHarmonicFunkTransformMatrix[lMax]; (* c -> 2Pi P_l c *)

      (* Create the matrix used to perform the anti-symmetrization of a function expressed in spherical harmonics *)
      shAntiSymmetrizationMatrix = SphericalHarmonicAntiSymmetrizationMatrix[lMax]; (* c -> (1 - (-1)^l ) c *)

      (* Determine Spherical Harmonic coefficients up to order 'L' of the diffusion kernel for diffusion scale 'scale' *)
      shCoefficientsVector = SphericalHarmonicCoefficientsDiffusionKernel[lMax, scale];

      (* Apply the Funk-transform and Anti-Symmetrization to the coefficients *)
      shCoefficientsVector = shFunkTransformMatrix.shCoefficientsVector + shAntiSymmetrizationMatrix.shCoefficientsVector; (* F.s + A.s *)

      (* Rotate (steer the coefficients) the wavelets *)
      shCoefficientsVector = Map[#.shCoefficientsVector &, shRotationMatrix];

      (* Compute/'Lookup' wavelets from spherical harmnics table *)
      waveletF = map[ArrayReshape[ sphericalHarmonicsTable.# , {2 size+1, 2 size+1, 2 size+1}]&, shCoefficientsVector];

       (*Remove the high frequencies by applying a Gaussian decay in radial direction*)
      mnWindow3D = MnWindow3D[size, sigmaRadialHigh, gamma];  (*analog to the MnWindow in the 2D case*)

       (*Remove the low frequencies by removing the center*)
      dcWindow3D =  DcWindow3D[size, DcStandardDeviationF];

       (*Apply dc and mn window to the fourier wavelet*)
      waveletF = Map[#*(mnWindow3D - dcWindow3D)&, waveletF];
      cake3DObj = Affix[cake3DObj, "WaveletsF" -> waveletF];

       (*Compute Fourier Inverse*)
      wavelet = Map[CenteredInverseFourier, waveletF];
      dcFilter = Re[CenteredInverseFourier[dcWindow3D]];

       (*Add meta-data*)
      cake3DObj = Affix[cake3DObj, {
        "Data"                      -> wavelet,
        "RadialWindow"              -> (mnWindow3D - dcWindow3D),
        "DcFilter"                  -> dcFilter,
        "OrientationCoordinates"    -> waveletOrientationCoords,
        "FullTopology"              -> topology,
        "Method"                    -> "JMIV",
        "LMax"                      -> lMax,
        "SigmaAngle"                -> sigmaAngle,
        "DcStandardDeviation"       -> DcStandardDeviationF,
        "MnStandardDeviation"       -> sigmaRadialHigh,
        "Gamma"                     -> gamma,
        "Epsilon"                   -> epsilon
      }];

       (*Return the 3D cake wavelets*)
      Return[cake3DObj]
    ];


    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
    (* MnWindow3D- - - - - - - - - - - - - - - - - - - - - - - - *)
    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)



    MnWindow3D[size_, MnStandardDeviation_, gamma_] := RadialFunctionErf[MnStandardDeviation,gamma(**size*)][GridRadiusTable[size]];
    NormalizedComplementaryErrorFunction[x_,sigma_,offset_]:=Erfc[(x-offset)/(Sqrt[2]sigma)]/2;
    RadialFunctionErf[sigmaHigh_,offsetHigh_] := Compile[{\[Rho]}, Evaluate[NormalizedComplementaryErrorFunction[\[Rho],sigmaHigh,offsetHigh]], RuntimeAttributes -> {Listable}];


    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
    (* DcWindow3D- - - - - - - - - - - - - - - - - - - - - - - - *)
    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)



    DcWindow3D[size_, DcStandardDeviation_] := Gaussian[DcStandardDeviation][GridRadiusTable[size]];



    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
    (* DcWindow3D- - - - - - - - - - - - - - - - - - - - - - - - *)
    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)



    Gaussian[sigma_] := Compile[{\[Rho]},Evaluate[E^(-\[Rho]^2/(2*sigma^2)) ],RuntimeAttributes->{Listable}];



    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
    (* SphericalHarmonicFunkTransformMatrix- - - - - - - - - - - *)
    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)



    SphericalHarmonicCoefficientsDiffusionKernel[L_,so_] := Table[If[SHIndexj2m[j]==0, Sqrt[(2SHIndexj2l[j]+1)/(4\[Pi])] E^(-(SHIndexj2l[j]+1)SHIndexj2l[j]so), 0], {j, 1, SHIndexlm2j[L, L]}];



    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
    (* SphericalHarmonicFunkTransformMatrix- - - - - - - - - - - *)
    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)



    SphericalHarmonicFunkTransformMatrix[s_List] := s.Transpose[SphericalHarmonicFunkTransformMatrix[SHIndexj2l[Dimensions[s][[-1]]]]];
    SphericalHarmonicFunkTransformMatrix[lMax_Integer] := DiagonalMatrix[Table[LegendreP[SHIndexj2l[jp],0],{jp,1,SHIndexlm2j[lMax,lMax]}]];



    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
    (* SphericalHarmonicsRotateMatrix- - - - - - - - - - - - - - *)
    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)



    SphericalHarmonicAntiSymmetrizationMatrix[s_List]:=s.Transpose[SHAntiSymmetrization[SHIndexj2l[Dimensions[s][[-1]]]]];
    SphericalHarmonicAntiSymmetrizationMatrix[lMax_Integer] := DiagonalMatrix[Table[1-(-1)^SHIndexj2l[jp],{jp,1,SHIndexlm2j[lMax,lMax]}]];



    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
    (* SphericalHarmonicsRotateMatrix- - - - - - - - - - - - - - *)
    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)



    SphericalHarmonicsRotateMatrix[gridOrientationCoords_, lMax_, symmetricQ_]:= Module[

      {gamma, beta, rotationMatrix},

      (* Convert to Spherical coordinates *)
      {beta, gamma} = Transpose[CartesianToSphericalCoordinates[gridOrientationCoords], Join[Range[2, Depth[gridOrientationCoords] - 1], {1}]][[2;;3]];

      (* Compute rotation matrix *)
      rotationMatrix = RotateMatrixZY[gamma, beta, lMax, symmetricQ];

      (* Return the rotation Matrix *)
      Return[rotationMatrix];

    ];
    RotateMatrixZY[gamma_, beta_, lMax_, symmetricQ_]:= Module[

      {compiledRotateZY, j = SHIndexlm2j, l = SHIndexj2l, m = SHIndexj2m},

      compiledRotateZY = Compile[{ggamma, bbeta},
        Evaluate[
          Module[{M = ConstantArray[0, {j[lMax, lMax], j[lMax, lMax]}]},
            If[symmetricQ,
              Table[(M[[jp, j[l[jp], 0]]] = WignerD[{l[jp], m[jp], 0}, -ggamma, -bbeta, 0]), {jp, 1, j[lMax, lMax]}],(*we only need the values for m' = 0*)
              Table[Table[(M[[jp, j[l[jp], mp]]] = WignerD[{l[jp], m[jp], mp}, -ggamma, -bbeta, -0]), {mp, -l[jp], l[jp]}], {jp, 1, j[lMax, lMax]}]
            ];
            M
          ]
        ],
        RuntimeAttributes-> Listable
      ];

      Return[compiledRotateZY[gamma, beta]]

    ];
    (* todo: SHCompiledRotateZYZ function? Where is it good for? *)
	
    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
    (* DetermineMaxOrderL- - - - - - - - - - - - - - - - - - - - *)
    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)



    DetermineMaxOrderL[so_,epsilon_:10^-3] := Module[

    (* Local Varialble Declaration *)
      {l = 0},

    (* Compute the maximal order L *)
      While[
        Sqrt[(2l+1)] * E^(-(l+1)l * so)>epsilon,
        l++
      ];

      (* Return the maximal order *)
      Return[l];

    ];



    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
    (* SphericalHarmonicTable- - - - - - - - - - - - - - - - - - *)
    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)


    (* todo: cleanup with the function that are used in the common context *)
    SphericalHarmonicTable[orientations_, lMax_Integer, bool_] := Module[{Mmat, R, \[Theta], \[Phi], CSHT = CompiledSHTable[lMax,bool]},
      {R, \[Theta], \[Phi]} = Transpose[CartesianToSphericalCoordinates[orientations], Join[Range[2, Depth[orientations] - 1], {1}]];
      Mmat = CSHT[\[Theta], \[Phi]];
      Mmat
    ];
    CompiledSHTable[lMax_Integer, bool_] := Compile[{th,ph},
      Evaluate[
        If[bool,
          N[ExpToTrig[Table[If[SHIndexj2m[j]==0,SphericalHarmonicY[SHIndexj2l[j],SHIndexj2m[j],th,ph],0],{j,1,(lMax+1)^2}]]],
          N[ExpToTrig[Table[SphericalHarmonicY[SHIndexj2l[j],SHIndexj2m[j],th,ph],{j,1,(lMax+1)^2}]]]
        ]
      ],
      RuntimeAttributes->Listable
    ];



    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
    (* GridOrientationsTable - - - - - - - - - - - - - - - - - - *)
    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)



    GridOrientationsTable = Compile[{{size,_Integer}},

      Flatten[
        Table[
          If[x==0&&y==0&&z==0,
            {1,0.,0.},
            N[{x,y,z}/Sqrt[x^2+y^2+z^2]]
          ],
          {x,-size,size},{y,-size,size},{z,-size,size}
        ],2
      ]

    ];



    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
    (* GridRadiusTable - - - - - - - - - - - - - - - - - - *)
    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)



    GridRadiusTable = Compile[{{size,_Integer}},

      Table[
        Sqrt[x^2+y^2+z^2]/size,
        {x,-size,size},{y,-size,size},{z,-size,size}
      ]

    ];



    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
    (* Spherical Harmonics Indexing Functions- - - - - - - - - - *)
    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)


    (*todo: remove these as these are now part of the common context *)
    SHIndexj2l[j_] := Floor[Sqrt[j-1]];
    SHIndexlm2j[l_,m_] := Round[(l^2+l+1)+m];
    SHIndexj2m[j_] := Round[j-1-(SHIndexj2l[j])^2-SHIndexj2l[j]];




End[];



EndPackage[];