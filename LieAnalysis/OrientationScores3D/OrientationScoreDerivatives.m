(*************************************************************************************************************************
** OrientationScoreDerivatives.m
** This .m file contains functions used to compute the derivatives of 3D Orientation Scores.
**
** Author: M.H.J.Janssen <m.h.j.janssen@tue.nl>
** Author: F.C. Martin <f.c.martin@tue.nl>
**************************************************************************************************************************)
BeginPackage["LieAnalysis`OrientationScores3D`OrientationScoreDerivatives`",{
  "LieAnalysis`"
}];



  Begin["`Private`"];



    Needs["Classes`"];
    Get["LieAnalysis`Common`"];



    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
    (* OrientationScoreDerivatives3D - - - - - - - - - - - - - - *)
    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)



	LeftInvariantDerivatives3D[os3dObj_ /; ValidQ[os3dObj, LieAnalysis`Objects`Obj3DPositionOrientationData`Obj3DPositionOrientationData], derivativeIndex_, opts:OptionsPattern[]] := Module[
		
		(* Local Varialble Declaration *)
		{
			derivatives
		},
		
		(StringQ[derivativeIndex] || IntegerQ[derivativeIndex] || MatchQ[derivativeIndex, {_Integer...}]) /. False :> (Message[General::argfail, "derivativeIndex", "Should be a Integer, String or List of Integers"]);
        
        (* Compute default left-invariant derivatives *)
        derivatives = LieAnalysis`OrientationScores3D`Derivatives`LeftInvariantDerivatives`Private`LeftInvariantDerivativesInternal[
        	os3dObj, 0, 0, derivativeIndex, "FiniteDifferences"
        ];
        
        (* Append the used options to the object *)
		derivatives = Switch[Head[derivatives],
			List,
				(Affix[#,"SpecifiedOptions"->{opts}])&/@derivatives,
			_,
				Affix[derivatives,"SpecifiedOptions"->{opts}]
		];
		
        (* Return *)
        Return[derivatives];

	];
	
	
    LeftInvariantDerivatives3D[os3dObj_ /; ValidQ[os3dObj, LieAnalysis`Objects`Obj3DPositionOrientationData`Obj3DPositionOrientationData],{sigmaSpatial_, sigmaOrientation_},derivativeIndex_, opts:OptionsPattern[]] := Module[

        (* Local Varialble Declaration *)
        {
          derivatives
        },

		
        (* Check Input *)
        (Positive[sigmaOrientation] || sigmaOrientation === Automatic) /. False :> (Message[General::argfail, "sigmaOrientation", "be a positive real number or automatic"]);

        Positive[sigmaSpatial] /. False :> (Message[General::argfail, "sigmaSpatial", "be a positivie real number"]);
        
        (StringQ[derivativeIndex] || IntegerQ[derivativeIndex] || MatchQ[derivativeIndex, {_Integer...}]) /. False :> (Message[General::argfail, "derivativeIndex", "Should be a Integer, String or List of Integers"]);
        
        
        
        (* Compute default left-invariant derivatives *)
        derivatives = LieAnalysis`OrientationScores3D`Derivatives`LeftInvariantDerivatives`Private`LeftInvariantDerivativesInternal[
        	os3dObj, sigmaSpatial, sigmaOrientation, derivativeIndex, "SphericalHarmonics"
        ];
        
        (* Append the used options to the object *)
		derivatives = Switch[Head[derivatives],
			List,
				(Affix[#,"SpecifiedOptions"->{opts}])&/@derivatives,
			_,
				Affix[derivatives,"SpecifiedOptions"->{opts}]
		];
		
        (* Return *)
        Return[derivatives];

    ];



  End[];



EndPackage[];