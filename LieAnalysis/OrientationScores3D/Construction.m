(*************************************************************************************************************************
** Construction.m
** This .m file contains common function used to construct the 3D Orientation Scores.
**
** Author: M.H.J.Janssen <m.h.j.janssen@tue.nl>
** Author: F.C. Martin <f.c.martin@tue.nl>
**************************************************************************************************************************)
BeginPackage["LieAnalysis`OrientationScores3D`Construction`",{
  	"LieAnalysis`",
  	"Classes`"
}];



  Begin["`Private`"];



    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
    (* WaveletTransform3D- - - - - - - - - - - - - - - - - - - - *)
    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)



    WaveletTransform3D[data_, wavelet3dObj_/;ValidQ[wavelet3dObj,LieAnalysis`Objects`ObjOrientationWavelet3D`ObjOrientationWavelet3D]] := Module[

      	(* Local Variables Declaration *)
      	{objwtf3d, wt3d},

      	(* Compute convolved volumes *)
      	wt3d = WaveletTransform3D[data, wavelet3dObj[["Data"]]];

	    (* Store Result *)
		objwtf3d = LieAnalysis`Objects`ObjWaveletTransform`ObjWaveletTransform[{
			"Data"->wt3d, 
			"InputData"->data, 
			"Wavelets"->wavelet3dObj, 
			"OrientationList"->wavelet3dObj[["OrientationList"]]
		}];

      	(* Return the Wavelet Transform *)
      	Return[objwtf3d]

    ];
    
    (* If something is inputted *)
	WaveletTransform3D[img_ /; ArrayQ[img], kernels_List] := 
		WaveletTransform3D[LieAnalysis`Common`ToImage3D[img],kernels]
	
	(* In case of Complex wavelets *)
	WaveletTransform3D[img_, kernels_List /; (Re[kernels] =!= kernels)] := 
		(#1+ I#2)&@@Map[WaveletTransform3D[img, #]&,{Re[kernels], -Im[kernels]}];

	(* Actual implementation *)
    WaveletTransform3D[data3d_/;ImageQ[data3d], kernels_List] := Module[

      (* Local Variables Declaration *)
      {
      	map = If[OptionValue[LieAnalysis,"DynamicUpdates"], GeneralUtilities`MonitoredMap, Map],
      	wt3d
      },

      (* Compute convolved volumes *)
      wt3d = map[ImageConvolve[data3d, Re[#], Padding->"Reversed"] &, kernels];

      (* Smash them into a single object *)
      wt3d = ImageData[ColorCombine[wt3d]];

      (* Return the transformed image *)
      Return[wt3d]

    ];



    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
    (* OrientationScoreTransform3D - - - - - - - - - - - - - - - *)
    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)



    Options[OrientationScoreTransform3D] = DeleteDuplicates@Join[Options[CakeWaveletStack3D],{
        "Orientations"  ->  12,
        "WaveletSize"   ->  19,
        "WaveletPreProcess"->Re
	}];
    
    
    OrientationScoreTransform3D[data3d_, opts:OptionsPattern[]] := Module[

      	(* Local Variables Declaration *)
      	{
        	waveletSize = OptionValue["WaveletSize"],
        	orientations = OptionValue["Orientations"],
        	cws3d, os3d
      	},

      	(* Create cakewaveletstack 3d *)
      	cws3d = CakeWaveletStack3D[orientations,waveletSize,FilterRules[{opts}, Options[CakeWaveletStack3D]]];

      	(* Compute OS 3D *)
      	os3d = OrientationScoreTransform3D[data3d, cws3d, opts];
		
		(* Store used options in object *)
      	AffixTo[os3d,"SpecifiedOptions"->{opts}];
      
      	(* Return the Orientation Score Transformed image *)
      	Return[os3d];

    ];


    OrientationScoreTransform3D[data3d_, cwsObj3D_/;ValidQ[cwsObj3D, LieAnalysis`Objects`ObjCakeWavelet3D`ObjCakeWavelet3D], OptionsPattern[]] := Module[

		(* Local Variables Declaration *)
		{data, dc3d, os3d, os3dObj, cws, f = OptionValue["WaveletPreProcess"]},
		
		(* Pre-process Wavelets *)
		cws = Affix[cwsObj3D,"Data"->f[cwsObj3D[["Data"]]]];
		
		(* Process the input *)
		data = LieAnalysis`Common`InputProcessor3D[data3d];
		
		(* Compute wavelet transform *)
		os3d = WaveletTransform3D[data, cws];
		os3d[[0]] = ClassSet[os3d[[0]],LieAnalysis`Objects`Obj3DPositionOrientationData`Obj3DPositionOrientationData];
		os3dObj = os3d;
		
		(* DC Component *)
      	dc3d = WaveletTransform3D[data,{cws[["DcFilter"]]}];
     
      	os3dObj = Affix[os3dObj, {
      		"DcData" -> dc3d, 
      		"Symmetry"->cws[["Symmetry"]], 
      		"OrientationList"->cws[["OrientationList"]]
      	}];

      	(* Return the Orientation Score Transformed image *)
      	Return[os3dObj];

    ];



  End[];



EndPackage[];