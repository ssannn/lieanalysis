(*************************************************************************************************************************
** OrientationScoreTensor.m
** This .m file contains functions used to compute 'tensors' from the SE(3) space.
**
** Author: F.C. Martin <f.c.martin@tue.nl>
**************************************************************************************************************************)
BeginPackage["LieAnalysis`OrientationScores3D`OrientationScoreTensor`",{
  "LieAnalysis`"
}];



  	Begin["`Private`"];



	    Needs["Classes`"];
	    Get["LieAnalysis`Common`"];

		
		
		Options[OrientationScoreTensor3D] = {
			"Method" -> "SphericalHarmonics"
		};
		
		
		
		OrientationScoreTensor3D[os3dObj_ /; ValidQ[os3dObj, LieAnalysis`Objects`Obj3DPositionOrientationData`Obj3DPositionOrientationData], {sigmaSpatial_, sigmaOrientation_},
      		order_List /; MatchQ[order, {_Integer...}], opts:OptionsPattern[]] := Block[
  		
  			(* Local Variable Declaration *)
  			{
  				method = OptionValue["Method"],
  				derivatives, tensor, labels	
  			},
			
			(* Check orders *)
			  			
  			(* Compute Derivatives *)
  			derivatives = Transpose[#["Data"],{2,3,4,1}]&/@(LieAnalysis`OrientationScores3D`Derivatives`LeftInvariantDerivatives`Private`LeftInvariantDerivativesInternal[os3dObj, sigmaSpatial, sigmaOrientation, order, method]);
  			
  			(* Merge into single Tensor *)
        	tensor = Transpose[
        		derivatives,
        		{5,4,1,2,3}
        	];
        	
        	(* Set labels *)
        	labels = Table["A" <> (ToString[#] & /@ Riffle[IntegerDigits[dir], "A"]),{dir,order}];
  			
  			(* Set in output object *)
	    	Return[
				LieAnalysis`Objects`ObjTensor`ObjTensor[{
					"Data" -> tensor,
					"Labels" -> labels,
					"SpatialDimensions"->3,
					"OrientationScore"->os3dObj
				}]
    		]
  			
  		];
		
		
		
		OrientationScoreTensor3D[os3dObj_ /; ValidQ[os3dObj, LieAnalysis`Objects`Obj3DPositionOrientationData`Obj3DPositionOrientationData], {sigmaSpatial_, sigmaOrientationIn_},
      		order_String, opts:OptionsPattern[]] := Block[
      	
	      	(* Local variable Declaration *)
	      	{
	      		tensorObj, sigmaOrientation
	      	},
	      	
	      	(* Convert Automatic *)
	      	sigmaOrientation = If[sigmaOrientationIn === Automatic,
		   		(0.216667 + 1.06061 os3dObj[["Wavelets"]][["SigmaAngle"]]) (* See #87 *),
		   		sigmaOrientationIn
	   		];
	      	
	      	(* Compute Tensor *)
	      	Switch[ToUpperCase@order,
	      		
		        "GRADIENT",( 
		        	
		        	tensorObj = OrientationScoreTensor3D[os3dObj, {sigmaSpatial, sigmaOrientation}, {1,2,3,4,5}, opts];
		        	
		        )
		        
		        ,
		        "HESSIAN", (  
					
					Block[{A1,A2,A3,A4,A5,A11,A12,A13,A14,A15,A22,A23,A24,A25,A33,A34,A35,A44,A54,A55,A6, tensor, labels},
								        	
			        	(* Compute Derivatives *)	
			        	{A1,A2,A3,A4,A5,A11,A12,A13,A14,A15,A22,A23,A24,A25,A33,A34,A35,A44,A54,A55} = 
			        		Transpose[#["Data"],{2,3,4,1}]&/@LieAnalysis`OrientationScores3D`Derivatives`LeftInvariantDerivatives`Private`LeftInvariantDerivativesInternal[os3dObj, sigmaSpatial, sigmaOrientation, {1,2,3,4,5,11,12,13,14,15,22,23,24,25,33,34,35,44,54,55},OptionValue["Method"]];
			            
			            
			            (* As A6 = 0, we fill the rest with zeros *)
			            A6 = ConstantArray[0, Dimensions[A1]]; 
			            
			            (* Assemble Tensor *)
						tensor = Transpose[
							Developer`ToPackedArray@{
									{A11,	A12,	A13,	A14,	A15-A3,	A2		},
									{A12,	A22,	A23,	A24+A3,	A25,	-A1		},
									{A13,	A23,	A33,	A34-A2,	A35+A1, A6		},
									{A14,	A24,	A34,	A44,	A54,	A5		},
									{A15,	A25, 	A35, 	A54, 	A55,	-A4		},
									{A6,	A6,		A6,		A6,		A6,		A6		}
							},{5,6,4,1,2,3}
						];
						
						(* Labels *)
						labels = {
							{"A1A1", "A2A1","A3A1","A4A1","A5A1","A6A1"},
							{"A1A2", "A2A2","A3A2","A4A2","A5A2","A6A2"},
							{"A1A3", "A2A3","A3A3","A4A3","A5A3","A6A3"},
							{"A1A4", "A2A4","A3A4","A4A4","A5A4","A6A4"},
							{"A1A5", "A2A5","A3A5","A4A5","A5A5","A6A5"},
							{"A1A6", "A2A6","A3A6","A4A6","A5A6","A6A6"}
						};
						
						tensorObj = LieAnalysis`Objects`ObjTensor`ObjTensor[{
							"Data" -> tensor,
							"Labels" -> labels,
							"SpatialDimensions"->3,
							"OrientationScore"->os3dObj,
							"MetaData"->Association["Name"->"Hessian", "SpatialSigma"->sigmaSpatial, "AngularSigma"->sigmaOrientation]
						}]
						
					]
		        )
		        
		        ,
		        "STRUCTURETENSOR", (
					
					(* Compute Gradient *)	
					tensorObj = OrientationScoreTensor3D[os3dObj,{sigmaSpatial, sigmaOrientation},"GRADIENT", opts ];
					
					Block[
						
						{
							A1 = tensorObj[["Data"]][[All,All,All,All,1]],
							A2 = tensorObj[["Data"]][[All,All,All,All,2]],
							A3 = tensorObj[["Data"]][[All,All,All,All,3]],
							A4 = tensorObj[["Data"]][[All,All,All,All,4]],
							A5 = tensorObj[["Data"]][[All,All,All,All,5]],
							A6, tensor,labels
						},
					
			            (* As A6 = 0, we fill the rest with zeros *)
			            A6 = ConstantArray[0, Dimensions[A1]]; 
			        
			            (* Assemble Tensor *)
						tensor = Transpose[
							Developer`ToPackedArray@{
									{A1 A1,	A1 A2,	A1 A3,	A1 A4,	A1 A5,	A6 A1		},
									{A1 A2,	A2 A2,	A2 A3,	A2 A4,	A2 A5,	A6 A2		},
									{A1 A3,	A2 A3,	A3 A3,	A3 A4,	A3 A5,  A6 A3		},
									{A1 A4,	A2 A4,	A3 A4,	A4 A4,	A5 A4,	A6 A4		},
									{A1 A5,	A2 A5, 	A3 A5, 	A5 A4, 	A5 A5,	A6 A5		},
									{A1 A6,	A2 A6,	A3 A6,	A4 A6,	A5 A6,	A6 A6		}
							},{5,6,1,2,3,4}
						];
						
						(* Labels *)
						labels = {
							{"A1*A1", "A2*A1","A3*A1","A4*A1","A5*A1","A6*A1"},
							{"A1*A2", "A2*A2","A3*A2","A4*A2","A5*A2","A6*A2"},
							{"A1*A3", "A2*A3","A3*A3","A4*A3","A5*A3","A6*A3"},
							{"A1*A4", "A2*A4","A3*A4","A4*A4","A5*A4","A6*A4"},
							{"A1*A5", "A2*A5","A3*A5","A4*A5","A5*A5","A6*A5"},
							{"A1*A6", "A2*A6","A3*A6","A4*A6","A5*A6","A6*A6"}
						};
						
						tensorObj = LieAnalysis`Objects`ObjTensor`ObjTensor[{
							"Data" -> tensor,
							"Labels" -> labels,
							"SpatialDimensions"->3,
							"OrientationScore"->os3dObj,
							"MetaData"->Association["Name"->"StructureTensor", "SpatialSigma"->sigmaSpatial, "AngularSigma"->sigmaOrientation]
						}]
						
					]		        	

		        )
    		];
    		
    		(* Store the specified options *)
    		AffixTo[tensorObj,"SpecifiedOptions"->{opts}];
    	
	    	(* Set in output object *)
	    	Return[tensorObj];
	    	
      ];
      
      
      
      OrientationScoreTensor3D[OrientationScores:{__?(ValidQ[#, LieAnalysis`Objects`Obj3DPositionOrientationData`Obj3DPositionOrientationData]&)..}] := Block[
      	
      	(* Local Variable Declaration *)
      	{
  			tensor, tensorObj
      	},
      	
      	(* Merge Orientation Score Objects *)
      	tensor = Transpose[Developer`ToPackedArray[Transpose[#["Data"],{2,3,4,1}]&/@OrientationScores],{5,4,1,2,3}];
      	
      	(* Create ObjTensor *)
      	tensorObj = LieAnalysis`Objects`ObjTensor`ObjTensor[{
			"Data" -> tensor,
			"SpatialDimensions"->3
		}];
      	
      	(* Return Tensor Obj *)
      	Return[tensorObj];
      	
      ];
		
		
	    
	End[];
	
	
EndPackage[];