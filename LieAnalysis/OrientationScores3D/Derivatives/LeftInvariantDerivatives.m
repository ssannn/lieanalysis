(*************************************************************************************************************************
** LeftInvariantDerivativesInternal.m
** This .m file contains functions to compute the left-invariant derivatives in se(3)
**
** Author: F.C. Martin <f.c.martin@tue.nl>
**************************************************************************************************************************)
BeginPackage["LieAnalysis`OrientationScores3D`Derivatives`LeftInvariantDerivatives`",{
  "LieAnalysis`"
}];



  (* TODO: make sure that the function input is the same as for the 2D case *)


  Begin["`Private`"];



    Needs["Classes`"];
    Get["LieAnalysis`Common`"];
    Get["LieAnalysis`Ultilities`"];



    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
    (* LeftInvariantDerivativesInternal- - - - - - - - - - - - - - - - *)
    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
	

	(*todo: symmetry could be maintained in derivativess for fixed alpha *)
    (* For a list of orders to increase speed *)
    
    (* In case the Orientation Score is Complex *)
    LeftInvariantDerivativesInternal[os3dObj_ /; (ValidQ[os3dObj, LieAnalysis`Objects`Obj3DPositionOrientationData`Obj3DPositionOrientationData] && os3dObj["ComplexQ"]), sigmaSpatial_, sigmaOrientation_,
      order_List, method_, returnInterpolationFunctions_:False] := Module[
      
      	{
      		realData = os3dObj["Data","Real"],
      		imData = os3dObj["Data","Imaginary"],
      		reObj, imObj, coObj
      	},
      	
      	(* Create Real Valued Obj3DPositionOrientationData *)
      	reObj = Affix[os3dObj, "Data"->realData];
      	imObj = Affix[os3dObj, "Data"->imData];
      
      	(* Compute Derivatives *)
      	reObj = LeftInvariantDerivativesInternal[reObj, sigmaSpatial, sigmaOrientation,order,method,returnInterpolationFunctions];
      	imObj = LeftInvariantDerivativesInternal[imObj, sigmaSpatial, sigmaOrientation,order,method,returnInterpolationFunctions];
      	
      	(* Stitch Together *)
      	coObj = MapThread[Affix[os3dObj, "Data"->(#1["Data"] + I #2["Data"])]&,{reObj,imObj}];
      	
      	(* Return Complex Derivatives *)
      	Return[coObj];
		
	]
      
    LeftInvariantDerivativesInternal[os3dObj_ /; (ValidQ[os3dObj, LieAnalysis`Objects`Obj3DPositionOrientationData`Obj3DPositionOrientationData] && !os3dObj["ComplexQ"]), sigmaSpatial_, sigmaOrientationIn_,
      order_List, method:"SphericalHarmonics", returnInterpolationFunctions_:False] := Module[
      
      	{
      		data = os3dObj["FullData","Real"],
      		orientations = os3dObj["FullOrientationList"],
      		illegalOrderDerivatives = Intersection[order, {41,42,43,51,52,53}],
      		firstOrderSpatialDerivatives = Intersection[order, {1,2,3}],
      		secondOrderSpatialDerivatives = Intersection[order, {11,12,13,21,22,23,31,32,33}],
      		firstOrderAngularDerivatives = Intersection[order, {4,5}],
      		secondOrderAngularDerivatives = Intersection[order, {44,45,54,55}],
      		mixedOrderDerivatives = Intersection[order, {14,15,24,25,34,35}],
      		frames = {}, labels = {},
      		sigmaOrientation, beta, gamma, lMax, c, A4Derivative, A5Derivative, msg,os3dObjs, cOrientationFirst,cSpatialBlur, cCartesianGradient, cCartesianHessian, indexes, derivatives, printTmp
      	},
      	
      	msg = Row[{ProgressIndicator[0.], " Initializing"}];
      	printTmp = If[OptionValue[LieAnalysis,"DynamicUpdates"], PrintTemporary[Dynamic@msg]];
      	
      	{beta, gamma} = os3dObj["FullPolarOrientationList"];
      	 
		(* Compute Spherical Harmonic Coefficients *)
		msg = Row[{ProgressIndicator[0.10], " Compute Spherical Harmonics Coefficients"}];
		lMax = SphericalHarmonicsLMax[os3dObj["Orientations"]];
	   	c = RealSphericalHarmonicFit[orientations, data,lMax];
	
	   	(* Apply Angular Blur *)
	   	msg = Row[{ProgressIndicator[0.20], " Apply some angular blur"}];
	   	sigmaOrientation = If[sigmaOrientationIn === Automatic,
	   		(0.216667 + 1.06061 os3dObj[["Wavelets"]][["SigmaAngle"]]) (* See #87 *),
	   		sigmaOrientationIn
	   	];
	   	c = SphericalHarmonicsAngularBlur[c, sigmaOrientation];
	   	
	   	(* Compute cartesian spatial derivatives*)
	   	msg = Row[{ProgressIndicator[0.22], " Computing Cartesian Derivatives"}];
	   	cOrientationFirst = Reorder[c,"OrientationFirst"];
      	cSpatialBlur = Reorder[GaussianDerivative[#,{sigmaSpatial, sigmaSpatial, sigmaSpatial},{0,0,0},"Fixed"]&/@cOrientationFirst,"SpatialFirst"];
		cCartesianGradient = Transpose[Table[GaussianDerivative[#, {sigmaSpatial,sigmaSpatial,sigmaSpatial},orderI,"Fixed"]&/@cOrientationFirst, {orderI,IdentityMatrix[3]}],{5,4,1,2,3}];
		cCartesianHessian = Transpose[Table[GaussianDerivative[#, {sigmaSpatial,sigmaSpatial,sigmaSpatial},orderI,"Fixed"]&/@cOrientationFirst, {orderI,{{2,0,0},{1,1,0},{1,0,1},{0,2,0},{0,1,1},{0,0,2}}}],{5,4,1,2,3}];(*only required for second order*)
      	
      	(* Add the required derivatives for the 'illegal' derivatives *)
      	msg = Row[{ProgressIndicator[0.30], " Check if there are illegal derivatives"}];
      	Map[
      		Switch[#,
				41, mixedOrderDerivatives = DeleteDuplicates[Append[mixedOrderDerivatives,14]],
				42, mixedOrderDerivatives = DeleteDuplicates[Append[mixedOrderDerivatives,24]];
					firstOrderSpatialDerivatives = DeleteDuplicates[Append[firstOrderSpatialDerivatives, 3]], (*+*)
				43, mixedOrderDerivatives = DeleteDuplicates[Append[mixedOrderDerivatives,34]];
					firstOrderSpatialDerivatives = DeleteDuplicates[Append[firstOrderSpatialDerivatives, 2]], (*-*)
				51, mixedOrderDerivatives = DeleteDuplicates[Append[mixedOrderDerivatives,15]];
					firstOrderSpatialDerivatives = DeleteDuplicates[Append[firstOrderSpatialDerivatives, 3]], (*-*)
				52, mixedOrderDerivatives = DeleteDuplicates[Append[mixedOrderDerivatives,25]],
				53, mixedOrderDerivatives = DeleteDuplicates[Append[mixedOrderDerivatives,35]];
					firstOrderSpatialDerivatives = DeleteDuplicates[Append[firstOrderSpatialDerivatives, 1]] (*+*)
			]&
      		,illegalOrderDerivatives
      	];
      	
      	(* Check if first spatial order derivatives are required *)
      	msg = Row[{ProgressIndicator[0.40], " Computing first order spatial derivatives (3/8)"}];
      	If[firstOrderSpatialDerivatives =!= {},
      		(
      			AppendTo[frames,SphericalHarmonicsInterpolationFunction[1, cCartesianGradient, 0, lMax, #]];
      			AppendTo[labels,#];
      		)&/@firstOrderSpatialDerivatives;
      	];
      	
      	msg = Row[{ProgressIndicator[0.50], " Computing first order angular derivatives (4/8)"}];
      	If[firstOrderAngularDerivatives =!= {},
      		(
      			AppendTo[frames,SphericalHarmonicsInterpolationFunction[0, cSpatialBlur, #, lMax]];
      			AppendTo[labels,#];
      		)&/@firstOrderAngularDerivatives; 
      	];
      	
      	msg = Row[{ProgressIndicator[0.60], " Computing second order spatial derivatives (5/8)"}];
      	If[secondOrderSpatialDerivatives =!= {},
      		(
      			AppendTo[frames,SphericalHarmonicsInterpolationFunction[2, cCartesianHessian, 0, lMax, IntegerDigits@#]];
      			AppendTo[labels,#];
      		)&/@secondOrderSpatialDerivatives; 
      	];
      	
      	msg = Row[{ProgressIndicator[0.70], " Computing second order angular derivatives (6/8)"}];
      	If[secondOrderAngularDerivatives =!= {},
      		(
      			AppendTo[frames,SphericalHarmonicsInterpolationFunction[0, cSpatialBlur, #, lMax]];
      			AppendTo[labels,#];
      		)&/@secondOrderAngularDerivatives;
      		 
      	];
      	
      	msg = Row[{ProgressIndicator[0.80], " Computing second order mixed derivatives (7/8)"}];
      	If[mixedOrderDerivatives =!= {}, 
      		
      		A4Derivative = Cases[IntegerDigits@mixedOrderDerivatives,{_,4}];
      		A5Derivative = Cases[IntegerDigits@mixedOrderDerivatives,{_,5}];
      		
      		If[A4Derivative =!= {},
      			(
      				AppendTo[frames,SphericalHarmonicsInterpolationFunction[1, cCartesianGradient, 4, lMax, #[[1]]]];
      				AppendTo[labels,FromDigits@#];
      			)&/@A4Derivative;
      		];
      		
      		If[A5Derivative =!= {},
      			(
	      			AppendTo[frames,SphericalHarmonicsInterpolationFunction[1, cCartesianGradient, 5, lMax, #[[1]]]];
	      			AppendTo[labels,FromDigits@#];
      			)&/@A5Derivative;
      		];
      		
      	];
      	
      	(* Compute illegal derivatives by commitators *)
      	msg = Row[{ProgressIndicator[0.90], " Use commutators to solve illegal derivatives"}];
      	Map[
	      	Switch[#,
				41, AppendTo[frames, frames[[Position[labels,14]]]]; AppendTo[labels,41],
				42, AppendTo[frames, frames[[Position[labels,24]]] + frames[[Position[labels,3]]] ]; AppendTo[labels,42], (*+*)
				43, AppendTo[frames, frames[[Position[labels,34]]] - frames[[Position[labels,2]]] ]; AppendTo[labels,43], (*-*)
				51, AppendTo[frames, frames[[Position[labels,15]]] - frames[[Position[labels,3]]] ]; AppendTo[labels,51], (*-*)
				52, AppendTo[frames, frames[[Position[labels,25]]]]; AppendTo[labels,52],
				53, AppendTo[frames, frames[[Position[labels,35]]] + frames[[Position[labels,1]]] ]; AppendTo[labels,53] {35,1}  (*+*)
			]&
			,illegalOrderDerivatives
      	];
			
		(* Compute the output order of the frames *)
		indexes = Flatten[Position[labels,#]&/@order];
		
		msg = Row[{ProgressIndicator[0.99], " Some post processing... almost done (8/8)"}];
		
		If[returnInterpolationFunctions,
			NotebookDelete[printTmp];
			Return[frames[[indexes]]];	
		,
			(* Extract only the required frames and evaluate them for specific betas and gammas *)
			msg = Row[{ProgressIndicator[0.99], " Evaluating specific beta's and gamma's (8/8)"}];
			derivatives = GeneralUtilities`MonitoredMap[#[beta,gamma]&,frames[[indexes]]];	      	
	      	
	      	os3dObjs = Affix[os3dObj, {"Data"->#,"InputData"->os3dObj, "Symmetry"-> None}]&/@derivatives;
	      	
	      	NotebookDelete[printTmp];
	      	Return[os3dObjs];
		]
	      	
	] 
      



    LeftInvariantDerivativesInternal[os3dObj_ /; ValidQ[os3dObj, LieAnalysis`Objects`Obj3DPositionOrientationData`Obj3DPositionOrientationData], sigmaSpatial_, sigmaOrientation_,
      order_Integer (*/; MatchQ[order, 1|2|3|4|5|11|12|13|14|15|21|22|23|24|25|33|31|32|33|34|35|44|45|54|55]*), method:"SphericalHarmonics"] := Module[

      (* Local Variables Declaration *)
      {
     
      },
     
      (* Return the object *)
      Return[First@LeftInvariantDerivativesInternal[os3dObj, sigmaSpatial,sigmaOrientation,{order},method]];

    ];



	(* For the specific A6 Derivative *)
	LeftInvariantDerivativesInternal[os3dObj_ /; ValidQ[os3dObj, LieAnalysis`Objects`Obj3DPositionOrientationData`Obj3DPositionOrientationData], sigmaSpatial_, sigmaOrientation_,
		order_Integer /; MemberQ[IntegerDigits[order], 6 ], method_] := Module[
		
		{
			os3dObjOut
		},
		
		(* Orientation Score Object *)
		os3dObjOut = Affix[os3dObj, {"Data"->ConstantArray[0,Dimensions[os3dObj["FullData"]]], "InputData"->os3dObj,"Symmetry" -> None}];
		
		(* Return the object *)
		Return[os3dObjOut];
		
	];



	LeftInvariantDerivativesInternal[os3dObj_ /; ValidQ[os3dObj, LieAnalysis`Objects`Obj3DPositionOrientationData`Obj3DPositionOrientationData], sigmaSpatial_, sigmaOrientation_,
      order_List, method:"FiniteDifferences"] := LeftInvariantDerivativesInternal[os3dObj, sigmaSpatial, sigmaOrientation, #, "FiniteDifferences"]&/@order;
	

	
	LeftInvariantDerivativesInternal[os3dObj_ /; ValidQ[os3dObj, LieAnalysis`Objects`Obj3DPositionOrientationData`Obj3DPositionOrientationData], sigmaSpatial_, sigmaOrientation_,
      order_Integer, method:"FiniteDifferences"] := Module[

		(* Local Variables Declaration *)
      	{
	        derOrder = IntegerDigits[order],
	        data = os3dObj["FullData","Real"],
	        orientations = os3dObj["FullOrientationList"],
	        topology = os3dObj["FullTopology"],
	       	derivative, os3dObjOut
      	},
      
		(* Compute derivative *)
		derivative = data;
		Do[
       		derivative = GroupDifferences3D[derivative, {orientations, topology}, "A"<>ToString[dir]];
			,{dir, derOrder}
      	];
      
      	(* Set the data-part to the derivative *)
      	os3dObjOut = Affix[os3dObj, {"Data"->derivative, "InputData" -> os3dObj, "Symmetry" -> None}];
      	
      	(* Return the derivative *)
      	Return[os3dObjOut];
      
	]
	
	
	
	(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
    (* SphericalHarmonicsInterpolationFunction - - - - - - - - - *)
    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
    
    
	
	SphericalHarmonicsInterpolationFunction[der:0, c_, shBasis_, lmax_, ___, opts:OptionsPattern[]] := Module[
		
		{
			compiledInterpolator
		},
		
		With[
		
			{
				shExpr = SphericalHarmonicsTable[shBasis, lmax]
			},
	
			compiledInterpolator = Compile[
							
				(* Input Pattern *)
				{ 
					{cList, _Real, 1}, {beta, _Real, 1}, {gamma, _Real, 1} 
				},
				
				(* Return the Derivative *)
				Return[cList.Transpose@shExpr[beta,gamma]]
					
				
				, RuntimeAttributes->Listable, Parallelization -> Automatic 
			];
			
			
			Return[
				Function[{beta,gamma}, compiledInterpolator[c,beta,gamma]] 		
			]
		]
	];

	
	
	
	SphericalHarmonicsInterpolationFunction[der:1, cCartesianGradient_, shBasis_, lmax_, order_:{1,2,3}, opts:OptionsPattern[]] := Module[
		
		{
			compiledInterpolator
		},
		
		With[
		
			{
				shExpr = SphericalHarmonicsTable[shBasis, lmax],
				reorder = If[Head[order] === List, {2,3,1},{2,1}]
			},
		
			(*vb c*)
			compiledInterpolator = Compile[
								
					(* Input Pattern *)
					{
						{cList, _Real, 2}, {beta, _Real, 1}, {gamma, _Real, 1} 
					},
					
					Module[
						
						{R},
						
						R = Transpose[{
							{Cos[beta] Cos[gamma],-Sin[gamma],Cos[gamma] Sin[beta]},
							{Cos[beta] Sin[gamma],Cos[gamma],Sin[beta] Sin[gamma]},
							{-Sin[beta],ConstantArray[0,Length[gamma]],Cos[beta]}
						}][[order]];
						
						(* Return the Derivative *)
						Return[
							MapThread[(#1.Transpose[cList].#2)&,{Transpose[R,reorder],shExpr[beta,gamma]}]
						]
					]
					
					, RuntimeAttributes->Listable, Parallelization -> Automatic(*, CompilationOptions->{"InlineCompiledFunctions"->True,"InlineExternalDefinitions"->True}*) 
			];
		
			Return[
				Function[{beta,gamma}, compiledInterpolator[cCartesianGradient,beta,gamma]] 		
			]
			
		];
		
		
	];
	
	
	
	SphericalHarmonicsInterpolationFunction[der:2, cCartesianHessian_, shBasis_, lmax_, order_:{1,2,3,4,5,6}, opts:OptionsPattern[]] := Module[
		
		{
			selector = order/.{{1,1}->1, {1,2}->2, {1,3}->3, {2,2}->4,{2,3}->5,{3,3}->6},
			compiledInterpolator
		},
		
		With[
			
			{
				shExpr = SphericalHarmonicsTable[shBasis, lmax],
				reorder = If[Head[selector] === List, {2,3,1},{2,1}]
			},
			
			(*vb c*)
			compiledInterpolator = Compile[
								
					(* Input Pattern *)
					{
						{cList, _Real, 2}, {beta, _Real, 1}, {gamma, _Real, 1} 
					},
					
					Module[
						
						{R, R11,R12,R13,R21,R22,R23,R31,R32,R33},
					
						(* Convert to rotation Matrix *)
						{{R11,R12,R13},{R21,R22,R23},{R31,R32,R33}} = {
							{Cos[beta] Cos[gamma],-Sin[gamma],Cos[gamma] Sin[beta]},
							{Cos[beta] Sin[gamma],Cos[gamma],Sin[beta] Sin[gamma]},
							{-Sin[beta],ConstantArray[0,Length[gamma]],Cos[beta]}
						};
						
						(* Create Rotation Vector *)
						R = {
							{R11^2,2 R11 R21,2 R11 R31,R21^2,2 R21 R31,R31^2},
							{R11 R12,R12 R21+R11 R22,R12 R31+R11 R32,R21 R22,R22 R31+R21 R32,R31 R32},
							{R11 R13,R13 R21+R11 R23,R13 R31+R11 R33,R21 R23,R23 R31+R21 R33,R31 R33},
							{R12^2,2 R12 R22,2 R12 R32,R22^2,2 R22 R32,R32^2},
							{R12 R13,R13 R22+R12 R23,R13 R32+R12 R33,R22 R23,R23 R32+R22 R33,R32 R33},
							{R13^2,2 R13 R23,2 R13 R33,R23^2,2 R23 R33,R33^2}
						}[[selector]];
							
						(* Return the Derivative *)
						Return[
							MapThread[(#1.Transpose[cList].#2)&,{Transpose[R,reorder],shExpr[beta,gamma]}]
						]
					]
					
					, RuntimeAttributes->Listable, Parallelization -> Automatic(*, CompilationOptions->{"InlineCompiledFunctions"->True,"InlineExternalDefinitions"->True}*) 
			];
			
			
			Return[
				Function[{beta,gamma}, compiledInterpolator[cCartesianHessian,beta,gamma]] 		
			]
		]
	];
	
	
	
	(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
    (* SpatialDerivatives- - - - - - - - - - - - - - - - - - - - *)
    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
	
	
	
	CartesianSpatialDerivatives[data_, sigma_, der_ /; MatchQ[der, {_Integer,_Integer,_Integer}]] := Module[
	
		{
			osT, derivative
		},
	
		(* Reorder so we can iterate over the orientations *)
		osT = Reorder[data,"OrientationFirst"];
		
		(* Compute the Gaussian derivative at each orientation *)
		derivative = GaussianDerivative[#, {sigma, sigma,sigma},der, "Fixed" ]&/@osT;
		
		(* Reorder to common format *)
		derivative = Reorder[derivative, "SpatialFirst"];
		
		(* Return the derivative *)
		Return[derivative];
		
	];
	
	
	
	CartesianSpatialDerivatives[data_, sigma_, der_ /; MatchQ[der, {{_,_,_}..}]] := Module[
		
		{
			osT, derivative
		},

		(* Reorder so we can iterate over the orientations *)
		osT = Reorder[data,"OrientationFirst"];
		
		(* Compute the Gaussian derivative at each orientation *)
		derivative = Table[GaussianDerivative[#, {sigma, sigma,sigma},dir, "Fixed" ]&/@osT,{dir,der}];
	
		(* Reorder to common format *)
		derivative = Transpose[derivative, {1,5,2,3,4}];

		(* Return the derivative *)
		Return[derivative];
		
	]
	
	
	
	CartesianSpatialDerivatives[data_, sigma_, "Gradient"] := CartesianSpatialDerivatives[data, sigma, {{1,0,0},{0,1,0},{0,0,1}}];
	
	
	
	CartesianSpatialDerivatives[data_, sigma_, "Hessian"] := 
		CartesianSpatialDerivatives[data, sigma, {{2,0,0},{1,1,0},{1,0,1},{0,2,0},{0,1,1},{0,0,2}}];


  End[];

EndPackage[];