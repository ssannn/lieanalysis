(*************************************************************************************************************************
** ExponentialCurveFit.m
** This .m file contains functions to compute the locally adaptive frame (gauge frame) in SE(3)
**
** Author: M.H.J.Janssen <m.h.j.janssen@tue.nl>
** Author: F.C. Martin <f.c.martin@tue.nl>
**************************************************************************************************************************)
BeginPackage["LieAnalysis`OrientationScores3D`GaugeFrames`ExponentialCurveFit`",{
  "LieAnalysis`"
}];


	
	Begin["`Private`"];
	
	
	
		Needs["Classes`"];
		Get["LieAnalysis`Common`"];
		
		
		Options[OrientationScoreGaugeFrames3D] = {
			"TensorType"->"Hessian"
		};
		
		OrientationScoreGaugeFrames3D[ ostObj_ /; ValidQ[ostObj, LieAnalysis`Objects`Obj3DPositionOrientationData`Obj3DPositionOrientationData], {sigmaSpatial_, sigmaOrientationIn_,sigmaSpatialExternal_:0,sigmaAngularExternal_:0}, OptionsPattern[] ] := Module[

			{
				orientationList = ostObj["FullOrientationList"],
				tensorType = OptionValue["TensorType"],
				statusToken,spatialTangent, frame, frameNew, cFinal, gaugeFrame, beta, gamma, Xi, horizontalTangent, interpolationFunctions, betaNew, gammaNew, spatialComponents, horizontalComponents, orientationListSpatialFit, sigmaOrientation, tensorInterpolationFunction
			},
			
			(* Store Original Sampling Scheme of the Sphere *)
			statusToken = StatusBar["Setting Awsome Parameters and pushing buttons"];
			{beta,gamma} = ostObj["FullPolarOrientationList"];
		
			(* The optimal Internal Regularization Scale is related to the SigmaAngle of the CakeWavelets *)
			sigmaOrientation = If[sigmaOrientationIn === Automatic, (*ToDo: this seems Hessian Specific *)
				 (0.216667 + 1.06061 ostObj[["Wavelets"]][["SigmaAngle"]]),
				 sigmaOrientationIn
			];
			
			(* Scaling Between orientation and spatial domain *)
			Xi = sigmaOrientation/sigmaSpatial;
		
			(* Create Left Invariant Interpolation Functions for either the Gradient or Hessian *)
			StatusBar["Preparing Left-Invariant Derivatives for the "<>tensorType];
			interpolationFunctions = CreateInterpolationFunctions[ostObj,sigmaSpatial,sigmaOrientation,tensorType];
			
			(* Prepare Tensor Interpolation Functions for the Product Hessian or Structure Tensor *)
			StatusBar["Preparing Tensor Interpolation Functions"];
			tensorInterpolationFunction = CreateTensorInterpolationFunctions[interpolationFunctions,Xi,{sigmaSpatialExternal,sigmaAngularExternal},orientationList];
			
			(* STEP 1: Determine Spatial Tangent *)
			(* Compute the spatial Components from either the Structure Tensor or Product Hessian *)
			StatusBar[StringTemplate["Computing Spatial Components for all positions and `1` orientations"][Length[beta]]];
			spatialComponents = tensorInterpolationFunction[beta,gamma,orientationList][[1;;6]]; (*6,x,y,z,R*)
			
			(* Eigen System Analyse of the Spatial Tensor *)
			StatusBar["Eigen Value Analysis of the Spatial Components"];
			spatialTangent = LieAnalysis`Common`RealSymmetricThreeEigensystem[Transpose[spatialComponents,{5,1,2,3,4}],"Output"->"EIGENVECTORS"][[All,All,All,All,-1]];
			frame = ConstantArray[LieAnalysis`Common`RnWithAlphaIs0[orientationList],Dimensions[ostObj["Data"],3]];
			orientationListSpatialFit =  MapThread[#1.#2&,{frame,spatialTangent},4];
			
			(* STEP 2: Determine Horizontal Tangent at gNew *)
			{betaNew,gammaNew} =  Transpose[LieAnalysis`Common`CartesianToSphericalCoordinates[orientationListSpatialFit][[All,All,All,All,2;;3]],{2,3,4,5,1}];
			StatusBar[StringTemplate["Computing Horizontal Components for all positions and `1` orientations"][Length[betaNew]]];
			horizontalComponents = tensorInterpolationFunction[betaNew,gammaNew,orientationListSpatialFit][[6;;11]]; (* 6,x,y,z,R *)
			
			(* Eigen System Analysis of the Horizontal Components *)
			StatusBar["Eigen Value Analysis of the Horizontal Components. Hang in there, almost done!"];
			horizontalTangent = LieAnalysis`Common`RealSymmetricThreeEigensystem[Transpose[horizontalComponents,{5,1,2,3,4}],"Output"->"EIGENVECTORS"][[All,All,All,All,-1]];
			horizontalTangent = horizontalTangent.DiagonalMatrix[{1/Xi,1,1}];
			horizontalTangent = Map[Normalize,horizontalTangent,{4}];
			frameNew = LieAnalysis`Common`RnWithAlphaIs0[orientationListSpatialFit];
			
			(* Compute the final tangent coefficients *)
			StatusBar["Constructing Final Coefficients"];
			cFinal = MapThread[LieAnalysis`Common`BlockMatrix[Transpose[#1].#2,Transpose[#1].#2][[All,3;;5]].#3&,{frame,frameNew,horizontalTangent},4];
			
			(* Construct final frame *)
			StatusBar["Finalizing Gauge Frame"];
			gaugeFrame = LieAnalysis`OrientationScores3D`GaugeFrames`ExponentialCurveFit`Private`TangentVectorToGaugeFrame[cFinal, Xi];
			
			(* Return Gauge frame*)
			StatusBarDelete[statusToken];
			Return[gaugeFrame];
					
		];
		
		(* 
		 __           ___                                         __                   
		/   _ _ _ |_ _ | _ _  _ _  _| _ |_ _ _ _  _ | _ |_. _  _ |_    _  _|_. _  _  _ 
		\__| (-(_||_(- |(-| )_)(_)| || )|_(-| |_)(_)|(_||_|(_)| )| |_|| )(_|_|(_)| )_) 
		                                      |                                        
		*)
		
		CreateTensorInterpolationFunctions[{A1f_,A2f_,A3f_,A4f_,A5f_},Xi_,{0,0},___] := Function[{beta,gamma,coords},{
			(*
				Structure Tensor Without External Regularization 
				If no external regularization is applied we do not have to compute all components, just the spatial and horizontal 
				components. A1A1, A1A2, A1A3, A2A2, A2A3, A3A3A, A3A4, A3A5, A4A4, A4A5, A5A5
			*)
			(A1f[beta,gamma]^2/Xi^2),
			(A1f[beta,gamma] A2f[beta,gamma]/Xi^2),
			(A1f[beta,gamma] A3f[beta,gamma]/Xi^2),
			(A2f[beta,gamma]^2/Xi^2),
			(A2f[beta,gamma] A3f[beta,gamma]/Xi^2),
			(A3f[beta,gamma]^2/Xi^2),
			(A3f[beta,gamma] A4f[beta,gamma]/Xi),
			(A3f[beta,gamma] A5f[beta,gamma]/Xi),
			(A4f[beta,gamma]^2),
			(A4f[beta,gamma] A5f[beta,gamma]),
			(A5f[beta,gamma]^2)
		}];
		
		CreateTensorInterpolationFunctions[{A1f_,A2f_,A3f_,A4f_,A5f_,A11f_,A12f_,A13f_,A14f_,A15f_,A22f_,A23f_,A24f_,A25f_,A33f_,A34f_,A35f_,A44f_,A45f_,A55f_},Xi_,{0,0},___] := Function[{beta,gamma,coords},{
			(*	
				If no external regularization is applied we do not have to compute all components, just the spatial and horizontal 
				components. A1A1, A1A2, A1A3, A2A2, A2A3, A3A3A, A3A4, A3A5, A4A4, A4A5, A5A5
			*)
			(*11*)(A11f[beta,gamma]^2/Xi^4 				+ A12f[beta,gamma]^2/Xi^4 + A13f[beta,gamma]^2/Xi^4 + A14f[beta,gamma]^2/Xi^2 + A15f[beta,gamma]^2/Xi^2), (*B11^2+B12^2+B13^2+B14^2+B15^2*)
			(*12*)((A11f[beta,gamma] A12f[beta,gamma])/Xi^4 	+ (A12f[beta,gamma] A22f[beta,gamma])/Xi^4 + (A13f[beta,gamma] A23f[beta,gamma])/Xi^4 + (A14f[beta,gamma] A24f[beta,gamma])/Xi^2 + (A15f[beta,gamma] A25f[beta,gamma])/Xi^2), (* B11 B21+B12 B22+B13 B23+B14 B24+B15 B25 *)
			(*13*)((A11f[beta,gamma] A13f[beta,gamma])/Xi^4 	+ (A12f[beta,gamma] A23f[beta,gamma])/Xi^4 + (A13f[beta,gamma] A33f[beta,gamma])/Xi^4 + (A14f[beta,gamma] A34f[beta,gamma])/Xi^2 + (A15f[beta,gamma] A35f[beta,gamma])/Xi^2), (*B11 B31+B12 B32+B13 B33+B14 B34+B15 B35*)
			(*22*)(A12f[beta,gamma]^2/Xi^4 				+ A22f[beta,gamma]^2/Xi^4 + A23f[beta,gamma]^2/Xi^4 +A24f[beta,gamma]^2/Xi^2 +A25f[beta,gamma]^2/Xi^2), (*B21^2+B22^2+B23^2+B24^2+B25^2*)
			(*23*)((A12f[beta,gamma] A13f[beta,gamma])/Xi^4 	+ (A22f[beta,gamma] A23f[beta,gamma])/Xi^4 + (A23f[beta,gamma] A33f[beta,gamma])/Xi^4 + (A24f[beta,gamma] A34f[beta,gamma])/Xi^2 + (A25f[beta,gamma] A35f[beta,gamma])/Xi^2), (*B21 B31+B22 B32+B23 B33+B24 B34+B25 B35*)
			(*33*)(A13f[beta,gamma]^2/Xi^4				+	A23f[beta,gamma]^2/Xi^4			+	A33f[beta,gamma]^2/Xi^4			+	A34f[beta,gamma]^2/Xi^2				+	A35f[beta,gamma]^2/Xi^2),
			(*34*)((A13f[beta,gamma] A14f[beta,gamma])/Xi^3	+	(A23f[beta,gamma] (A24f[beta,gamma]+A3f[beta,gamma]))/Xi^3	+	(A33f[beta,gamma] (-A2f[beta,gamma]+A34f[beta,gamma]))/Xi^3+	(A34f[beta,gamma] A44f[beta,gamma])/Xi			+	(A35f[beta,gamma] A45f[beta,gamma])/Xi),
			(*35*)((A23f[beta,gamma] A25f[beta,gamma])/Xi^3	+	(A13f[beta,gamma] (A15f[beta,gamma]-A3f[beta,gamma]))/Xi^3	+	(A33f[beta,gamma] (A1f[beta,gamma]+A35f[beta,gamma]))/Xi^3	+	(A34f[beta,gamma] A45f[beta,gamma])/Xi			+	(A35f[beta,gamma] A55f[beta,gamma])/Xi),
			(*44*)(A44f[beta,gamma]^2					+	A45f[beta,gamma]^2				+	A14f[beta,gamma]^2/Xi^2			+	(A24f[beta,gamma]+A3f[beta,gamma])^2/Xi^2			+	(-A2f[beta,gamma]+A34f[beta,gamma])^2/Xi^2),
			(*45*)(A44f[beta,gamma] A45f[beta,gamma]			+	A45f[beta,gamma] A55f[beta,gamma]				+	(A14f[beta,gamma] (A15f[beta,gamma]-A3f[beta,gamma]))/Xi^2	+	(A25f[beta,gamma] (A24f[beta,gamma]+A3f[beta,gamma]))/Xi^2		+	((-A2f[beta,gamma]+A34f[beta,gamma]) (A1f[beta,gamma]+A35f[beta,gamma]))/Xi^2),
			(*55*)(A45f[beta,gamma]^2					+	A55f[beta,gamma]^2								+	A25f[beta,gamma]^2/Xi^2			+	(A15f[beta,gamma]-A3f[beta,gamma])^2/Xi^2			+	(A1f[beta,gamma]+A35f[beta,gamma])^2/Xi^2)
		}];
		
		CreateTensorInterpolationFunctions[interpolationFunctions_,Xi_,{sigmaSpatialExternal_,sigmaAngularExternal_},orientationList_] := Module[
			(*
				For both the Hessian and Structure Tensor Including External Regularization	
				Because of external regularization we need to sample the interpolation functions first.
				In order to do so we need to super sample the sphere as we are multiplying 
				L-order polynomials and we do not want to lose too much precision 
			*)
		
			{
				sigmas = ConstantArray[sigmaSpatialExternal,3],
				betaS, gammaS,sampledTensor, shCoefficients, lMax, shCoefficientsBlurred, interpolationFunction,cartesianInterpolationFunction,
				cartT,sampledTensorSpatBlur,superSampleCoordinates,symmetricMatrixTensor,cartSymmetricMatrixTensor,cartComponents
			},
			
			StatusBar["Super sample the spherical harmonics expressions in order to construct the Tensor Interpolation Functions"];
			superSampleCoordinates = LieAnalysis`OrientationScores3D`Filters`ElectrostaticRepulsion`SphereSampleCoordinates[Length[orientationList]];
			superSampleCoordinates = Join[superSampleCoordinates,-superSampleCoordinates];
			
			(*Express Coordinates on Sphere in Polar Representation *)
			{betaS,gammaS} = Transpose[LieAnalysis`Common`CartesianToSphericalCoordinates[superSampleCoordinates][[All,2;;3]]];
			
			sampledTensor = ConstructTensorComponentsFromInterplationFunctions[interpolationFunctions,betaS,gammaS,Xi];
			
			StatusBar["Apply Regularization of the Spatial and Angular componentes"];
			(* Apply Spatial Regularization *)
			sampledTensorSpatBlur = Developer`ToPackedArray@Map[
				GaussianFilter[#,{4 sigmas+1, sigmas}]&,
				Transpose[sampledTensor,{1,3,4,5,2}],{2}
			]; (* 15,R,x,y,z *)
			
			(* 
				Rotate towards Cartesian Frame, this is needed because the A-frame axes are orientation dependant making the meaning 
				of the component orientation dependant on the current orientation which we do not want. (Thus (x1, Rn1) can be smoothed with (x1,Rn2) as
				the A-frame has a different orientation.
			*)
			symmetricMatrixTensor = ToSymmetricMatrix[sampledTensorSpatBlur]; (* 6,6,R,x,y,z *)
			cartSymmetricMatrixTensor = LieAnalysis`Common`FromLeftInvariantFrame[superSampleCoordinates,Transpose[symmetricMatrixTensor,{5,6,4,1,2,3}]];
			cartComponents = FromSymmetricMatrix[Transpose[cartSymmetricMatrixTensor,{4,5,6,3,1,2}]]; (* 21,R,x,y,z *)
			
			(* Spheric Harmonic Interpolation *)
			lMax = LieAnalysis`Common`SphericalHarmonicsLMax[Length[superSampleCoordinates]];
			shCoefficients = Developer`ToPackedArray[
				Table[
					LieAnalysis`Common`RealSphericalHarmonicFit[superSampleCoordinates,Transpose[cartComponents[[i]],{4,1,2,3}],lMax] 
					,
					{i,1,Length[cartComponents]}
				]
			];
			
			(* Apply Angular Regularization *)
			shCoefficientsBlurred = LieAnalysis`Common`SphericalHarmonicsAngularBlur[shCoefficients,sigmaAngularExternal]; (* 15,x,y,z,C *)
			
			(* Create Interpolation Functions *)
			cartesianInterpolationFunction = SphericalHarmonicsInterpolationFunction[Transpose[shCoefficientsBlurred,{5,1,2,3,4}],lMax];
			
			StatusBar["Finalizing Tensor Interpolation Functions"];
			(* Create Left Invariant Interpolation Function *)
			interpolationFunction = Function[{beta,gamma,coords},
				Module[{cartBlurredSymmetricMatrixTensor,leftSymmetricMatrixTensor,components},
					cartT = Transpose[cartesianInterpolationFunction[beta,gamma],{2,3,4,5,1}]; (* 15,x,y,z,R *)
					cartBlurredSymmetricMatrixTensor = ToSymmetricMatrix[cartT]; (* 6,6,x,y,z,R *)
					leftSymmetricMatrixTensor = ToLeftInvariantFrame[coords,Transpose[cartBlurredSymmetricMatrixTensor,{5,6,1,2,3,4}]];
					components = Extract[FromSymmetricMatrix[Transpose[leftSymmetricMatrixTensor,{3,4,5,6,1,2}]],{{1},{2},{3},{7},{8},{12},{13},{14},{16},{17},{19}}]; (* 11,R,x,y,z *)
					components
				]
			];
			
			(* Return Tensor Interpolation Functions *)
			Return[interpolationFunction];
		
		];
		
		(* 			
		 __                                                                      __                 
		(_  _ |_  _ _. _ _ ||__| _  _ _  _  _ . _ _| _ |_ _ _ _  _ | _ |_. _  _ |_    _  _|_. _  _  
		__)|_)| )(-| |(_(_|||  |(_|| |||(_)| )|(__)|| )|_(-| |_)(_)|(_||_|(_)| )| |_|| )(_|_|(_)| ) 
		   |                                                 |                                      
		*)
		
		SphericalHarmonicsInterpolationFunction[cCartesianComponents_, lmax_] := Module[
		
			{
				compiledInterpolator
			},
			
			With[
				
				{
					shExpr = SphericalHarmonicsTable[0, lmax]
				},
				
				(* Interpolation functions for all positions and all orientations *)
				compiledInterpolator = Compile[
									
						(* Input Pattern *)
						{
							{cList, _Real, 2}, {beta, _Real, 1}, {gamma, _Real, 1} 
						},
						
						Return[Map[Transpose[cList].#&,shExpr[beta,gamma]]]
						
						, RuntimeAttributes->{Listable}, Parallelization -> False, CompilationOptions->{"InlineCompiledFunctions"->False,"InlineExternalDefinitions"->False} 
				];
				
				(* Wrapper for the coefficients *)
				Return[
					Function[{beta,gamma}, compiledInterpolator[cCartesianComponents,beta,gamma]]
				]
			]
		];
		
		(*
		 __                                         __                   
		/   _ _ _ |_ _| _ |_ _ _ _  _ | _ |_. _  _ |_    _  _|_. _  _  _ 
		\__| (-(_||_(-|| )|_(-| |_)(_)|(_||_|(_)| )| |_|| )(_|_|(_)| )_) 
		                        |                                        
		*)
		CreateInterpolationFunctions[ostObj_,sigmaSpatial_,sigmaOrientation_,"StructureTensor"] := 
			LieAnalysis`OrientationScores3D`Derivatives`LeftInvariantDerivatives`Private`LeftInvariantDerivativesInternal[
				ostObj, sigmaSpatial, sigmaOrientation,{1,2,3,4,5},"SphericalHarmonics",True];
		
		CreateInterpolationFunctions[ostObj_,sigmaSpatial_,sigmaOrientation_,"Hessian"] := 
			LieAnalysis`OrientationScores3D`Derivatives`LeftInvariantDerivatives`Private`LeftInvariantDerivativesInternal[
				ostObj, sigmaSpatial, sigmaOrientation,{1,2,3,4,5,11,12,13,14,15,22,23,24,25,33,34,35,44,45,55},"SphericalHarmonics",True];	
		
		(*
		 __                   ___            __                         __                                 __                   
		/   _  _  _|_ _    _|_ | _ _  _ _  _/   _  _  _  _  _  _ _ |_ _|__ _  _ | _ |_ _ _ _ | _ |_. _  _ |_    _  _|_. _  _  _ 
		\__(_)| )_)|_| |_|(_|_ |(-| )_)(_)| \__(_)||||_)(_)| )(-| )|__)|| (_)||||| )|_(-| |_)|(_||_|(_)| )| |_|| )(_|_|(_)| )_) 
		                                             |                                    |                                     
		*)
		ConstructTensorComponentsFromInterplationFunctions[{A1f_,A2f_,A3f_,A4f_,A5f_},beta_,gamma_,Xi_] := Module[

			{A1,A2,A3,A4,A5},
			
			(* Compute Gradient *)
			{A1,A2,A3} = (#[beta,gamma])&/@{A1f,A2f,A3f};
			{A4,A5} = #[beta,gamma]&/@{A4f,A5f};
			
			(* Sample and Construct All Tensor Components *)
			Return[{
				A1^2/Xi^2, (A1 A2)/Xi^2, (A1 A3)/Xi^2, (A1 A4)/Xi, (A1 A5)/Xi,
					          A2^2/Xi^2, (A2 A3)/Xi^2, (A2 A4)/Xi, (A2 A5)/Xi,
					  		              A3^2/Xi^2, (A3 A4)/Xi, (A3 A5)/Xi,
					  		                               A4^5,      A4 A5,
					  		 			                              A5^2
			}];
			
		];
		
		ConstructTensorComponentsFromInterplationFunctions[{A1f_,A2f_,A3f_,A4f_,A5f_,A11f_,A12f_,A13f_,A14f_,A15f_,A22f_,A23f_,A24f_,A25f_,A33f_,A34f_,A35f_,A44f_,A45f_,A55f_},beta_,gamma_,Xi_] := Module[
		
			{A1,A2,A3,A4,A5,A11,A12,A13,A14,A15,A22,A23,A24,A25,A33,A34,A35,A44,A45,A55},
			
			(* Evaluate at Specific Orientations *)
			{A1,A2,A3,A4,A5,A11,A12,A13,A14,A15,A22,A23,A24,A25,A33,A34,A35,A44,A45,A55} 
				= #[beta,gamma]&/@{A1f,A2f,A3f,A4f,A5f,A11f,A12f,A13f,A14f,A15f,A22f,A23f,A24f,A25f,A33f,A34f,A35f,A44f,A45f,A55f};
			
			Return[{
				(*11*)A11^2/Xi^4 + A12^2/Xi^4 + A13^2/Xi^4 + A14^2/Xi^2 + A15^2/Xi^2, (*B11^2+B12^2+B13^2+B14^2+B15^2*)
				(*12*)(A11 A12)/Xi^4 + (A12 A22)/Xi^4 + (A13 A23)/Xi^4 + (A14 A24)/Xi^2 + (A15 A25)/Xi^2, (* B11 B21+B12 B22+B13 B23+B14 B24+B15 B25 *)
				(*13*)(A11 A13)/Xi^4 + (A12 A23)/Xi^4 + (A13 A33)/Xi^4 + (A14 A34)/Xi^2 + (A15 A35)/Xi^2, (*B11 B31+B12 B32+B13 B33+B14 B34+B15 B35*)
				(*14*)(A11 A14)/Xi^3 + (A12 A24)/Xi^3 + (A13(-A2+A34))/Xi^3 + (A14 A44)/Xi + (A15 A45)/Xi, (*B11 B14+B12 B24+B13 (-A2+B34)+B14 B44+B15 B45*)
				(*15*)(A11 A15)/Xi^3 + (A12 A25)/Xi^3 + (A13 (A1+A35))/Xi^3 + (A14 A45)/Xi + (A15 A55)/Xi, (*B11 B15+B12 B25+B13 (A1+B35)+B14 B54+B15 B55*)
				(*16*)(-A1 A12)/Xi^3 + (A11 A2)/Xi^3 - (A15 A4)/Xi + (A14 A5)/Xi,(*-B1 B12+B11 B2-B15 B4+B14 B5*)
				(*22*) A12^2/Xi^4 + A22^2/Xi^4 + A23^2/Xi^4 +A24^2/Xi^2 +A25^2/Xi^2, (*B21^2+B22^2+B23^2+B24^2+B25^2*)
				(*23*) (A12 A13)/Xi^4 + (A22 A23)/Xi^4 + (A23 A33)/Xi^4 + (A24 A34)/Xi^2 + (A25 A35)/Xi^2, (*B21 B31+B22 B32+B23 B33+B24 B34+B25 B35*)
				(*24*) (A14 A12)/Xi^3 + (A22 A24)/Xi^3 + (A23 (-A2+A34))/Xi^3 + (A24 A44)/Xi + (A25 A45)/Xi, (*B14 B21+B22 B24+B23 (-A2+B34)+B24 B44+B25 B45*)
				(*25*) (A15 A12)/Xi^3 + (A22 A25)/Xi^3 + (A23 (A1+A35))/Xi^3 + (A24 A45)/Xi + (A25 A55)/Xi, (*B15 B21+B22 B25+B23 (B1+B35)+B24 B54+B25 B55*)
				(*26*) (A2 A12)/Xi^3 - (A1 A22)/Xi^3 - (A25 A4)/Xi + (A24 A5)/Xi, (*B2 B21-B1 B22-B25 B4+B24 B5*)
				(*33*)A13^2/Xi^4		+	A23^2/Xi^4			+	A33^2/Xi^4			+	A34^2/Xi^2				+	A35^2/Xi^2,
				(*34*)(A13 A14)/Xi^3	+	(A23 (A24+A3))/Xi^3	+	(A33 (-A2+A34))/Xi^3+	(A34 A44)/Xi			+	(A35 A45)/Xi,
				(*35*)(A23 A25)/Xi^3	+	(A13 (A15-A3))/Xi^3	+	(A33 (A1+A35))/Xi^3	+	(A34 A45)/Xi			+	(A35 A55)/Xi,
				(*36*) (A2 A13)/Xi^3 - (A1 A23)/Xi^3 - (A35 A4)/Xi + (A34 A5)/Xi,
				(*44*)A44^2			+	A45^2				+	A14^2/Xi^2			+	(A24+A3)^2/Xi^2			+	(-A2+A34)^2/Xi^2,
				(*45*)A44 A45			+	A45 A55				+	(A14 (A15-A3))/Xi^2	+	(A25 (A24+A3))/Xi^2		+	((-A2+A34) (A1+A35))/Xi^2,
				(*46*) (A14 A2)/Xi^2 - (A1 A24)/Xi^2 - (A4 A45) + (A44 A5),
				(*55*)A45^2			+	A55^2				+	A25^2/Xi^2			+	(A15-A3)^2/Xi^2			+	(A1+A35)^2/Xi^2,
				(*56*) (A15 A2)/Xi^2 - (A1 A25)/Xi^2 + (A5 A45) - (A4 A55),
				(*66*) A1^2/Xi^2 + A2^2/Xi^2 + A4^2 + A5^2 (*B1^2+B2^2+B4^2+B5^2*)
			}];
		];


		
		
		(*
		                 __                   
		|__| _| _  _ _  |_    _  _|_. _  _  _ 
		|  |(-||_)(-|   | |_|| )(_|_|(_)| )_) 
		       |                              
		*)
		ToSymmetricMatrix[c_ /; Length[c] == 15] := 
			ToSymmetricMatrix[With[{zero = ConstantArray[0,Dimensions[c[[1]]]]},Join[c[[1;;5]],{zero},c[[6;;9]],{zero},c[[10;;12]],{zero},c[[13;;14]],{zero},{c[[15]]},{zero,zero}]]];
		
		ToSymmetricMatrix[c_ /; Length[c] == 21] := {
			{c[[1]],c[[2]],c[[3]],c[[4]],c[[5]],c[[6]]},
			{c[[2]],c[[7]],c[[8]],c[[9]],c[[10]],c[[11]]},
			{c[[3]],c[[8]],c[[12]],c[[13]],c[[14]],c[[15]]},
			{c[[4]],c[[9]],c[[13]],c[[16]],c[[17]],c[[18]]},
			{c[[5]],c[[10]],c[[14]],c[[17]],c[[19]],c[[20]]},
			{c[[6]],c[[11]],c[[15]],c[[18]],c[[20]],c[[21]]}
		};
		
		FromSymmetricMatrix[c_ /; Dimensions[c,2]=={6,6}] :=
			{c[[1,1]],c[[1,2]],c[[1,3]],c[[1,4]],c[[1,5]],c[[1,6]],c[[2,2]],c[[2,3]],c[[2,4]],c[[2,5]],c[[2,6]],c[[3,3]],c[[3,4]],c[[3,5]],c[[3,6]],c[[4,4]],c[[4,5]],c[[4,6]],c[[5,5]],c[[5,6]],c[[6,6]]};
			
			
		(*			
		___                             ___                 __         
		 | _  _  _  _ _ |_\  /_ _|_ _  _ | _  _  _     _  _|__ _  _  _ 
		 |(_|| )(_)(-| )|_ \/(-(_|_(_)|  |(_)(_)(_||_|(_)(-|| (_||||(- 
		        _/                           _/       _/               
		*)
	
		TangentVectorToGaugeFrame[c_] := TangentVectorToGaugeFrameC[c,1];
		
		
		TangentVectorToGaugeFrame[c_,mu_] := TangentVectorToGaugeFrameC[c,mu];
		
		TangentVectorToGaugeFrameR1C=Compile[{{v,_Real,1}},
			Module[{eps=10^-8,v1=v[[1]],v2=v[[2]],v3=v[[3]],c},
				c=v1^2+v2^2;
				If[c>eps,
					{{(v2^2+v1^2 v3)/c,(v1 v2 (-1+v3))/c,v1},{(v1 v2 (-1+v3))/c,(v1^2+v2^2 v3)/c,v2},{-v1,-v2,v3}},
					{{1.`,0.`,0.`},{0.`,1.`,0.`},{0.`,0.`,1.`}}
				]
			] 
		];
	
		
		TangentVectorToGaugeFrameR2C=Compile[{{v,_Real,1}},
			Module[{eps=10^-8,v1=v[[1]],v2=v[[2]],v3=v[[3]],v4=v[[4]],c},
				c=v2^2+v3^2+v4^2;
				If[c>eps ,
					{
						{v1,-v2,-v3,-v4},
						{v2,(v1 v2^2+v3^2+v4^2)/c,((-1+v1) v2 v3)/c,((-1+v1) v2 v4)/c},
						{v3,((-1+v1) v2 v3)/c,(v2^2+v1 v3^2+v4^2)/c,((-1+v1) v3 v4)/c},
						{v4,((-1+v1) v2 v4)/c,((-1+v1) v3 v4)/c,(v2^2+v3^2+v1 v4^2)/c}
					},{
						{1.`,0.`,0.`,0.`}
						,{0.`,1.`,0.`,0.`}
						,{0.`,0.`,1.`,0.`}
						,{0.`,0.`,0.`,1.`}
					}
				]
			]
		];
		
		
		
		TangentVectorToGaugeFrameC=Compile[{{c,_Real,1},{mu,_Real}},
			Module[{Rs,Ra,MmuI,Mmu,ctilde},
				Ra=Rs={{1.`,0.`,0.`,0.`,0.`,0.`},{0.`,1.`,0.`,0.`,0.`,0.`},{0.`,0.`,1.`,0.`,0.`,0.`},{0.`,0.`,0.`,1.`,0.`,0.`},{0.`,0.`,0.`,0.`,1.`,0.`},{0.`,0.`,0.`,0.`,0.`,1.`}};
				MmuI={{1/mu,0.`,0.`,0.`,0.`,0.`},{0.`,1/mu,0.`,0.`,0.`,0.`},{0.`,0.`,1/mu,0.`,0.`,0.`},{0.`,0.`,0.`,1.`,0.`,0.`},{0.`,0.`,0.`,0.`,1.`,0.`},{0.`,0.`,0.`,0.`,0.`,1.`}};
				Mmu={{mu,0.`,0.`,0.`,0.`,0.`},{0.`,mu,0.`,0.`,0.`,0.`},{0.`,0.`,mu,0.`,0.`,0.`},{0.`,0.`,0.`,1.`,0.`,0.`},{0.`,0.`,0.`,0.`,1.`,0.`},{0.`,0.`,0.`,0.`,0.`,1.`}};
				ctilde=Mmu.c;
				Rs[[1;;3,1;;3]]=TangentVectorToGaugeFrameR1C[ctilde[[1;;3]]/Norm[ctilde[[1;;3]]]];
				Ra[[3;;6,3;;6]]=TangentVectorToGaugeFrameR2C[{Norm[ctilde[[1;;3]]],ctilde[[4]],ctilde[[5]],ctilde[[6]]}/Norm[ctilde[[1;;6]]]];
		
				(*mu^ is added when we want to follow same convention as in Franken where mu-orthogonality is defined as all vectors having norm equal to \[Mu]*)
				(*mu*) Transpose[Rs.Ra].MmuI
			],
			RuntimeAttributes-> Listable,CompilationOptions -> {"InlineExternalDefinitions" -> True, "InlineCompiledFunctions" -> True}
		];
	
	
	
	End[];



EndPackage[];