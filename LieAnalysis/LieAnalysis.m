(*************************************************************************************************************************
** LieAnalysis.m
** This .m file contains the the interface with the package
**
** Author: F.C. Martin <f.c.martin@tue.nl>
**************************************************************************************************************************)
BeginPackage["LieAnalysis`"];



	(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
	(* Other - - - - - - - - - - - - - - - - - - - - - - - - - - *)
	(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)



	LieAnalysisData::usage = "
	OrientationScoreData[] returns the available example data types, 
	OrientationScoreData[type] returns the available example data for a specific type, 
	and OrientationScoreData[{type,name}] return the specified example data.
	Note that an internet connection is required for loading any set for the first time.
	";
	Macros`SetArgumentCount[LieAnalysisData, {0,1}];
	LieAnalysisData = LieAnalysis`ExampleData`LieAnalysisData`Private`LieAnalysisData;
	

	
	LieAnalysis::usage = "Symbol to modify package wide options and some upvalues are attached";
	LieAnalysis = LieAnalysis`Ultilities`Private`LieAnalysis;


	StatusBar::usage = "
	StatusBar[message_] Prints a temporary message (Like PrintTemporary) containing the computation time and a message.
	It is also possible to update the message without printing it to a new cell, making it possible to give updates from 
	child functions in the same update field. Note that the messages only will be shown if OptionValue[LieAnalysis,\"DynamicUpdates\"]
	is set to true.
	";
	StatusBar = LieAnalysis`Ultilities`Private`StatusBar;
	
	StatusBarDelete::usage = "
	StatusBarDelete[] Deletes the StatusBar, which you should always do if you do not need the status updates anymore.
	"
	StatusBarDelete = LieAnalysis`Ultilities`Private`StatusBarDelete;
	
	(*DONOTINCLUDEINEMBEDDEDVERSION*)
	EmbedLieAnalysisPackage::usage = "EmbedLieAnalysisPackage[], Embed the entire package into the notebook.";
	Macros`SetArgumentCount[EmbedLieAnalysisPackage, 0];
	EmbedLieAnalysisPackage = LieAnalysis`Ultilities`Private`EmbedLieAnalysisPackage;
	(*ENDEXCLUSIONFROMEMBEDDEDVERSION*)
	
	
	
	$LieAnalysisVersion::usage = "Returns the version number (major.minor.release) of the LieAnalysis Package.";
	$LieAnalysisVersion = LieAnalysis`Ultilities`Private`$LieAnalysisVersion;



	(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
	(* OrientationScores2D - - - - - - - - - - - - - - - - - - - *)
	(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)



	GaborWaveletStack::usage = "
	GaborWaveletStack[n_, scale_, size_, OptionsPattern_] returns a set of 'n' rotated Gabor wavelets at scale, with standard or specified paramters.
	";
	SyntaxInformation[GaborWaveletStack] = {"ArgumentsPattern" -> {_, _, OptionsPattern[]}};
	Macros`SetArgumentCount[GaborWaveletStack, {1, Infinity}];
	GaborWaveletStack = LieAnalysis`OrientationScores2D`Filters`GaborWavelets`Private`GaborWaveletStack;



	CakeWaveletStack::usage = "
	CakeWaveletStack[n_, size_, OptionsPattern_] creates a 2D Cake Wavelet Stack with 'n' orientations and using the default or specified parameters.
	";
	SyntaxInformation[CakeWaveletStack] = {"ArgumentsPattern" -> {_,_, OptionsPattern[]}};
	Macros`SetArgumentCount[CakeWaveletStack, {2, Infinity}];
	CakeWaveletStack = LieAnalysis`OrientationScores2D`Filters`CakeWavelets`Private`CakeWaveletStack;



	MultiScaleCakeWaveletStack::usage = "
	MultiCakeWaveletStack[n_, size_, {rhoMin_, rhoMax_, nScales_}, OptionsPattern[]] creates a Multi-Scale Cake Wavelet Stack with 'n' orientations and using the default or specified parameters for the processing of 2D images.
	";
	SyntaxInformation[MultiScaleCakeWaveletStack] = {"ArgumentsPattern" -> {_,_, {_,_,_},OptionsPattern[]}};
	Macros`SetArgumentCount[MultiScaleCakeWaveletStack, {3, Infinity}];
	MultiScaleCakeWaveletStack = LieAnalysis`OrientationScores2D`Filters`MultiScaleCakeWaveletStack`Private`MultiScaleCakeWaveletStack;



	OrientationScoreTransform::usage = "
	OrientationScoreTransform[image, OptionsPattern_] constructs an Orientation Score from the image using Cake Wavelets with default or suplied paramters.
	OrientationScoreTransform[image, ObjOrientationWavelet_] constructs an Orientation Score from the image using the ObjOrientationWavelet.
	";
	SyntaxInformation[OrientationScoreTransform] = {"ArgumentsPattern"-> {_, _., OptionsPattern[]}};
	Macros`SetArgumentCount[OrientationScoreTransform, {1, Infinity}];
	OrientationScoreTransform = LieAnalysis`OrientationScores2D`Construction`Private`OrientationScoreTransform;



	InverseOrientationScoreTransform::usage = "
	InverseOrientationScoreTransform[ObjPositionOrientationData_, OptionsPattern_] performs reconstruction of the data from an Orientation Score 2D by (default) Summation, L2 or Exact.
	";
	SyntaxInformation[InverseOrientationScoreTransform] = {"ArgumentsPattern"-> {_, OptionsPattern[]}};
	Macros`SetArgumentCount[InverseOrientationScoreTransform, {1, Infinity}];
	InverseOrientationScoreTransform = LieAnalysis`OrientationScores2D`Reconstruction`Private`InverseOrientationScoreTransform;



	LeftInvariantDerivatives::usage = "
	LeftInvariantDerivatives[ObjPositionOrientationData_, {sigmaSpatial_, sigmaOrientation_}, derivativeIndex_, OptionsPattern_] computes the derivatives of the SE(2) space. By default the cartesian derivatives are returned.
	";
	SyntaxInformation[LeftInvariantDerivatives] = {"ArgumentsPattern"-> {_,_,_.,OptionsPattern[]}};
	Macros`SetArgumentCount[LeftInvariantDerivatives, {3, Infinity}];
	LeftInvariantDerivatives = LieAnalysis`OrientationScores2D`OrientationScoreDerivatives`Private`LeftInvariantDerivatives;



	OrientationScoreTensor::usage = "
	OrientationScoreTensor[ObjPositionOrientationData_, {sigmaSpatial_, sigmaOrientation_}, derivativeIndex_, OptionsPattern_] computes the tensor of the SE(2) space.
	";
	LeftInvariantDerivatives3D = LieAnalysis`OrientationScores2D`OrientationScoreTensor`Private`OrientationScoreTensor;



	OrientationScoreGaugeFrames::usage ="
	OrientationScoreGaugeFrames[objTensor_, opts:OptionsPattern[]]
	OrientationScoreGaugeFrames[objOst_, {sigmaOrientation_, sigmaSpatial_}, opts:OptionsPattern[]]
	";
	SyntaxInformation[OrientationScoreGaugeFrames] = {"ArgumentsPattern"->{_,_.,OptionsPattern[]}};
	OrientationScoreGaugeFrames = LieAnalysis`OrientationScores2D`GaugeFrames`ExponentialCurveFitting`Private`OrientationScoreGaugeFrames;
	
	

	(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
	(* OrientationScores3D - - - - - - - - - - - - - - - - - - - *)
	(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)



	CakeWaveletStack3D::usage = "
	CakeWaveletStack3D[n, size_, OptionsPattern_] constructs a 3D cakewavelet stack with 'n' orientations with dimensions sizexsizexsize and using default parameters.
	";
	SyntaxInformation[CakeWaveletStack3D] = {"ArgumentsPattern"-> {_, _,OptionsPattern[]}};
	Macros`SetArgumentCount[CakeWaveletStack3D, {1, Infinity}];
	CakeWaveletStack3D = LieAnalysis`OrientationScores3D`Filters`CakeWavelets`Private`CakeWaveletStack3D;



	OrientationScoreTransform3D::usage = "
	OrientationScoreTransform3D[data_,OptionsPatten_] creates an orientation score from data using the default parameters or the specified options for the cake wavelets.
	OrientationScoreTransform3D[data_, ObjCakeWavelet3D_] creates an orientation score using the supplied ObjCakeWavelet3D
  	";
	SyntaxInformation[OrientationScoreTransform3D] = {"ArgumentsPattern"-> {_, OptionsPattern[]}};
	Macros`SetArgumentCount[OrientationScoreTransform3D, {1, Infinity}];
	OrientationScoreTransform3D = LieAnalysis`OrientationScores3D`Construction`Private`OrientationScoreTransform3D;



	InverseOrientationScoreTransform3D::usage = "
  	InverseOrientationScoreTransform3D[data_, Obj3DPositionOrientationData_, OptionsPattern_] performs reconstruction of the data from an Orientation Score 3D by (default) Summation, L2 or Exact.
  	";
	SyntaxInformation[InverseOrientationScoreTransform3D] = {"ArgumentsPattern"-> {_, OptionsPattern[]}};
	Macros`SetArgumentCount[InverseOrientationScoreTransform3D, {1, Infinity}];
	InverseOrientationScoreTransform3D = LieAnalysis`OrientationScores3D`Reconstruction`Private`InverseOrientationScoreTransform3D;



	LeftInvariantDerivatives3D::usage = "
	OrientationScoreDerivatives3D[Obj3DPositionOrientationData_, {sigmaSpatial_, sigmaOrientation_}, derivativeIndex_, OptionsPattern_] computes the derivatives of the SE(3) space.
	";
	(*SyntaxInformation[OrientationScoreDerivatives3D] = {"ArgumentsPattern"-> {_, {_,_},_,OptionsPattern[]}};*)
	(*Macros`SetArgumentCount[OrientationScoreDerivatives3D, {3, Infinity}];*)
	LeftInvariantDerivatives3D = LieAnalysis`OrientationScores3D`OrientationScoreDerivatives`Private`LeftInvariantDerivatives3D;

	
	
	OrientationScoreTensor3D::usage = "
	OrientationScoreTensor3D[Obj3DPositionOrientationData_, {sigmaSpatial_, sigmaOrientation_}, derivativeIndex_, OptionsPattern_] computes the tensor of the SE(3) space.
	";
	OrientationScoreTensor3D = LieAnalysis`OrientationScores3D`OrientationScoreTensor`Private`OrientationScoreTensor3D;
	
	
	
	GlyphPlot::usage = "
		todo
	";
	GlyphPlot = LieAnalysis`Visualization`GlyphPlot`Private`GlyphPlot;

	OrientationScoreGaugeFrames3D::usage ="
	OrientationScoreGaugeFrames3D[objTensor_, opts:OptionsPattern[]]
	OrientationScoreGaugeFrames3D[objOst_, {sigmaOrientation_, sigmaSpatial_}, opts:OptionsPattern[]]
	";
	SyntaxInformation[OrientationScoreGaugeFrames3D] = {"ArgumentsPattern"->{_,_.,OptionsPattern[]}};
	OrientationScoreGaugeFrames3D = LieAnalysis`OrientationScores3D`GaugeFrames`ExponentialCurveFit`Private`OrientationScoreGaugeFrames3D;
	

EndPackage[];

