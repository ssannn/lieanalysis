(*************************************************************************************************************************
** Common.m
** This .m file contains context for commonly used functions
**
** Author: F.C. Martin <f.c.martin@tue.nl>
** Version: 0.1
**************************************************************************************************************************)
BeginPackage["LieAnalysis`Common`", {
	"Classes`",
	"LieAnalysis`Common`GaussianDerivative`",
	"LieAnalysis`Common`SphericalHarmonics`",
	"LieAnalysis`Common`GroupDifferences`",
	"LieAnalysis`Common`GroupDifferences3D`",
	"LieAnalysis`Common`EigenSystem`",
	"LieAnalysis`Common`Rotations`",
	"LieAnalysis`Common`DownloadFiles`",
	"LieAnalysis`Common`CUDALibraryLoad`",
	"LieAnalysis`Common`ExternalRegularization`"
}];


	
	DownloadFile::usage = "
	DownloadFile[url_], Download a file specified by the URL.
	";
	DownloadFile = LieAnalysis`Common`DownloadFiles`Private`DownloadFile;


	
	LoadCUDAFunction::usage = "
	LoadCudaFunction[functionName_], load the cuda functionName
	";
	LoadCUDAFunction = LieAnalysis`Common`CUDALibraryLoad`Private`LoadCUDAFunction;

	LoadCppFunction::usage = "
	LoadCppFunction[functionName_], load the cpp functionName
	";
	LoadCppFunction = LieAnalysis`Common`CUDALibraryLoad`Private`LoadCppFunction;


	WaveletTransform::usage = "
	WaveletTransform[image_, kernels_] correlates each kernel with the image and returns the stack of transformed images.
	WaveletTransform[image_, ObjWavelet] correlates each kernel from the ObjWavelet with the image and returns the stack of transformed images.
	";
	SyntaxInformation[WaveletTransform] = {"ArgumentsPattern"-> {_, _, OptionsPattern[]}};
	Macros`SetArgumentCount[WaveletTransform, {1, Infinity}];
	WaveletTransform = LieAnalysis`OrientationScores2D`Construction`Private`WaveletTransform;



	WaveletTransform3D::usage = "
	WaveletTransform3D[data_, kernels_] computes the wavelet transform of data using the supplied kernels.
	WaveletTransform3D[data_, ObjWavelet3D] computes the wavelet transform of data using the kernels from ObjWavelet3D.
	";
	SyntaxInformation[WaveletTransform3D] = {"ArgumentsPattern"-> {_,_, OptionsPattern[]}};
	Macros`SetArgumentCount[WaveletTransform3D, {1, Infinity}];
	WaveletTransform3D = LieAnalysis`OrientationScores3D`Construction`Private`WaveletTransform3D;



	GaussianDerivative::usage = "
	GaussianDerivative[data, sigmas, orders, padding], computes the gaussian derivative from data of the specified order.
	";
	GaussianDerivative = LieAnalysis`Common`GaussianDerivative`Private`GaussianDerivative;


	(* Finite differences in 3D *)
	GroupDifferences3D::usage = "
	GroupDifferences[U,direction] calculates finite differences of the function U in direction (\"A1\", \"A2\", \"A3\",
	\"A4\", \"A5\"). The scheme and differentiation order can be changed using the options DifferentiationScheme
	(\"Forward\", \"Backward\", \"Central\") and DifferentiationOrder (1, 2) respectively.
	";
	GroupDifferences3D = LieAnalysis`Common`GroupDifferences3D`Private`GroupDifferences3D;

	GroupDifferences::usage = "
	GroupDifferences[os, order] calculates the left-invariant finite difference derivative of the orientation score \"os\". The order of the derivative is spedified by an integer number by parameter \"order\". The following inputs for \"order\" are possible:
	1 :    \!\(\*SubscriptBox[\(A\), \(1\)]\)-derivative (aligned with the first array index (spatial), i.e., from top to bottom in an image when representing it as an array),\n            
	11:    \!\(\*SubscriptBox[\(A\), \(1\)]\)\!\(\*SubscriptBox[\(A\), \(1\)]\)-derivative,\n            
	2 :    \!\(\*SubscriptBox[\(A\), \(2\)]\)-derivative (aligned with the second array index (spatial), i.e., from left to right in an image when representing it as an array),\n            
	22:    \!\(\*SubscriptBox[\(A\), \(2\)]\)\!\(\*SubscriptBox[\(A\), \(2\)]\)-derivative,\n            
	3 :    \!\(\*SubscriptBox[\(A\), \(3\)]\)-derivative (aligned with third array index (orientation)),\n            
	33:    \!\(\*SubscriptBox[\(A\), \(3\)]\)\!\(\*SubscriptBox[\(A\), \(3\)]\)-derivative,\n            
	??:    Any other arbitrary order of derivatives will be generated using a sequence of first order derivatives, executed from left to right (i.e., \"12\" means \!\(\*SubscriptBox[\(A\), \(2\)]\)\!\(\*SubscriptBox[\(A\), \(1\)]\)).\n
	      	The option \"Method\" can be set as follows:\n            \"Method\"->\"Forward\": use forward differences,\n            \"Method\"->\"Backward\": use backward differences,\n            \"Method\"->\"Central\" (DEFAULT): use central differences.\n
	      	The option \"OrientationFirst\" can be set as follows:\n            \"OrientationFirst\"->False (DEFAULT): the input (and output) is provided in an array in which the first two indices encode the spatial coordinates and the third encodes orientation,\n            \"OrientationFirst\"->True: the input (and output) is provided in an array in which the first index encodes orientation and the other two the spatial coordinates.\n
	      	The option \"Periodicity\" can be set as follows:\n            \"Periodicity\"->\[Pi] (DEFAULT): the orientation axis of the score runs from 0 to \[Pi],\n            \"Periodicity\"->2\[Pi]: the orientation axis of the score runs from 0 to 2\[Pi].
	";
	GroupDifferences = LieAnalysis`Common`GroupDifferences`Private`GroupDifferences;


	(* Spherical Harmonics *)
	RealSphericalHarmonicParameterList::usage = "
	RealSphericalHarmonicParameterList[order], returns the m and l indices pairs for the specified order.
	";
	RealSphericalHarmonicParameterList = LieAnalysis`Common`SphericalHarmonics`Private`RealSphericalHarmonicParameterList;



	RealSphericalHarmonicOrderList::usage = "
	RealSphericalHarmonicOrderList[order], returns the m indices for the specified order.
	";
	RealSphericalHarmonicOrderList = LieAnalysis`Common`SphericalHarmonics`Private`RealSphericalHarmonicOrderList;



	RealSphericalHarmonicValues::uage = "
	RealSphericalHarmonicValues[angles, order, opts], returns the spherical harmonics basis for all the specified angles
	and orders.
	";
	RealSphericalHarmonicValues = LieAnalysis`Common`SphericalHarmonics`Private`RealSphericalHarmonicValues;



	RealSphericalHarmonicFit::usage = "
	RealSphericalHarmonicFit[pts, data, order], computes the spherical harmonics coefficients (up to 'order'). The pts are
	coordinates and data is a list containing the values at the corresponding coordinates.
	";
	RealSphericalHarmonicFit = LieAnalysis`Common`SphericalHarmonics`Private`RealSphericalHarmonicFit;



	RealSphericalHarmonicY::usage = "
	RealSphericalHarmonicY[l,m,theta,phi], returns the spherical harmonic for the specified l,m and angles.
	";
	RealSphericalHarmonicY = LieAnalysis`Common`SphericalHarmonics`Private`RealSphericalHarmonicY;



	A4RealSphericalHarmonicY::usage = "
	A4RealSphericalHarmonicY[], returns the phi derivative spherical harmonic for the sp
	";
	A4RealSphericalHarmonicY = LieAnalysis`Common`SphericalHarmonics`Private`A4RealSphericalHarmonicY;



	A5RealSphericalHarmonicY::usage = "
	";
	A5RealSphericalHarmonicY = LieAnalysis`Common`SphericalHarmonics`Private`A5RealSphericalHarmonicY;



	A5A5RealSphericalHarmonicY::usage = "
	";
	A5A5RealSphericalHarmonicY = LieAnalysis`Common`SphericalHarmonics`Private`A5A5RealSphericalHarmonicY;



	A5A4RealSphericalHarmonicY::usage = "
	";
	A5A4RealSphericalHarmonicY = LieAnalysis`Common`SphericalHarmonics`Private`A5A4RealSphericalHarmonicY;



	A4A4RealSphericalHarmonicY::usage = "
	";
	A4A4RealSphericalHarmonicY = LieAnalysis`Common`SphericalHarmonics`Private`A4A4RealSphericalHarmonicY;



	RealSphericalHarmonicYNormal::usage;
	RealSphericalHarmonicYNormal = LieAnalysis`Common`SphericalHarmonics`Private`RealSphericalHarmonicYNormal;



	SphericalHarmonicsLMax::usage = "
	Computes the maximum L (used in Spherical Harmonics) for a specified number of samples.
	";
	SphericalHarmonicsLMax = LieAnalysis`Common`SphericalHarmonics`Private`SphericalHarmonicsLMax;



	SphericalHarmonicsIndexToL::usage;
	SphericalHarmonicsIndexToL = LieAnalysis`Common`SphericalHarmonics`Private`SphericalHarmonicsIndexToL;



	SphericalHarmonicsLMToIndex::usage;
	SphericalHarmonicsLMToIndex = LieAnalysis`Common`SphericalHarmonics`Private`SphericalHarmonicsLMToIndex;



	SphericalHarmonicsIndexToM::usage;
	SphericalHarmonicsIndexToM = LieAnalysis`Common`SphericalHarmonics`Private`SphericalHarmonicsIndexToM;

	SphericalHarmonicsAngularBlur::usage = "
	"
	SphericalHarmonicsAngularBlur = LieAnalysis`Common`SphericalHarmonics`Private`SphericalHarmonicsAngularBlur;

	SphericalHarmonicsTable::usage = "
	SphericalHarmonicsTable[direction, lMax_Integer], pre-computes the spherical harmonics expressions up to Lmax. Then the expresions can be evaluated at specified beta and gamma.
	";
	SphericalHarmonicsTable = LieAnalysis`Common`SphericalHarmonics`Private`SphericalHarmonicsTable;

    

	CartesianToSphericalCoordinates::usage = "
	CartesianToSphericalCoordinates[{x,y,z}], converts coordinates {x,y,z} to spherical coordinates, function is listable. 
	";
	CartesianToSphericalCoordinates = LieAnalysis`Common`CoordinateSystemConversions`Private`CartesianToSphericalCoordinates;
	
	
	
	FromLeftInvariantFrame::usage = "
	FromLeftInvariantFrame[orientationList, gradient], change of basis from left-invariant to cartesian frame of the gradient.
	FromLeftInvariantFrame[orientationList, tensor], change of basis from left-invariant to cartesian frame of the Hessian of structure tensor. 
	";
	FromLeftInvariantFrame = LieAnalysis`Common`CoordinateSystemConversions`Private`FromLeftInvariantFrame;
	
	
	
	ToLeftInvariantFrame::usage = "
	ToLeftInvariantFrame[orientationList, gradient], change of basis from cartesian to left-invariant frame of the gradient.
	ToLeftInvariantFrame[orientationList, tensor], change of basis from cartesian to left-invariant frame of the Hessian or structure tensor.
	";
	ToLeftInvariantFrame = LieAnalysis`Common`CoordinateSystemConversions`Private`ToLeftInvariantFrame;
	
	
	
	ExternalRegularization::usage = "
	ExternalRegularization[tensor, orientationList, {sigmaSpatialExternal, sigmaAngularExternal}]
	";
	ExternalRegularization = LieAnalysis`Common`ExternalRegularization`Private`ExternalRegularization;
	
	
	
	RealSymmetricThreeEigensystem::usage = "
	Calculates the Eigensystem of a four dimensional array of real, symmetric, three dimensional, second order tensor components, ordered according to the Eigenvalues from largest to smallest.
	By specifying the option Output the type of output can be controlled: 'Eigenvalues', 'Eigenvectors', 'Eigensystem'. By default the eigenvectors are returned.
	";
	RealSymmetricThreeEigensystem = LieAnalysis`Common`EigenSystem`Private`RealSymmetricThreeEigensystem;
	
	
	
	RealSymmetricThreePositiveDefiniteQ::usage = "
	Evaluates each position in a four dimensional array of real, symmetric, three dimensional, second order tensor components, returning 1 if the matrix is positive definite, and 0 otherwise.
	";
	RealSymmetricThreePositiveDefiniteQ = LieAnalysis`Common`EigenSystem`Private`RealSymmetricThreePositiveDefiniteQ;
	
	
    RnWithAlphaIs0::usage = "
    ";
    RnWithAlphaIs0 = LieAnalysis`Common`Rotations`Private`RnWithAlphaIs0;



	CenteredFourier::usage 					= "
	CenteredFourier[im_], computes the Fourier and puts the low frequencies to the center.
	Note that the Fourier transformed is not scaled to the number of samples!
	";



	CenteredInverseFourier::usage 	= "
	CenteredInverseFourier[im_], computes the Inverse Fourier from a Centered Fourier spectrum.
	";


	Reorder::usage = "
	Reorder[os_, order_], reorders the orientation array. Order can either be 'OrientationFirst' or 'SpatialFirst'
	";

	ExpandSymmetry::usage = "
	ExpandSymmetry[os_], returns a copy of the os object with the symmetry removed
	";
	
	
	BlockMatrix::usage = "
	";
	
	ToImage::usage = "
	ToImage[data], triess to convert data to a grayscale image. If failed, it aborts the execution.
	";
	
	ToImage3D::usage = "
	ToImage3D[data], triess to convert data to a grayscale image. If failed, it aborts the execution.
	";
	
	InputProcessor::usage = "
	InputProcessor[data], tries to convert data to a 2D array.
	";
	
	InputProcessor3D::usage = "
	InputProcessor3D[data], tries to convert data to a 3D array.
	";
	
	ComplexQ::usage ="
	ComplexQ[data], checks if the provided data is complex valued.
	"
	

	Begin["`Private`"];

		ComplexQ[val_?NumericQ] := val =!= Re[val]; 
		
		BlockMatrix[M1_, M2_] := ArrayFlatten[({{M1, 0}, {0, M2}})];


		(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
		(* CenteredFourier- - - - - - - - - - - - - - - - - - - - -  *)
		(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)



		CenteredFourier[data_]:=
      RotateRight[Fourier[RotateLeft[data,Floor[Dimensions[data]/2]],FourierParameters->{1,1}],Floor[Dimensions[data]/2]];



		(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
		(* CenteredInverseFourier - - - - - - - - - - - - - - - - -  *)
		(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)



		CenteredInverseFourier[data_]:=
			RotateRight[InverseFourier[RotateLeft[data,Floor[Dimensions[data]/2]],FourierParameters->{1,1}],Floor[Dimensions[data]/2]];



		(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
		(* ToImage- - - - - - - - - - - - - - - - - - - - - - *)
		(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
		
		
		
		convertFail::fail = "Conversion failed to create an `1`.";
		
		
		
		ToImage[data_] := Block[
			
			(* Local Variables Declaration *)
			{image},
			
			(* Convert Matrix *)
			image = Image[data,"Real32"];
			
			(* Convert to Grayscale if needed *)
			image = If[ImageChannels[image] > 1, ColorConvert[image,"Grayscale"],image];
			
			(* Check Proper Image *)
			If[!ImageQ[image], Message[ToImage::fail, "image"]; Abort[];];
			
			(* Return Image *)
			Return[image];
			
		];
		
		
		
		(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
		(* ToImage- - - - - - - - - - - - - - - - - - - - - - - - - -*)
		(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
		
		
		ToImage3D[data_] := Block[
			
			(* Local Variables Declaration *)
			{image},
		
			(* Convert 3D image to data *)
			image = If[MatchQ[data,{{{___}..}..}], Image3D[data,"Real32"], data];

			(* Convert to Grayscale if needed *)
			image = If[ImageChannels[image] > 1, ColorConvert[image,"Grayscale"],image];
			
			(* Check Proper Image *)
			If[!ImageQ[image], Message[ToImage::fail, "image"]; Abort[];]; 
			
			(* Return Image *)
			Return[image];
			
		];
		
		
		(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
		(* InputProcessor- - - - - - - - - - - - - - - - - - - - - - *)
		(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)



		InputProcessor[im_ /; ImageQ[im] ] := InputProcessor[ImageData[im]];
			
			
		InputProcessor[array_ /; ArrayQ[array,3] ] := InputProcessor[ImageData[ColorConvert[Image[array],"GrayScale"]]];
			
			
		InputProcessor[graphics_Graphics] := InputProcessor[ImageData[graphics]];
			
			
		InputProcessor[mat_ /; MatrixQ[mat]] := Block[
	
			(* Local Variables *)
			{data},
			
			(* Check that we ended up with an Packed Array *)
			data = If[!Developer`PackedArrayQ[mat],Developer`ToPackedArray[mat],mat];
			
			(* Ensure the data is between 0 and 1 *)
			(*data = Rescale[data];*) (* see #77 *)
			
			(* Check we ended up with an Matrix *)
			If[!MatrixQ[data],Message[convertFail::fail,"matrix"];Abort[];];
			
			(* Return the clean data *)
			Return[data];
			
		];

		
		InputProcessor[___] := (Message[convertFail::fail, "matrix"];Abort[]);
		
		

		(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
		(* InputProcessor3D- - - - - - - - - - - - - - - - - - - - - *)
		(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)



		InputProcessor3D[im_ /; ImageQ[im] ] := InputProcessor3D[ImageData[im]];
			
			
		InputProcessor3D[array_ /; ArrayQ[array,4] ] := InputProcessor3D[ImageData[ColorConvert[Image3D[array],"GrayScale"]]];

		
		InputProcessor3D[graphics_Graphics3D] := InputProcessor3D[ImageData[graphics]];

			
		InputProcessor3D[mat_ /; ArrayQ[mat,3]] := Block[
	
			(* Local Variables *)
			{data},
			
			(* Check that we ended up with an Packed Array *)
			data = If[!Developer`PackedArrayQ[mat],Developer`ToPackedArray[mat],mat];
			
			(* Ensure the data is between 0 and 1 *)
			(*data = Rescale[data];*) (* see #77 *)
			
			(* Check we ended up with an Matrix *)
			If[!ArrayQ[data,3],Message[convertFail::fail,"3d array"];Abort[];];
			
			(* Return the clean data *)
			Return[data];
			
		]
		
		
		InputProcessor3D[___] := (Message[convertFail::fail, "3d array"];Abort[]);
		


		(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
		(* Reorder - - - - - - - - - - - - - - - - - - - - - - - - - *)
		(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)


		Reorder[os_, order_String] := Which[
			(MatchQ[order, "OrientationFirst"] && Depth[os] == 4),
				Transpose[os, {2,3,1}],
			(MatchQ[order, "SpatialFirst"] && Depth[os] == 4),
				Transpose[os, {3,1,2}],
			(MatchQ[order, "OrientationFirst"] && Depth[os] == 5),
				Transpose[os, {2,3,4,1}],
			(MatchQ[order, "SpatialFirst"] && Depth[os] == 5),
				Transpose[os, {4,1,2,3}]
		];
		
		(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
		(* Expand Symmetry - - - - - - - - - - - - - - - - - - - - - *)
		(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
		
	    (* Returns a full Obj3DPositionOrientationData object *)
    	ExpandSymmetry[object_/;ValidQ[object,LieAnalysis`Objects`Obj3DPositionOrientationData`Obj3DPositionOrientationData]] := Module[
    		
    		{
    			OSCopy = object, cws = object[["Wavelets"]],cwsCopy = object[["Wavelets"]]
    		},
	    	(*Set Values for the wavelet object*)
	   		AffixTo[cwsCopy, 
	   			{
	   				"Data" -> cws["FullData"],
	   		 		"OrientationList" -> cws["FullOrientationList"],
	   				"Symmetry" -> None
	   			}
	   		];
	   
	   		(*Set Values for the Orientation score object*)
	   		AffixTo[OSCopy, 
	   			{
	   				"Data" -> object["FullData"],
	   				"Symmetry" -> None,
	   				"OrientationList" -> cws["FullOrientationList"],
	   				"Wavelets" -> cwsCopy
	   			}
	   		];
	   		OSCopy
	   	];


	End[];



EndPackage[];